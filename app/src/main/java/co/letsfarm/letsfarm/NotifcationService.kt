package co.letsfarm.letsfarm

import android.app.PendingIntent
import android.content.Intent
import android.graphics.BitmapFactory
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import co.letsfarm.letsfarm.utils.log
import co.letsfarm.letsfarm.utils.savepref
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import java.lang.Exception


class NotifcationService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        val channelId = "LetsFarmChannelID"
        log("onMessageReceived ")


        try {
            log("try ")

            val intent = Intent(this, NotificationActivity::class.java)
            intent.putExtra("title", remoteMessage.notification?.title)
            intent.putExtra("body", remoteMessage.notification?.body)

            val pendingIntent =
                PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
            val mBuilder = NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
                .setContentTitle(remoteMessage.notification?.title)
                .setContentText(remoteMessage.notification?.body)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)

            val notificationManager = NotificationManagerCompat.from(this)
            notificationManager.notify(0, mBuilder.build())
        }
        catch (e: Exception){
            log("onMessageReceived Exception")
        }

    }

    override fun onNewToken(s: String) {
        super.onNewToken(s)
    }

}
