package co.letsfarm.letsfarm

import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import android.view.ViewGroup
import co.letsfarm.letsfarm.utils.showDialog
import kotlinx.android.synthetic.main.activity_notification.*

class NotificationActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification)

        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        intent.extras?.let {
            val title = it.getString("title")
            val body = it.getString("body")

            title_tv.text = title
            body_tv.text = body
        }
        close_btn.setOnClickListener { finish() }

    }

}