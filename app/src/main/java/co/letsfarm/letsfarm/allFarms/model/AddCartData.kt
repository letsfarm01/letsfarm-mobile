package co.letsfarm.letsfarm.allFarms.model

import com.google.gson.annotations.SerializedName

data class AddCartData(

	@field:SerializedName("quantity")
	var quantity: Int? = null,

	@field:SerializedName("farmId")
	var farmId: String? = null,

	@field:SerializedName("userId")
	var userId: String? = null
)
