package co.letsfarm.letsfarm.allFarms.model

import com.google.gson.annotations.SerializedName

data class AddToCartRes(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class Data(

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("quantity")
	val quantity: Int? = null,

	@field:SerializedName("deleted")
	val deleted: Boolean? = null,

	@field:SerializedName("total_price")
	val totalPrice: Int? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("farmId")
	val farmId: String? = null,

	@field:SerializedName("userId")
	val userId: String? = null,

	@field:SerializedName("checkout_status")
	val checkoutStatus: Boolean? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)
