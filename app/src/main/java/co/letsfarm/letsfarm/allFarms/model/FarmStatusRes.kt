package co.letsfarm.letsfarm.allFarms.model

import com.google.gson.annotations.SerializedName

//data class FarmStatusRes(
//
//	@field:SerializedName("data")
//	val data: List<FarmStatusData?>? = null
//)

data class FarmStatusData(

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("deleted")
	val deleted: Boolean? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("can_sponsor")
	val canSponsor: Boolean? = null,

	@field:SerializedName("status")
	val status: Boolean? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)
