package co.letsfarm.letsfarm.allFarms.model

import com.google.gson.annotations.SerializedName

data class VerifyRes(

	@field:SerializedName("sponsorship")
	val sponsorship: List<SponsorshipItem?>? = null,

	@field:SerializedName("body")
	val body: String? = null
)

data class SponsorshipItem(

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("deleted")
	val deleted: Boolean? = null,

	@field:SerializedName("orderId")
	val orderId: String? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("expected_end_date")
	val expectedEndDate: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("expected_return")
	val expectedReturn: Int? = null,

	@field:SerializedName("userId")
	val userId: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("start_date")
	val startDate: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)
