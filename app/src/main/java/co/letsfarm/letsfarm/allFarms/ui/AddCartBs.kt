package co.letsfarm.letsfarm.allFarms.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import co.letsfarm.letsfarm.utils.BaseBottomSheetDialog

import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.allFarms.model.AddCartData
import co.letsfarm.letsfarm.allFarms.viewModel.AllFarmsViewModel
import co.letsfarm.letsfarm.dashBoard.model.AllFarmsDataItem
import co.letsfarm.letsfarm.utils.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_add_cart_bs.*

/**
 * A simple [Fragment] subclass.
 */
class AddCartBs() : BaseBottomSheetDialog() {

    var farmQuantity:Int = 0
    lateinit var viewModel: AllFarmsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_cart_bs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = (activity as AllFarmsActivity).viewModel
        setViews()
        setObserver()
    }

    private fun setViews() {
        Picasso.get().load(farm.displayImg).into(farm_img)

        farm_name_tv.text = farm.farmName
        location_tv.text = farm.farmLocation
        roi_tv.text = "${farm.percentageToGain}% ROI ${farm.duration} in ${farm.durationType}"
        min_amount_tv.text = (farm.amountToInvest ?: 0).getMoney + " Per unit"
        location_tv.text = farm.farmLocation

        add_wash_img.setOnClickListener {
            farmQuantity += 1
            wash_et.setText("$farmQuantity")
            amount_tv.text = "${(farmQuantity * (farm.amountToInvest ?: 0 )).getMoney}"
        }
        remove_wash_img.setOnClickListener {
            if (farmQuantity!=0) {
                farmQuantity -= 1
                wash_et.setText("$farmQuantity")
                amount_tv.text = "${(farmQuantity * (farm.amountToInvest ?: 0 )).getMoney}"
            }
        }

        add_cart_btn.setOnClickListener {
            if (farmQuantity == 0){
                activity?.showErrorDialog("Invalid quantity")
            }else {
                val data = AddCartData().apply {
                    farmId = farm.id
                    quantity = farmQuantity
                    userId = activity?.savepref()?.get("userId", "")
                }
                log(data)
                viewModel.addToCart(data)
            }
        }
    }

    private fun setObserver() {
        viewModel.addToCartRequest.observe(viewLifecycleOwner, Observer {resource->
//            if (resource != null)
                add_cart_btn.load(resource is Resource.Loading)

            when(resource){
                is Resource.Success -> {
                    viewModel.addToCart(null)
//                    MaterialAlertDialogBuilder(requireContext()).setTitle(resource.data?.msg ?: "Successful")
//                        .setMessage(resource.message)
//                        .setPositiveButton("Okay"){dialog, which ->
//                            dialog.dismiss()
//                            dismiss()
//                        }.setCancelable(false)
//                        .show()
                    activity?.processError(resource.message, resource.responseCode)
                    dismiss()
                    viewModel.getCartCount()
                }
                is Resource.Error -> {
                    viewModel.addToCart(null)
                    activity?.processError(resource.message, resource.responseCode)
                }
            }

        })
    }

    companion object{

        lateinit var farm: AllFarmsDataItem

        fun newInstance(farm: AllFarmsDataItem):AddCartBs{
            val bs = AddCartBs()
            this.farm = farm
            return bs
        }
    }

}
