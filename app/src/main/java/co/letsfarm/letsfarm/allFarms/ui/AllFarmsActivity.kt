package co.letsfarm.letsfarm.allFarms.ui

import android.app.Application
import android.os.Bundle
import co.letsfarm.letsfarm.BaseActivity
import androidx.lifecycle.Observer
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.allFarms.model.FarmStatusData
import co.letsfarm.letsfarm.allFarms.viewModel.AllFarmsRespository
import co.letsfarm.letsfarm.allFarms.viewModel.AllFarmsViewModel
import co.letsfarm.letsfarm.dashBoard.model.AllFarmsDataItem
import co.letsfarm.letsfarm.utils.*
import dagger.Component
import dagger.Module
import dagger.Provides
import kotlinx.android.synthetic.main.activity_all_farms.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

class AllFarmsActivity : BaseActivity() {

    @Inject
    lateinit var viewModel:AllFarmsViewModel

    val farmStatusAdapter = FarmStatusAdapter(::farmStatusSelected)
    val allFarmsAdapter = AllFarmsAdapter(::farmSelected)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_all_farms)

        DaggerAllFarmsComponent
            .builder()
            .allFarmsModule(AllFarmsModule(application))
            .build()
            .inject(this)

        viewModel.getCartCount()
        viewModel.fetchAllFarms()
        viewModel.fetchFarmStatus()

        setObservers()
        setViews()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getCartCount()
    }

    private fun setViews() {
        cart_img.setOnClickListener {
            if (cart_count_tv.text != "0")
            navigateTo<CartActivity>()
        }
        cart_count_tv.setOnClickListener {
            if (cart_count_tv.text != "0")
            navigateTo<CartActivity>()
        }
        back_img.setOnClickListener { onBackPressed() }
        farm_status_rv.adapter = farmStatusAdapter
        farms_rv.adapter = allFarmsAdapter
    }
    var allFarmsData: List<AllFarmsDataItem?>? = null
    var allFarmsStatusData: List<FarmStatusData?>? = null

    private fun setObservers() {
        viewModel.getCartCountRequest.observe(this, Observer {
            when(it){
                is Resource.Success->{
                    cart_count_tv.text = it.data ?: "0"
                }
                is Resource.Error->{
                    processError(it.message, it.responseCode)
                }
            }
        })
        viewModel.allFarmsRequest.observe(this, Observer {
            progress.load(it is Resource.Loading)
            when(it){
                is Resource.Success->{
                    allFarmsData = it.data?.data

                    allFarmsAdapter.setData(allFarmsData)
                    GlobalScope.launch {
                        createFarmGrouping()
                    }
                }
                is Resource.Error->{
                    processError(it.message, it.responseCode)
                }
            }
        })
        viewModel.farmStatusRequest.observe(this, Observer {
            when(it){
                is Resource.Success->{
                    allFarmsStatusData = it.data
                    farmStatusAdapter.setFarmStatus(allFarmsStatusData)
                    GlobalScope.launch {
                        createFarmGrouping()
                    }
                }
                is Resource.Error->{
                    processError(it.message, it.responseCode)
                }
            }
        })
    }
    val allFarmsGrouped:MutableList<List<AllFarmsDataItem?>>? = mutableListOf()

    private suspend fun createFarmGrouping() {
        if (areNotNull(allFarmsStatusData, allFarmsData)) {

            withContext(Dispatchers.Default) {
                allFarmsStatusData!!.forEach { status ->
                    val sorted = allFarmsData!!.filter {
                        it?.farmStatus?.id == status?.id
                    }
                    allFarmsGrouped!!.add(sorted)
                }
            }
        }
    }

    private fun farmStatusSelected(data: FarmStatusData, position:Int){
        allFarmsAdapter.setData(allFarmsGrouped?.get(position))
    }

    fun farmSelected(data: AllFarmsDataItem){
        AddCartBs.newInstance(data).show(supportFragmentManager, null)
    }

}


@Singleton
@Component(modules = [AllFarmsModule::class])
interface AllFarmsComponent {
    fun inject(activity: AllFarmsActivity)
}

@Module
class AllFarmsModule(private val application: Application) : NetModule() {

    @Provides
    fun provideAllFarmsViewmodel(repo: AllFarmsRespository) : AllFarmsViewModel {
        return AllFarmsViewModel(repo, application)
    }

    @Provides
    fun provideRepository(service: ApiServices) : AllFarmsRespository {
        return AllFarmsRespository(service, application)
    }
}