package co.letsfarm.letsfarm.allFarms.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.dashBoard.model.AllFarmsDataItem
import co.letsfarm.letsfarm.utils.getMoney
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.holder_all_farms.view.*
import kotlin.reflect.KFunction1

class AllFarmsAdapter(val clickListener: (AllFarmsDataItem)->Unit) : RecyclerView.Adapter<AllFarmsViewHolder>() {

    var allFarms: List<AllFarmsDataItem?>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AllFarmsViewHolder {
        LayoutInflater.from(parent.context).inflate(R.layout.holder_all_farms, parent, false).run {
            return AllFarmsViewHolder(this)
        }
    }

    override fun getItemCount() = allFarms?.size ?: 0

    override fun onBindViewHolder(holder: AllFarmsViewHolder, position: Int) {

        holder.itemView.run {
            allFarms?.get(position)?.let {farm->
                setOnClickListener {
                    if (farm.farmStatus?.canSponsor == true){
                        clickListener(farm)
                    }
                }

                available_btn.setOnClickListener {
                    if (farm.farmStatus?.canSponsor == true){
                        clickListener(farm)
                    }
                }

                if (farm.farmStatus?.canSponsor == true){
                    available_btn.text = "Available"
                }else{
                    available_btn.text = "Not available"
                }

                Picasso.get().load(farm.displayImg).into(farm_img)

                farm_name_tv.text = farm.farmName
                location_tv.text = farm.farmLocation
                roi_tv.text = "${farm.percentageToGain}% ROI in ${farm.duration} ${farm.durationType}"
                min_amount_tv.text = (farm.amountToInvest ?: 0).getMoney + " Per unit"
                location_tv.text = farm.farmLocation
            }
        }
    }

    fun setData(data: List<AllFarmsDataItem?>?) {
        allFarms = data
        notifyDataSetChanged()
    }

}

class AllFarmsViewHolder(itemView: View):RecyclerView.ViewHolder(itemView)
