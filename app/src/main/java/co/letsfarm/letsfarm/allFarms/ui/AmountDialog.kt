package co.letsfarm.letsfarm.allFarms.ui

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import co.letsfarm.letsfarm.utils.AmountEtFormattingTextWatcher
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.utils.isValid
import co.letsfarm.letsfarm.utils.removeComma
import kotlinx.android.synthetic.main.fragment_amount_dialog.*


/**
 * A simple [Fragment] subclass.
 */
class AmountDialog(val listener:(String)->Unit) : DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_amount_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        amount_et.addTextChangedListener(
            AmountEtFormattingTextWatcher(
                amount_et
            )
        )

        submit_btn.setOnClickListener {
            if (amount_et.isValid()){
                listener(amount_et.removeComma)
                dismiss()
            }
        }

        cancel_btn.setOnClickListener {
            dismiss()
        }
    }

    override fun onStart() {
        super.onStart()
        val dialog: Dialog? = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            dialog.window?.setLayout(width, height)
        }
    }
}
