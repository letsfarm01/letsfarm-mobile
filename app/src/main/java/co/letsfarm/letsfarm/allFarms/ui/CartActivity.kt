package co.letsfarm.letsfarm.allFarms.ui

import android.app.Application
import android.os.Bundle
import co.letsfarm.letsfarm.BaseActivity
import androidx.lifecycle.Observer
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.allFarms.model.GetCartData
import co.letsfarm.letsfarm.allFarms.viewModel.AllFarmsRespository
import co.letsfarm.letsfarm.allFarms.viewModel.AllFarmsViewModel
import co.letsfarm.letsfarm.allFarms.viewModel.CardTransactions
import co.letsfarm.letsfarm.dashBoard.model.CardPaymentCharges
import co.letsfarm.letsfarm.dashBoard.model.CheckOutSavedCard
import co.letsfarm.letsfarm.dashBoard.model.PaymentOptionRes
import co.letsfarm.letsfarm.dashBoard.ui.*
import co.letsfarm.letsfarm.paymentProof.ui.PaymentProofActivity
import co.letsfarm.letsfarm.ui.BottomNavActivity
import co.letsfarm.letsfarm.utils.*
import dagger.Component
import dagger.Module
import dagger.Provides
import kotlinx.android.synthetic.main.activity_cart.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import javax.inject.Inject
import javax.inject.Singleton

class CartActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: AllFarmsViewModel
    var totalPrice = 0
    var cardAdapter = CartAdapter{viewModel.deleteCartItem(it)}
    var paymentOptions:List<PaymentOptionRes?>? = null
    var cardPaymentCharges: CardPaymentCharges? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)

        DaggerCartComponent
            .builder()
            .cartModule(CartModule(application))
            .build()
            .inject(this)

        viewModel.getCart()
        viewModel.getWalletBalance()
        viewModel.getPaymentOption()
        setViews()
        setObservers()
    }

    private fun setObservers() {
        viewModel.deleteCartItemRequest.observe(this, Observer {resource->
            check_out_btn.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success->{
                    viewModel.getCart()
                }
                is Resource.Error->{
                    processError(resource.message, resource.responseCode)
                }
            }
        })
        viewModel.getCartRequest.observe(this, Observer {resource->
            progress.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success->{
                    if (resource.data.isNullOrEmpty()){
                        processError("No item found", resource.responseCode)
                        onBackPressed()
                    }else {
                        cardAdapter.setdData(resource.data)
                        totalPrice = getTotalPrice(resource.data)

                        viewModel.getCardPaymentCharges(totalPrice)

                        total_price_tv.text = totalPrice.getMoney
                    }
                }
                is Resource.Error->{
                    processError(resource.message, resource.responseCode)
                }
            }
        })
        viewModel.getCardPaymentChargesRequest.observe(this, Observer {resource->
            when(resource){
                is Resource.Success->{
                    cardPaymentCharges = resource.data
                }
                is Resource.Error->{
                    processError(resource.message, resource.responseCode)
                }
            }
        })
        viewModel.getPaymentOptionRequest.observe(this, Observer {resource->
            when(resource){
                is Resource.Success->{
                    paymentOptions = resource.data
                }
                is Resource.Error->{
                    processError(resource.message, resource.responseCode)
                }
            }
        })
        viewModel.checkoutRequest.observe(this, Observer {resource->
            check_out_btn.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success->{

                    processError(resource.message, resource.responseCode)

                    when(checkOutOption!!.name){
                        // saved card uses another route
                        "Paystack"->{
                            navigateTo<CheckOutNewCardActivity> {
                                resource.data?.details?.run {
                                    putExtra("cardTransactions", CardTransactions.FARM_CHECKOUT.toString())
                                    putExtra("ref", ref)
                                    putExtra("accessCode", accessCode)
                                    putExtra("transactionId", transactionId)
                                    putExtra("totalPrice", total)
                                }
                            }
                        }
                        "Bank Transfer"->{
                            navigateTo<PaymentProofActivity> {
                                putExtra("amount", resource.data?.amount)
                                putExtra("transactionId", resource.data?.transactionId)
                                putExtra("transactionRef", resource.data?.transactionRef)
                            }
                        }
                        "e-Wallet"-> navigateToNew<BottomNavActivity>()
                    }

                }
                is Resource.Error->{
                    processError(resource.message, resource.responseCode)
                }
            }
        })
        viewModel.checkoutSavedCardRequest.observe(this, Observer {resource->
            check_out_btn.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success->{
                    navigateTo<BottomNavActivity>()
                    processError(resource.message, resource.responseCode)
                }
                is Resource.Error->{
                    processError(resource.message, resource.responseCode)
                }
            }
        })

        viewModel.getWalletBalanceRequest.observe(this, Observer {
            when(it){
                is Resource.Success->{
                    walletBalance = (it.data?.get("wallet_balance") ?: "0").toDouble()


                }
                is Resource.Error->{
                    processError(it.message, it.responseCode)
                }
            }
        })
    }

    var walletBalance:Double? = null

    private fun getTotalPrice(data: List<GetCartData?>?): Int {
        var totalPrice = 0
        data?.forEach {
            totalPrice += it?.totalPrice ?: 0
        }
        return totalPrice
    }

    private fun setViews() {
        toolbar_title_tv.text = "Cart"
        back_img.setOnClickListener { onBackPressed() }
        cart_rv.adapter = cardAdapter
        check_out_btn.setOnClickListener { showPaymentOptions() }
    }

    private fun showPaymentOptions() {

        if (paymentOptions == null){
            viewModel.getPaymentOption()
            return
        }

        if (cardPaymentCharges == null){
            viewModel.getCardPaymentCharges(totalPrice)
            return
        }
        if (walletBalance == null){
            viewModel.getWalletBalance()
            return
        }

        PaymentOptionsFragment(walletBalance!!, paymentOptions!!, cardPaymentCharges!!,
            ::paymentOptionsselection).show(supportFragmentManager, null)
    }

    var checkOutOption:PaymentOptionRes?=null

    fun paymentOptionsselection(option:PaymentOptionRes){
        checkOutOption = option

        if (option.name == "Paystack"){
            CardSelectionDialog(CardSelectionOrigin.CART){ mCardId->
                if (mCardId == null){
                    viewModel.checkout(option.id)
                }else{
                    val checkOutSavedCard = CheckOutSavedCard().apply{
                        paymentGateway = option.id
                        cardId = mCardId
                        userId = savepref().get("userId", "")
                        savepref().get("userId", "")
                    }

                    viewModel.checkoutSavedCard(checkOutSavedCard)
                }
            }.show(supportFragmentManager, null)
        }else{
            viewModel.checkout(option.id)
        }
    }
}

@Singleton
@Component(modules = [CartModule::class])
interface CartComponent {
    fun inject(activity: CartActivity)
}

@Module
class CartModule(private val application: Application) : NetModule() {

    @Provides
    fun provideAllFarmsViewModel(repo: AllFarmsRespository) : AllFarmsViewModel {
        return AllFarmsViewModel(repo, application)
    }

    @Provides
    fun provideRepository(service: ApiServices) : AllFarmsRespository {
        return AllFarmsRespository(service, application)
    }
}