package co.letsfarm.letsfarm.allFarms.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.allFarms.model.GetCartData
import co.letsfarm.letsfarm.utils.getMoney
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.holder_cart.view.*

class CartAdapter(val deleteListener:(String)->Unit) : RecyclerView.Adapter<CartViewHolder>() {

    var cartData: List<GetCartData?>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder {
        LayoutInflater.from(parent.context).inflate(R.layout.holder_cart, parent, false).run {
            return CartViewHolder(this)
        }
    }

    override fun getItemCount() = cartData?.size ?: 0

    override fun onBindViewHolder(holder: CartViewHolder, position: Int) {
        holder.itemView.run {
            cartData?.get(position)?.let {cart->
                val farm = cart.farmId!!
                Picasso.get().load(farm.displayImg).into(farm_img)

                delete_img.setOnClickListener {
                    deleteListener(cart.id!!)
                }

                farm_name_tv.text = farm.farmName
                location_tv.text = farm.farmLocation
                roi_tv.text = "${farm.percentageToGain}% ROI in ${farm.duration} ${farm.durationType}"
                min_amount_tv.text = (farm.amountToInvest ?: 0).getMoney + " Per unit"
                location_tv.text = farm.farmLocation
                number_tv.text = "${cart.quantity} units"
                total_amount_tv.text = "${cart.totalPrice?.getMoney} in total"
            }
        }
    }

    fun setdData(data: List<GetCartData?>?) {
        cartData = data
        notifyDataSetChanged()
    }

}

class CartViewHolder(itemView:View):RecyclerView.ViewHolder(itemView)
