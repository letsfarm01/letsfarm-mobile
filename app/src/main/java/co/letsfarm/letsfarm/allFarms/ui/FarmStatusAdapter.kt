package co.letsfarm.letsfarm.allFarms.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.allFarms.model.FarmStatusData
import kotlinx.android.synthetic.main.holder_farm_status.view.*

class FarmStatusAdapter(val selectionListener:(FarmStatusData, Int)->Unit) : RecyclerView.Adapter<FarmStatusViewHolder>() {

    var farmStatusList: List<FarmStatusData?>? = null
    var selectedItemPosition:Int = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FarmStatusViewHolder {
        LayoutInflater.from(parent.context).inflate(R.layout.holder_farm_status, parent, false).run {
            return FarmStatusViewHolder(this)
        }
    }

    override fun getItemCount() = farmStatusList?.size ?: 0

    override fun onBindViewHolder(holder: FarmStatusViewHolder, position: Int) {
        holder.itemView.run {
            farmStatusList?.get(position)?.let {status->
                setOnClickListener{
                    selectedItemPosition = position
                    selectionListener(status, position)
                    notifyDataSetChanged()
                }
                status_tv.text = status.name
                if (selectedItemPosition == position){
                    status_tv.setBackgroundResource(R.drawable.unselected_farm_status_bg)
                    status_tv.setTextColor(resources.getColor(android.R.color.white))
                }else{
                    status_tv.setBackgroundResource(R.drawable.farm_status_bg)
                    status_tv.setTextColor(resources.getColor(android.R.color.black))
                }
            }
        }
    }

    fun setFarmStatus(data: List<FarmStatusData?>?) {
        this.farmStatusList = data
        notifyDataSetChanged()
    }

}

class FarmStatusViewHolder(itemView:View):RecyclerView.ViewHolder(itemView)
