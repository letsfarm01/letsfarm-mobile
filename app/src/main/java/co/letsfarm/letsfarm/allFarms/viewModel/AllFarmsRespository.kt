package co.letsfarm.letsfarm.allFarms.viewModel

import android.app.Application
import androidx.lifecycle.LiveData
import co.letsfarm.letsfarm.dashBoard.model.GeneralRes
import co.letsfarm.letsfarm.allFarms.model.*
import co.letsfarm.letsfarm.auth.viewModel.NetworkResource
import co.letsfarm.letsfarm.utils.ApiResponse
import co.letsfarm.letsfarm.dashBoard.model.*
import co.letsfarm.letsfarm.utils.ApiServices
import co.letsfarm.letsfarm.utils.BaseRepositiory
import co.letsfarm.letsfarm.utils.Resource
import co.letsfarm.letsfarm.utils.isConnectedToTheInternet


class AllFarmsRespository (val apiServices: ApiServices, val application: Application) : BaseRepositiory(apiServices, application){

    fun fetchAllFarms(): LiveData<Resource<AllFarmsResData>> {
        return object : NetworkResource<AllFarmsResData, AllFarmsResData>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<AllFarmsResData>>> {
                return apiServices.fetchAllFarms(token)
            }

        }.asLiveData()
    }

    fun fetchFarmStatus(): LiveData<Resource<List<FarmStatusData?>?>> {
        return object : NetworkResource<List<FarmStatusData?>?, List<FarmStatusData?>?>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<List<FarmStatusData?>?>>> {
                return apiServices.fetchFarmStatus(token)
            }

        }.asLiveData()
    }
    fun addToCart(data: AddCartData): LiveData<Resource<AddToCartRes?>> {
        return object : NetworkResource<AddToCartRes?, AddToCartRes?>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<AddToCartRes?>>> {
                return apiServices.addToCart(token, data)
            }

        }.asLiveData()
    }
    fun getCart(): LiveData<Resource<List<GetCartData?>>> {
        return object : NetworkResource<List<GetCartData?>, List<GetCartData?>>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<List<GetCartData?>>>> {
                return apiServices.getCart(token, userId)
            }

        }.asLiveData()
    }
    fun deleteCartItem(id:String): LiveData<Resource<Any>> {
        return object : NetworkResource<Any, Any>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<Any>>> {
                return apiServices.deleteCartItem(token, id)
            }

        }.asLiveData()
    }
    fun getCartCount(): LiveData<Resource<String?>> {
        return object : NetworkResource<String?, String?>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<String?>>> {
                return apiServices.getCartCount(token, userId)
            }

        }.asLiveData()
    }

    fun getPaymentOption(): LiveData<Resource<List<PaymentOptionRes?>>> {
        return object : NetworkResource<List<PaymentOptionRes?>, List<PaymentOptionRes?>>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<List<PaymentOptionRes?>>>> {
                return apiServices.getPaymentOption(token)
            }

        }.asLiveData()
    }
    fun checkout(data: String): LiveData<Resource<CheckOutRes>> {
        return object : NetworkResource<CheckOutRes, CheckOutRes>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<CheckOutRes>>> {
                return apiServices.checkout(token, data)
            }

        }.asLiveData()
    }
    fun checkoutSavedCard(data: CheckOutSavedCard): LiveData<Resource<CheckOutRes>> {
        return object : NetworkResource<CheckOutRes, CheckOutRes>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<CheckOutRes>>> {
                return apiServices.checkoutSavedCard(token, data)
            }

        }.asLiveData()
    }
    fun verifyTransaction(transactionId: String, cardTransactions: CardTransactions): LiveData<Resource<Any>> {
        return object : NetworkResource<Any, Any>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<Any>>> {

                return when(cardTransactions){
                    CardTransactions.WALLET_FUNDING->{
                        apiServices.verifyWalletTransaction(token, transactionId)
                    }
                    CardTransactions.FARM_CHECKOUT->{
                        apiServices.verifyTransaction(token, transactionId)
                    }
                    CardTransactions.PRODUCT_CHECKOUT->{
                        apiServices.verifyProductTransaction(token, transactionId)
                    }
                }

            }
        }.asLiveData()
    }

    fun fetchPendingTransactions(): LiveData<Resource<List<PendingTransfersRes>>> {
        return object : NetworkResource<List<PendingTransfersRes>, List<PendingTransfersRes>>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<List<PendingTransfersRes>>>> {
                return apiServices.fetchPendingTransfers(token, userId)
            }
        }.asLiveData()
    }

    fun uploadProof(data:UploadProofData): LiveData<Resource<Any>> {
        return object : NetworkResource<Any, Any>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<Any>>> {
                return apiServices.uploadProof(token, data)
            }
        }.asLiveData()
    }

     fun getCardPaymentCharges(totalPrice: Int): LiveData<Resource<CardPaymentCharges>> {
        return object : NetworkResource<CardPaymentCharges, CardPaymentCharges>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<CardPaymentCharges>>> {
                return apiServices.getCardPaymentCharges(token, totalPrice)
            }
        }.asLiveData()
    }

    fun getWalletBalance(): LiveData<Resource<Map<String, String>>> {
        return object : NetworkResource<Map<String, String>, Map<String, String>>(application.isConnectedToTheInternet()){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<Map<String, String>>>> {
                return apiServices.getWalletBalance(token, userId)
            }

        }.asLiveData()
    }
}
enum class CardTransactions{
    WALLET_FUNDING, FARM_CHECKOUT, PRODUCT_CHECKOUT
}