package co.letsfarm.letsfarm.allFarms.viewModel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.switchMap
import co.letsfarm.letsfarm.allFarms.model.*
import co.letsfarm.letsfarm.common.utils.AbsentLiveData
import co.letsfarm.letsfarm.dashBoard.model.*
import co.letsfarm.letsfarm.utils.BaseViewModel
import co.letsfarm.letsfarm.utils.Resource

class AllFarmsViewModel(val repository: AllFarmsRespository, val app: Application): BaseViewModel(repository, app){

    private val allFarmsDataLive= MutableLiveData<Boolean>()

    fun fetchAllFarms(){
        allFarmsDataLive.value=true
    }

    val allFarmsRequest: LiveData<Resource<AllFarmsResData>> = allFarmsDataLive.switchMap { allFarmsData->
        if (allFarmsData != null) {
            repository.fetchAllFarms()
        }else{
            AbsentLiveData.create()
        }
    }

    private val farmStatusDataLive= MutableLiveData<Boolean>()

    fun fetchFarmStatus(){
        farmStatusDataLive.value=true
    }

    val farmStatusRequest: LiveData<Resource<List<FarmStatusData?>?>> = farmStatusDataLive.switchMap { farmStatuData->
        if (farmStatuData != null) {
            repository.fetchFarmStatus()
        }else{
            AbsentLiveData.create()
        }
    }

    private val addToCartLive= MutableLiveData<AddCartData?>()

    fun addToCart(data: AddCartData?){
        addToCartLive.value=data
    }

    val addToCartRequest: LiveData<Resource<AddToCartRes?>> = addToCartLive.switchMap { data->
        if (data != null) {
            repository.addToCart(data)
        }else{
            AbsentLiveData.create()
        }
    }


    private val getCartCountLive= MutableLiveData<Boolean>()

    fun getCartCount(){
        getCartCountLive.value=true
    }

    val getCartCountRequest: LiveData<Resource<String?>> = getCartCountLive.switchMap { allFarmsData->
        if (allFarmsData != null) {
            repository.getCartCount()
        }else{
            AbsentLiveData.create()
        }
    }

    private val getCartLive= MutableLiveData<Boolean>()

    fun getCart(){
        getCartLive.value=true
    }

    val getCartRequest: LiveData<Resource<List<GetCartData?>>> = getCartLive.switchMap { allFarmsData->
        if (allFarmsData != null) {
            repository.getCart()
        }else{
            AbsentLiveData.create()
        }
    }

    private val deleteCartItemLive= MutableLiveData<String>()

    fun deleteCartItem(id:String){
        deleteCartItemLive.value=id
    }

    val deleteCartItemRequest: LiveData<Resource<Any>> = deleteCartItemLive.switchMap { data->
        if (data != null) {
            repository.deleteCartItem(data)
        }else{
            AbsentLiveData.create()
        }
    }


    private val getPaymentOptionLive= MutableLiveData<Boolean>()

    fun getPaymentOption(){
        getPaymentOptionLive.value=true
    }
    val getPaymentOptionRequest: LiveData<Resource<List<PaymentOptionRes?>>> = getPaymentOptionLive.switchMap { allFarmsData->
        if (allFarmsData != null) {
            repository.getPaymentOption()
        }else{
            AbsentLiveData.create()
        }
    }

    private val checkoutLive= MutableLiveData<String>()

    fun checkout(id: String?) {
        checkoutLive.value=id
    }

    val checkoutRequest: LiveData<Resource<CheckOutRes>> = checkoutLive.switchMap { data->
        if (data != null) {
            repository.checkout(data)
        }else{
            AbsentLiveData.create()
        }
    }

    private val checkoutSavedCardLive= MutableLiveData<CheckOutSavedCard>()

    fun checkoutSavedCard(id: CheckOutSavedCard?) {
        checkoutSavedCardLive.value=id
    }

    val checkoutSavedCardRequest: LiveData<Resource<CheckOutRes>> = checkoutSavedCardLive.switchMap { data->
        if (data != null) {
            repository.checkoutSavedCard(data)
        }else{
            AbsentLiveData.create()
        }
    }

    private val verifyTransactionLive= MutableLiveData<String>()
    var cardTransactions:CardTransactions? = null

    fun verifyTransaction(transactionId: String, cardTransactions: CardTransactions) {
        this.cardTransactions = cardTransactions
        verifyTransactionLive.value=transactionId
    }

    val verifyTransactionRequest: LiveData<Resource<Any>> = verifyTransactionLive.switchMap { data->
        if (data != null) {
            repository.verifyTransaction(data, cardTransactions!!)
        }else{
            AbsentLiveData.create()
        }
    }

    private val fetchPendingTransactionsLive= MutableLiveData<Boolean>()

    fun fetchPendingTransactions() {
        fetchPendingTransactionsLive.value=true
    }

    val fetchPendingTransactionsRequest: LiveData<Resource<List<PendingTransfersRes>>> = fetchPendingTransactionsLive.switchMap { data->
        if (data != null) {
            repository.fetchPendingTransactions()
        }else{
            AbsentLiveData.create()
        }
    }

    private val uploadProofLive= MutableLiveData<UploadProofData>()

    fun uploadProof(data:UploadProofData) {
        uploadProofLive.value=data
    }

    val uploadProofRequest: LiveData<Resource<Any>> = uploadProofLive.switchMap { data->
        if (data != null) {
            repository.uploadProof(data)
        }else{
            AbsentLiveData.create()
        }
    }

    private val getWalletBalanceLive= MutableLiveData<Boolean>()

    fun getWalletBalance(){
        getWalletBalanceLive.value=true
    }

    val getWalletBalanceRequest: LiveData<Resource<Map<String, String>>> = getWalletBalanceLive.switchMap { loginData->
        if (loginData != null) {
            repository.getWalletBalance()
        }else{
            AbsentLiveData.create()
        }
    }

    private val getCardPaymentChargesLive= MutableLiveData<Int>()

    fun getCardPaymentCharges(totalPrice: Int) {
        getCardPaymentChargesLive.value=totalPrice
    }

    val getCardPaymentChargesRequest: LiveData<Resource<CardPaymentCharges>> = getCardPaymentChargesLive.switchMap { data->
        if (data != null) {
            repository.getCardPaymentCharges(data)
        }else{
            AbsentLiveData.create()
        }
    }
}