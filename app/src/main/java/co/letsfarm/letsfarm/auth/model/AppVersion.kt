package co.letsfarm.letsfarm.auth.model

import com.google.gson.annotations.SerializedName

data class AppVersion(

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("min_allowed_android_version")
	val minAllowedAndroidVersion: String? = null,

	@field:SerializedName("deleted")
	val deleted: Boolean? = null,

	@field:SerializedName("min_allowed_ios_version")
	val minAllowedIosVersion: String? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)
