package co.letsfarm.letsfarm.auth.model

import com.google.gson.annotations.SerializedName

data class LoginData(

	@field:SerializedName("deviceId")
	var deviceId: String? = null,

	@field:SerializedName("password")
	var password: String? = null,

	@field:SerializedName("email")
	var email: String? = null
)