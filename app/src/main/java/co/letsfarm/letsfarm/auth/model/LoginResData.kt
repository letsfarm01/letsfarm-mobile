package co.letsfarm.letsfarm.auth.model

import com.google.gson.annotations.SerializedName

data class LoginResData(

	@field:SerializedName("user")
	val user: LoginResUser? = null,

	@field:SerializedName("token")
	val token: String? = null
)
data class LoginResUser(

	@field:SerializedName("auth_type")
	val authType: String? = null,

	@field:SerializedName("role")
	val role: String? = null,

	@field:SerializedName("last_name")
	val lastName: String? = null,

	@field:SerializedName("is_auth_sign_up")
	val isAuthSignUp: Boolean? = null,

	@field:SerializedName("is_verified")
	val isVerified: Boolean? = null,

	@field:SerializedName("password")
	val password: Any? = null,

	@field:SerializedName("deleted")
	val deleted: Boolean? = null,

	@field:SerializedName("full_name")
	val fullName: String? = null,

	@field:SerializedName("referral_code")
	val referralCode: String? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("phone_number")
	val phoneNumber: String? = null,

	@field:SerializedName("_id")
	val _id: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("email")
	val email: String? = null
)