package co.letsfarm.letsfarm.auth.model

import com.google.gson.annotations.SerializedName

data class ResetPasswordData(

	@field:SerializedName("password")
	var password: String? = null,

	@field:SerializedName("confirmPassword")
	var confirmPassword: String? = null,

	@field:SerializedName("token")
	var token: String? = null
)