package co.letsfarm.letsfarm.auth.model

import com.google.gson.annotations.SerializedName

data class SignUpData(

	@field:SerializedName("password")
	var password: String? = null,

	@field:SerializedName("role")
	var role: String? = null,

	@field:SerializedName("referral_email")
	var referralCode: String? = null,

	@field:SerializedName("last_name")
	var lastName: String? = null,

	@field:SerializedName("phone_number")
	var phoneNumber: String? = null,

	@field:SerializedName("channel_notice")
	var channelNotice: String? = null,

	@field:SerializedName("first_name")
	var firstName: String? = null,

	@field:SerializedName("is_verified")
	var isVerified: Boolean? = null,

	@field:SerializedName("email")
	var email: String? = null
)