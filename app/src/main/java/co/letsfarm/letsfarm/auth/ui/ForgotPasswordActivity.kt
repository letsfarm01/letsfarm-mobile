package co.letsfarm.letsfarm.auth.ui

import android.app.Application
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import br.com.ilhasoft.support.validation.Validator
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.auth.viewModel.AuthRespository
import co.letsfarm.letsfarm.auth.viewModel.AuthViewModel
import co.letsfarm.letsfarm.databinding.ActivityForgotPasswordBinding
import co.letsfarm.letsfarm.utils.*
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.Component
import dagger.Module
import dagger.Provides
import kotlinx.android.synthetic.main.activity_forgot_password.*
import javax.inject.Inject
import javax.inject.Singleton

class ForgotPasswordActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModel: AuthViewModel

    lateinit var binding: ActivityForgotPasswordBinding

    lateinit var validator: Validator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password)

        DaggerForgotPasswordComponent
            .builder()
            .forgotPasswordModule(ForgotPasswordModule(application))
            .build()
            .inject(this)

        validator = Validator(binding)
        binding.owner = this

        setViews()
        setObservers()
    }

    private fun setObservers() {
        viewModel.forgotPasswordRequest.observe(this, Observer {resource->
            send_code_btn.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success -> {
                    MaterialAlertDialogBuilder(this).setTitle("Successful")
                        .setMessage(resource.message)
                        .setPositiveButton("Okay"){dialog, which ->
                            dialog.dismiss()
                            navigateTo<ResetPasswordActivity>()
                        }.setCancelable(false)
                        .show()
                }
                is Resource.Error -> {
                    showErrorDialog(message = resource.message)
                }
            }
        })
    }

    private fun setViews() {
        send_code_btn.setOnClickListener{
            resetPassword()
        }

        login_tv.setOnClickListener {
            navigateTo<LoginActivity>()
        }
    }

    private fun resetPassword() {
        HideSoftKeyboard()
        if (email_et.isEmailValid()){
            viewModel.forgotPassword()
        }
    }
}
@Singleton
@Component(modules = [ForgotPasswordModule::class])
interface ForgotPasswordComponent {
    fun inject(activity: ForgotPasswordActivity)
}

@Module
class ForgotPasswordModule(private val application: Application) : NetModule() {

    @Provides
    fun provideLoginViewmodel(repo: AuthRespository) : AuthViewModel {
        return AuthViewModel(repo, application)
    }

    @Provides
    fun provideRepository(service: ApiServices) : AuthRespository {
        return AuthRespository(service, application)
    }
}