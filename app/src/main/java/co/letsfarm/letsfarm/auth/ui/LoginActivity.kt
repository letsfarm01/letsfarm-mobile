package co.letsfarm.letsfarm.auth.ui

import android.app.Application
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import br.com.ilhasoft.support.validation.Validator
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.auth.viewModel.AuthRespository
import co.letsfarm.letsfarm.auth.viewModel.AuthViewModel
import co.letsfarm.letsfarm.databinding.ActivityLoginBinding
import co.letsfarm.letsfarm.ui.BottomNavActivity
import co.letsfarm.letsfarm.utils.*
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.iid.FirebaseInstanceId
import dagger.Component
import dagger.Module
import dagger.Provides
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject
import javax.inject.Singleton

class LoginActivity : AppCompatActivity() {

    lateinit var binding:ActivityLoginBinding
    lateinit var validator: Validator

    @Inject
    lateinit var viewModel:AuthViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)

        DaggerLoginComponent.builder()
            .loginModule(LoginModule(application))
            .build().inject(this)

        validator = Validator(binding)

        if (!isLive) {
            viewModel.loginData.email = "arokoyuolalekan@gmail.com"
            viewModel.loginData.password = "Lekens0001"

//            viewModel.loginData.email = "pharmjaynee@gmail.com"
//            viewModel.loginData.password = "WD85CSkeFB35"

//            viewModel.loginData.email = "akano.adekola@gmail.com"
//            viewModel.loginData.password = "kkkkkk"
        }

        viewModel.loginData.deviceId = FirebaseInstanceId.getInstance().token

        log("${FirebaseInstanceId.getInstance().token}")
        setViews()

        binding.owner = this

        setObservers()
    }

    private fun setViews() {
        binding.loginBtn.setOnClickListener{
            login()
        }

        forgot_password_tv.setOnClickListener {
            navigateTo<ForgotPasswordActivity>()
        }

        sign_up_tv.setOnClickListener {
            navigateTo<SignUpActivity>()
        }
    }

    private fun setObservers() {
        viewModel.loginRequest.observe(this, Observer {resource->
            login_btn.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success -> {
                    if (resource.data?.user?.isVerified == true){
                        val token = "${resource.data.token}"
                        val userId = "${resource.data.user.id}"
                        val userEmail = "${resource.data.user.email}"
                        val userName = "${resource.data.user.firstName}"
                        val refCode = "${resource.data.user.referralCode}"

                        //Expired token
//                        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiI0c1dfdWRFazVGTXFIWkdzclBpeWthZmFhOTc1RnNXX3VkRWs1Rk1xSFpHc3JQaXlrYWZhYU1xSFpHc3JQaXlrYWZhYXRqcWViamYiLCJkYXRhIjp7InB1YmxpYyI6eyJyb2xlIjoiVVNFUiIsImZ1bGxfbmFtZSI6IkFkZWtvbGEgQWthbm8iLCJ1c2VySWQiOiI1ZWY1ZWQzOTk5NjJiZjAwMTcxOThkMjAiLCJlbWFpbCI6ImFrYW5vLmFkZWtvbGFAZ21haWwuY29tIiwiYWNjZXNzVG9rZW4iOiIxMjM4NC0wOTg3NXBvaXV5dHktOTg3dG84OTBwb2pydDktMjk3NnRyeS0yMDIwLTFxc2RpZFBPS0pIajdpa2otbzg5MHBvanJ0OS0ydHJlZDtzZGZndXl0cjA4ai05ODd0bzg5MHBvanJ0OS0ydHJlZDtzZGZndXl0cjA4ai1hcm9rb3l1b2xhbGVrYW4iLCJwaG9uZV9udW1iZXIiOiIwODA5OTQxMTMwMyJ9LCJzZXNzaW9uIjoiMzdjZjBkYmE4MDdhODcyN2EyYmU3NTc5ZmYzNGU2YmZiNTg1YmMxMzY4MjAxYmI5Yzc1MDQ4ZDVmOGNhYzZiOWUwMDI4NjBmOWYxNzc5ODM3OWY2MjY2OWJkNjFmN2IwZGM4NGM1MjVhNjI4MTZiMjIwNTgyNGMwYWQ2NDVlOGQ0YzM2Njk4ZWRlZDk3YzdmMWJjM2I0MGFlMjY5NzE5ZTZjMDc0MDM3NGRjMmZiYzk2NWJlZjYzNzg3NTE0NjgyZDliYTJiMjYzN2VmNjM4ZGRiMjVkOTQyYzgzYmMzYWI0MWQyM2JlZDQzNzQwMjgxMzE4NjE2ZGU5ZTQ4OGE5NGNlYTc4MDNlN2QxOGU1MjYxY2IxY2JmZDQ5NjJlZGI0MzE5OGI2NDY4NzYzM2E5ZDkyYjI4M2M4N2UxYWFkNjQzN2UxMjRmZjQ3MTljMzdiZDk2YTdkYTczN2JjNDNlNGE0NDk4Mzg3MTM3ZjZlNTJmZjMwMDJmYTFmMzRkZGVlN2Q2YjlmZDQ0M2M0Yzc0NTkxM2MwNjkzNGQxNzQzOGY0MjEwOThkMTY0MzI1ZDk1ZWVlM2E5NmJiMDc5NzRmODVmMjhiNzQ3NzIxZmE1MmI0NTAyMDk2ZWRhN2ExMzBjZWE1MzIyNTNhZjM4YWQwZjc1ZTMyNGM3MDQ3MDdlODM3ZjcyN2UwOTJkN2VlZGU1NzAxM2FjZmQ2Y2IyNDI4MTQ0ZjhiZGEyNjg0NGU3NGFiZDdkYzc0ZTA4NGYwNDRjOGY4YjQzZmQ2Y2M1OGQ3ZWY5ZTUyNDFmOTY0MTdjZWQxOGZmOWY1ODY2YzgwYmU3NDRlNTQ0NDFlYmU3YmM1OGFiMzA3YzZjYjM3YzhhZjQ3ZjFhODNjOTBhNjU5YWQxODI0ODlhZjE0Y2I4M2ZjMGU0OTI3YjI3MGNlNWRkNjFhNWJjYTAwYjk0MjY2MTE4MjE3YjMwZmMzNWUzYTAzM2MzMGU1M2M3ZjU3MjQyZTY2MzQwZGY4ZDMzNTI1MzFlMDg1NTIwMzY2MDY0YWI5NzRmMmUxNDBmNzk0NTEzZmU1YWMzMjI4MDU5Y2U4Y2Y3YTc2NjA4NDZiMjRmYjRhZGM2MTA1NjY0NGY2MDk1ZDI5ODNmMDgwZTRiZWM0YjUyOTEyMDBkNjFmYjdkMDcyYzVlNDU3MTY2NzZmOTFkYWFmOWQ5NzZhNzM4ZjA2OTA4MjY3ZDJjNDQ0ZDAyZjEwZGZiZWRjYTJiZjQ3ZmEzYjE0MGM2Yzk3MGExOGQ1OTYwOWY3MjRlYTVkNmMzMjM3YTAyNzcxYWExYjE5NzVkNGM0M2VlYWFkZjBlMzRhMzQzN2NmNWM0ZDJmOWIxMmJkNzUxMjJiOWJlODAzOGU4NDY5MWE4NDlhN2IwYjI2YTBiOWYzOGJkZDI5OGViODIwMTNkZGMxOGU5ODhmNTU3MDdmMWFmNGM1ODU3MmYwNGRlMWI3MzlkNjNmN2FkMmRhNjk4NDA2ZGY1ZDAyZjkwOTRlZTY4ZGJhZjI5ZjEzYzA4ZmFmM2QyYmUyN2I3ZmZkZGQ4MGRlNjY0MzRkYzgwOGRlOTFhZmE4NjcxNmM3NjhkM2NjMTk5ODNiOTllMzFlYmY4ZjdmNmQzNDExOGNhZWI1MDQ5MDA1ZGY4ZWY0ZWFmZGZjNzJhOTVmMDdmODQxNmIzYjIxYTI4YTExMzM4YjM4Y2EwZjgzZDRhN2YxNTk4OTA5YmJhNzI2OGQ5ZGRmZGI3NWQzZjdhZGMyNTk5YWM5OGI2MmY1MGRiYmM3YTc1NjkzOWYwYjNlOGYzNDQwMjQ2YjhiNmY5YmNiYTJhYmVkYjBkMDhmYmY3Mjk3MTY1Y2E4N2ZmY2UyMzMxZDVlNDVmMTQ1NWIzZWMzYjM1ZTFjNTVjMmEyNWIwNjRkNzljNjE5YjFlMGU5ZDNlODRjOTIzMTA4YzE0ZGIzOWRlNzkzNTAwZTNiNmQ5ZDA3ZjFlNTA5MTJmOWUzNmRhYzMxMyJ9LCJpYXQiOjE1OTM3MTEwNzIsImV4cCI6MTU5NDAzNTA3Mn0.4M2ZnoobQ0-5TEAMK4efG-bseQZI2977L4J4jlrT12c"

                        savepref().putData("token", token)
                        savepref().putData("userId", userId)
                        savepref().putData("userEmail", userEmail)
                        savepref().putData("userName", userName)
                        savepref().putData("refCode", refCode)

                        if (remember_me_cb.isChecked)
                            savepref().putData("rememberMe", true)

                        navigateTo<BottomNavActivity>()
                    }else{
                        viewModel.resendActivationLink()
                    }
                }
                is Resource.Error -> {
//
                    if (resource.status == "USER_NOT_VERIFIED"){
                        viewModel.resendActivationLink()
                    }
                    else
                        showErrorDialog(message = resource.message)
                }
            }
        })

        viewModel.resendActivationLinkRequest.observe(this, Observer {resource->
            login_btn.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success -> {
                    MaterialAlertDialogBuilder(this).setTitle("Activate account")
                        .setMessage(resource.message)
                        .setPositiveButton("Okay"){dialog, which ->
                            dialog.dismiss()
                            login()
                        }
                        .show()
                }
                is Resource.Error -> {
                    showErrorDialog(message = resource.message)
                }
            }
        })
    }

    fun login(){
        HideSoftKeyboard()
        if (validator.validate()){
            viewModel.login()
        }
    }

}

@Singleton
@Component(modules = [LoginModule::class])
interface LoginComponent {
    fun inject(activity: LoginActivity)
}

@Module
class LoginModule(private val application: Application) : NetModule() {

    @Provides
    fun provideLoginViewmodel(repo: AuthRespository) : AuthViewModel {
        return AuthViewModel(repo, application)
    }

    @Provides
    fun provideRepository(service: ApiServices) : AuthRespository {
        return AuthRespository(service, application)
    }
}