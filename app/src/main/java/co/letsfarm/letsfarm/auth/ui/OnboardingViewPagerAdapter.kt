package co.letsfarm.letsfarm.auth.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import co.letsfarm.letsfarm.R
import com.smarteist.autoimageslider.SliderViewAdapter

class OnboardingViewPagerAdapter(val titles: MutableList<String>, val images: MutableList<Int>,
                                 val description: MutableList<String>)
    : SliderViewAdapter<OnboardingAdapterVH>() {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup): OnboardingAdapterVH {
        context = parent.context
        LayoutInflater.from(context).inflate(R.layout.holder_onboarding_view_pager, parent, false).run{
            return OnboardingAdapterVH(this)
        }
    }

    override fun onBindViewHolder(viewHolder: OnboardingAdapterVH, position: Int) {
//        viewHolder.imageView.setImageDrawable(context.resources.getDrawable(images[position]))
        viewHolder.imageView.setImageResource(images[position])

        viewHolder.titleTv.text = titles[position]
        viewHolder.descriptionTv.text = description[position]
    }

    override fun getCount() = 3

}

class OnboardingAdapterVH(itemView: View) : SliderViewAdapter.ViewHolder(itemView) {
    var imageView: ImageView = itemView.findViewById(R.id.imageView)
    var titleTv: TextView = itemView.findViewById(R.id.title_tv)
    var descriptionTv: TextView = itemView.findViewById(R.id.description_tv)
}
