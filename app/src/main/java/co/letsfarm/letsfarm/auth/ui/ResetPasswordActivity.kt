package co.letsfarm.letsfarm.auth.ui

import android.app.Application
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import br.com.ilhasoft.support.validation.Validator
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.auth.viewModel.AuthRespository
import co.letsfarm.letsfarm.auth.viewModel.AuthViewModel
import co.letsfarm.letsfarm.databinding.ActivityResetPasswordBinding
import co.letsfarm.letsfarm.utils.*
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.Component
import dagger.Module
import dagger.Provides
import kotlinx.android.synthetic.main.activity_reset_password.*
import javax.inject.Inject
import javax.inject.Singleton

class ResetPasswordActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModel: AuthViewModel

    lateinit var binding: ActivityResetPasswordBinding

    lateinit var validator: Validator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_reset_password)

        DaggerResetPasswordComponent
            .builder()
            .resetPasswordModule(ResetPasswordModule(application))
            .build()
            .inject(this)

        validator = Validator(binding)

        binding.owner = this

        setViews()
        setObserver()
    }

    private fun setObserver() {
        viewModel.resetPasswordRequest.observe(this, Observer {resource->
            change_password_btn.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success -> {
                    MaterialAlertDialogBuilder(this).setTitle("Successfully")
                        .setMessage(resource.message)
                        .setPositiveButton("Okay"){dialog, which ->
                            dialog.dismiss()
                            navigateTo<LoginActivity>()
                        }.setCancelable(false)
                        .show()
                }

                is Resource.Error -> {
                    showErrorDialog(message = resource.message)
                }
            }
        })
    }

    private fun setViews() {
        back_img.setOnClickListener {
            onBackPressed()
        }

        change_password_btn.setOnClickListener {
            HideSoftKeyboard()
            if (validator.validate() && doPasswordsMatch())
                viewModel.resetPassword()
        }

        login_tv.setOnClickListener {
            navigateTo<LoginActivity>()
        }
    }
    private fun doPasswordsMatch(): Boolean {
        val valid = password_et.trimmedText == confirm_password_et.trimmedText
        if (!valid){
            password_et.error = "Passwords don't match"
            confirm_password_et.error = "Passwords don't match"
        }
        return valid
    }
}
@Singleton
@Component(modules = [ResetPasswordModule::class])
interface ResetPasswordComponent {
    fun inject(activity: ResetPasswordActivity)
}

@Module
class ResetPasswordModule(private val application: Application) : NetModule() {

    @Provides
    fun provideLoginViewmodel(repo: AuthRespository) : AuthViewModel {
        return AuthViewModel(repo, application)
    }

    @Provides
    fun provideRepository(service: ApiServices) : AuthRespository {
        return AuthRespository(service, application)
    }
}