package co.letsfarm.letsfarm.auth.ui

import android.app.Application
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import br.com.ilhasoft.support.validation.Validator
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.auth.viewModel.AuthRespository
import co.letsfarm.letsfarm.auth.viewModel.AuthViewModel
import co.letsfarm.letsfarm.databinding.ActivitySignUpBinding
import co.letsfarm.letsfarm.utils.*
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.Component
import dagger.Module
import dagger.Provides
import kotlinx.android.synthetic.main.activity_sign_up.*
import javax.inject.Inject
import javax.inject.Singleton

class SignUpActivity : AppCompatActivity() {

    lateinit var binding: ActivitySignUpBinding
    lateinit var validator: Validator

    @Inject
    lateinit var viewModel:AuthViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up)

        DaggerSignUpComponent
            .builder()
            .signUpModule(SignUpModule(application))
            .build()
            .inject(this)

        validator = Validator(binding)

        binding.owner = this

        setViews()
        setObservers()
    }

    private fun setViews() {
        binding.loginTv.setOnClickListener{
            navigateTo<LoginActivity>()
        }
        binding.signUpBtn.setOnClickListener{
            signUp()
        }

        binding.termsTv.setOnClickListener {
            val url = "https://letsfarm.com.ng/terms-of-use"
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            startActivity(browserIntent)
        }
    }

    private fun setObservers() {
        viewModel.signUpRequest.observe(this, Observer {resource->
            sign_up_btn.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success -> {
                    MaterialAlertDialogBuilder(this).setTitle("Sign Up Successful")
                        .setMessage(resource.message)
                        .setPositiveButton("Okay"){dialog, which ->
                            dialog.dismiss()
                            navigateTo<LoginActivity>()
                        }
                        .show()
                }
                is Resource.Error -> {
                    showErrorDialog(message = resource.message)
                }
            }
        })
    }

    fun signUp(){
        HideSoftKeyboard()
        if (validator.validate() && doPasswordsMatch()){
            viewModel.signUp()
        }
    }

    private fun doPasswordsMatch(): Boolean {
        val valid = password_et.trimmedText == confirm_password_et.trimmedText
        if (!valid){
            password_et.error = "Passwords don't match"
            confirm_password_et.error = "Passwords don't match"
        }
        return valid
    }
}

@Singleton
@Component(modules = [SignUpModule::class])
interface SignUpComponent {
    fun inject(activity: SignUpActivity)
}

@Module
class SignUpModule(private val application: Application) : NetModule() {

    @Provides
    fun provideSignUpViewmodel(repo: AuthRespository) : AuthViewModel {
        return AuthViewModel(repo, application)
    }

    @Provides
    fun provideRepository(service: ApiServices) : AuthRespository {
        return AuthRespository(service, application)
    }
}