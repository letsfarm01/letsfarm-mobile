package co.letsfarm.letsfarm.auth.ui

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import co.letsfarm.letsfarm.BaseActivity
import androidx.lifecycle.Observer
import co.letsfarm.letsfarm.BuildConfig
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.auth.viewModel.AuthViewModel
import co.letsfarm.letsfarm.ui.BottomNavActivity
import co.letsfarm.letsfarm.utils.*
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability
import com.google.firebase.iid.FirebaseInstanceId
import dagger.Component
import javax.inject.Inject
import javax.inject.Singleton

class SplashActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: AuthViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        DaggerSplashScreenComponent.builder()
            .loginModule(LoginModule(application))
            .build().inject(this)

        setObservers()
    }

    private fun setObservers() {
        viewModel.appVersion()

        viewModel.appVersionRequest.observe(this, Observer {resource->
            when(resource){
                is Resource.Success->{
                    resource.data?.let {appVersion->
                        next(BuildConfig.VERSION_CODE < (appVersion.minAllowedAndroidVersion?.toInt() ?: 0))
                    }
                }
                is Resource.Error->{
                    processError(resource.message, resource.responseCode)
                }
            }
        })

    }

    fun next(appNeedsUpdating:Boolean){
        if (appNeedsUpdating){
            showDialog("Update Required!", "Kindly update your app to enjoy new and amazing features"){
                val url = Uri.parse("https://play.google.com/store/apps/details?id=co.letsfarm.letsfarm")
                startActivity(Intent(Intent.ACTION_VIEW, url))
            }

        }else{

            if(Build.VERSION.SDK_INT >= 21) {
                val appUpdateManager = AppUpdateManagerFactory.create(this)

                val updateListener = InstallStateUpdatedListener { state ->
                    if (state.installStatus() == InstallStatus.DOWNLOADED) {
                        appUpdateManager.completeUpdate()
                    }
                }

                appUpdateManager.registerListener(updateListener)
                val appUpdateInfoTask = appUpdateManager.appUpdateInfo

                appUpdateInfoTask.addOnSuccessListener { appUpdateInfo ->
                    if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                        appUpdateManager.startUpdateFlowForResult(
                            appUpdateInfo,
                            AppUpdateType.FLEXIBLE,
                            this,
                            987
                        )
                    }
                }
            }

            if (savepref("firstTimeUser").get("firstTimeUser", true))
                navigateToNew<ViewPagerActivity>()
            else {

                if (savepref().get("rememberMe", false)){
                    navigateToNew<BottomNavActivity>()
                }else {
                    navigateToNew<LoginActivity>()
                }
            }

        }
    }
}

@Singleton
@Component(modules = [LoginModule::class])
interface SplashScreenComponent {
    fun inject(activity: SplashActivity)
}