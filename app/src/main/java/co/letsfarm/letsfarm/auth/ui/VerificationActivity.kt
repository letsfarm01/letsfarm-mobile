package co.letsfarm.letsfarm.auth.ui

import android.app.Application
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.widget.EditText
import android.widget.TextView
import co.letsfarm.letsfarm.BaseActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.auth.viewModel.AuthRespository
import co.letsfarm.letsfarm.auth.viewModel.AuthViewModel
import co.letsfarm.letsfarm.databinding.ActivityVerificationBinding
import co.letsfarm.letsfarm.utils.ApiServices
import co.letsfarm.letsfarm.utils.NetModule
import co.letsfarm.letsfarm.utils.Resource
import co.letsfarm.letsfarm.utils.trimmedText
import com.google.android.material.textfield.TextInputEditText
import dagger.Component
import dagger.Module
import dagger.Provides
import kotlinx.android.synthetic.main.activity_verification.*
import javax.inject.Inject
import javax.inject.Singleton

class VerificationActivity : BaseActivity(), TextWatcher {

    private var mCode1: TextInputEditText? = null
    private var mCode2: TextInputEditText? = null
    private var mCode3: TextInputEditText? = null
    private var mCode4: TextInputEditText? = null
    private var mCode5: TextInputEditText? = null
    private var mCode6: TextInputEditText? = null
    private var mTextView: TextView? = null
    private var mSubTitleView: TextView? = null
    lateinit var editTexts: List<TextInputEditText>

    var email:String?=null

    @Inject
    lateinit var viewModel: AuthViewModel
    lateinit var binding: ActivityVerificationBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_verification)

        DaggerVerificationComponent
            .builder()
            .verificationModule(VerificationModule(application))
            .build()
            .inject(this)

        email = intent?.extras?.getString("email")

        setViews()
        startTimer()
        setObservers()
    }

    private fun setObservers() {
//        viewModel.resendCodeRequest.observe(this, Observer {
//            when(it){
//                is Resource.Success->{
//                    startTimer()
//                    val successBs = SuccessBs.newInstance("${it.data?.message ?: "A reset token has been sent"}") {
//
//                    }
//                    successBs.show(supportFragmentManager, null)
//                    viewModel.resendCodeClickTrigger.value = false
//                }
//                is Resource.Loading->{}
//                is Resource.Error->{
//                    viewModel.resendCodeClickTrigger.value = false
//                    showErrorDialog("Error", "${it.message}")
//                }
//            }
//        })
////        141854

        viewModel.verifyUserRequest.observe(this, Observer {resource->
            when(resource){
                is Resource.Success ->{

                }
                is Resource.Error->{

                }
            }
        })
    }

    private fun setViews() {
        mCode1 = findViewById(R.id.code_1)
        mCode2 = findViewById(R.id.code_2)
        mCode3 = findViewById(R.id.code_3)
        mCode4 = findViewById(R.id.code_4)
        mCode5 = findViewById(R.id.code_5)
        mCode6 = findViewById(R.id.code_6)

        mTextView = findViewById(R.id.resend_code_tv)

        mCode1?.addTextChangedListener(this)
        mCode2?.addTextChangedListener(this)
        mCode3?.addTextChangedListener(this)
        mCode4?.addTextChangedListener(this)
        mCode5?.addTextChangedListener(this)
        mCode6?.addTextChangedListener(this)

        editTexts = listOf(mCode1!!, mCode2!!, mCode3!!, mCode4!!, mCode5!!, mCode6!!)

        mCode1!!.addTextChangedListener(PinTextWatcher(0, editTexts))
        mCode2!!.addTextChangedListener(PinTextWatcher(1, editTexts))
        mCode3!!.addTextChangedListener(PinTextWatcher(2, editTexts))
        mCode4!!.addTextChangedListener(PinTextWatcher(3, editTexts))
        mCode5!!.addTextChangedListener(PinTextWatcher(4, editTexts))
        mCode6!!.addTextChangedListener(PinTextWatcher(5, editTexts))

        mCode1!!.setOnKeyListener(PinOnKeyListener(0, editTexts))
        mCode2!!.setOnKeyListener(PinOnKeyListener(1, editTexts))
        mCode3!!.setOnKeyListener(PinOnKeyListener(2, editTexts))
        mCode4!!.setOnKeyListener(PinOnKeyListener(3, editTexts))
        mCode5!!.setOnKeyListener(PinOnKeyListener(4, editTexts))
        mCode6!!.setOnKeyListener(PinOnKeyListener(5, editTexts))


        findViewById<View>(R.id.activate_button)?.setOnClickListener { onContinueClick() }
        findViewById<View>(R.id.resend_code_tv)?.setOnClickListener { onResendClick() }

    }

    var cTimer: CountDownTimer? = null
    var isRunning = true

    fun startTimer() {
        isRunning = true
        cTimer = object : CountDownTimer(180000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val mins: Long = millisUntilFinished / (60 * 1000) % 60
                val secs: Long = millisUntilFinished / 1000 % 60
                timer_tv.text = "$mins:$secs"

            }
            override fun onFinish() {
                isRunning = false
            }
        }
        (cTimer as CountDownTimer).start()
    }

    private fun onContinueClick() {
        getCode()?.let {code->
            val request = HashMap<String, String>()
            request["token"] = code
            request["email"] = email ?: ""
            viewModel.verifyUser(request)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        cancelTimer()
    }

    fun cancelTimer() {
        if (cTimer != null) cTimer!!.cancel()
    }

    private fun onResendClick() {
//        if (isRunning || viewModel.resendCodeClickTrigger.value == true){
//
//        }else {
//            viewModel.resendCode()
//        }
    }

    fun getCode(): String? {
        val code = mCode1?.trimmedText + mCode2?.trimmedText + mCode3?.trimmedText + mCode4?.trimmedText + mCode5?.trimmedText + mCode6?.trimmedText
        return if (code.length == 6){
            code
        }
        else{
            showError(editTexts)
            null
        }
    }

    private fun showError(editTexts: List<TextInputEditText>) {
        editTexts.forEach {
            if (it.text.isNullOrEmpty())
                it.error = "Input code"
        }
    }


    override fun afterTextChanged(editable: Editable?) {
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        this.getCode()?.let {
            if (it.length == 6) {
                onContinueClick()
            }
        }

    }

    // TODO("CHANGE") delete all below

    class PinTextWatcher internal constructor(private val currentIndex: Int, private val editTexts: List<EditText>) :
        TextWatcher {
        private var isFirst = false
        private var isLast = false
        private var newTypedString = ""
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            newTypedString = s.subSequence(start, start + count).toString().trim { it <= ' ' }
        }

        override fun afterTextChanged(s: Editable) {
            var text = newTypedString
            /* Detect paste event and set first char */if (text.length > 1) text = text[0].toString() // TODO: We can fill out other EditTexts
            editTexts.get(currentIndex).removeTextChangedListener(this)
            editTexts.get(currentIndex).setText(text)
            editTexts.get(currentIndex).setSelection(text.length)
            editTexts.get(currentIndex).addTextChangedListener(this)
            if (text.length == 1) moveToNext()
//            else if (text.length == 0) moveToPrevious()
        }

        private fun moveToNext() {
            if (!isLast) editTexts.get(currentIndex + 1).requestFocus()
            if (isAllEditTextsFilled && isLast) { // isLast is optional
                editTexts.get(currentIndex).clearFocus()
//                hideKeyboard()
//                onContinueClick()
            }
        }

        private fun moveToPrevious() {
            if (!isFirst) editTexts.get(currentIndex - 1).requestFocus()
        }

        private val isAllEditTextsFilled: Boolean
            private get() {
                for (editText in editTexts) if (editText.text.toString().trim().isEmpty()) return false
                return true
            }


        init {
            if (currentIndex == 0) isFirst = true else if (currentIndex == editTexts.size - 1) isLast = true
        }
    }

    class PinOnKeyListener internal constructor(private val currentIndex: Int, private val editTexts: List<EditText>) : View.OnKeyListener {
        override fun onKey(v: View?, keyCode: Int, event: KeyEvent): Boolean {
            if (keyCode == KeyEvent.KEYCODE_DEL && event.action == KeyEvent.ACTION_DOWN) {
                if (editTexts.get(currentIndex).text.toString().isEmpty() && currentIndex != 0) editTexts.get(currentIndex - 1).requestFocus()
            }
            return false
        }

    }

}

@Singleton
@Component(modules = [VerificationModule::class])
interface VerificationComponent {
    fun inject(activity: VerificationActivity)
}

@Module
class VerificationModule(private val application: Application) : NetModule() {

    @Provides
    fun provideLoginViewmodel(repo: AuthRespository) : AuthViewModel {
        return AuthViewModel(repo, application)
    }

    @Provides
    fun provideRepository(service: ApiServices) : AuthRespository {
        return AuthRespository(service, application)
    }
}
