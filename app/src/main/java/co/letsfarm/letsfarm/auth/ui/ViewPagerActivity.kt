package co.letsfarm.letsfarm.auth.ui

import co.letsfarm.letsfarm.BaseActivity
import android.os.Bundle
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.utils.navigateToNew
import co.letsfarm.letsfarm.utils.putData
import co.letsfarm.letsfarm.utils.savepref
import com.smarteist.autoimageslider.IndicatorAnimations
import com.smarteist.autoimageslider.SliderAnimations
import kotlinx.android.synthetic.main.activity_view_pager.*

class ViewPagerActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_pager)

        startSlideShowAdapter()
        setViews()
    }

    private fun setViews() {
        continue_btn.setOnClickListener {
            savepref("firstTimeUser").putData("firstTimeUser", false)
            navigateToNew<LoginActivity>()
        }
    }

    private fun startSlideShowAdapter() {
        val names = getTitles()
        val description = getDescription()
        val images = getImages()

        sliderView.sliderAdapter =
            OnboardingViewPagerAdapter(
                names,
                images,
                description
            )
        sliderView.startAutoCycle()
        sliderView.setIndicatorAnimation(IndicatorAnimations.WORM)
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
        sliderView.scrollTimeInSec = 3
    }

    private fun getImages(): MutableList<Int> {
        return mutableListOf(
            R.drawable.ic_onboarding1,
            R.drawable.ic_onboarding2,
            R.drawable.ic_onboarding3
        )
    }
    private fun getDescription(): MutableList<String> {
        return mutableListOf(
            "We have some of the highest interest rates in the business!",
            "We provide jobs and equipment for farmers",
            "Our farms are insured and your investments are safe from losses!")
    }
    private fun getTitles(): MutableList<String> {
        return mutableListOf("Unbeatable interest rates", "Empowering Farmers", "we’re insured")
    }
}