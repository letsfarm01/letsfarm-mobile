package co.letsfarm.letsfarm.auth.viewModel

import android.app.Application
import androidx.lifecycle.LiveData
import co.letsfarm.letsfarm.auth.model.AppVersion
import co.letsfarm.letsfarm.dashBoard.model.GeneralRes
import co.letsfarm.letsfarm.auth.model.*
import co.letsfarm.letsfarm.utils.*
import java.util.HashMap

class AuthRespository (val apiServices: ApiServices, val application: Application) : BaseRepositiory(apiServices, application){

    fun login(loginRequest: LoginData): LiveData<Resource<LoginResData>> {
        return object : NetworkResource<LoginResData, LoginResData>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<LoginResData>>> {
                return apiServices.login(loginRequest)
            }

        }.asLiveData()
    }

    fun signUp(signUpRequest: SignUpData): LiveData<Resource<SignUpResData>> {
        return object : NetworkResource<SignUpResData, SignUpResData>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<SignUpResData>>> {
                return apiServices.signUp(signUpRequest)
            }

        }.asLiveData()
    }

    fun forgotPassword(request: HashMap<String, String>): LiveData<Resource<Any>> {
        return object : NetworkResource<Any, Any>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<Any>>> {
                return apiServices.forgotPassword(request)
            }

        }.asLiveData()
    }

    fun resetPassword(resetPasswordData: ResetPasswordData): LiveData<Resource<Any>> {
        return object : NetworkResource<Any, Any>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<Any>>> {
                return apiServices.resetPassword(resetPasswordData)
            }

        }.asLiveData()
    }

    fun verifyUser(request: HashMap<String, String>): LiveData<Resource<Any>> {
        return object : NetworkResource<Any, Any>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<Any>>> {
                return apiServices.verifyUser(request)
            }

        }.asLiveData()
    }

    fun resendActivationLink(email: String): LiveData<Resource<Any>> {

        val request = HashMap<String, String>()
        request["email"] = email

        return object : NetworkResource<Any, Any>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<Any>>> {
                return apiServices.resendActivationLink(request)
            }

        }.asLiveData()
    }
    fun appVersion(): LiveData<Resource<AppVersion>> {
        return object : NetworkResource<AppVersion, AppVersion>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<AppVersion>>> {
                return apiServices.appVersion()
            }

        }.asLiveData()
    }

}