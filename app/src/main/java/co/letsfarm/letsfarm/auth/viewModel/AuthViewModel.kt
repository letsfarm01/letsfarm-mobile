package co.letsfarm.letsfarm.auth.viewModel

import android.app.Application
import androidx.lifecycle.*
import co.letsfarm.letsfarm.auth.model.AppVersion
import co.letsfarm.letsfarm.auth.model.*
import co.letsfarm.letsfarm.common.utils.AbsentLiveData
import co.letsfarm.letsfarm.utils.Resource
import co.letsfarm.letsfarm.utils.BaseViewModel
import kotlinx.coroutines.launch
import java.util.HashMap


class AuthViewModel(val repository: AuthRespository, val app: Application): BaseViewModel(repository, app){

    val loginData = LoginData().apply {
//        email = "moyoolasupo02@gmail.com"
//        password = "Napster123"
    }

    private val loginDataLive= MutableLiveData<LoginData>()

    fun login(){
        loginDataLive.value=loginData
    }

    val loginRequest: LiveData<Resource<LoginResData>> = loginDataLive.switchMap { loginData->
        if (loginData != null) {
            viewModelScope.launch{
            }
            repository.login(loginData)
        }else{
            AbsentLiveData.create()
        }
    }

    val signUpData = SignUpData().apply {
//        email = "akano.adekola@gmail.com"
//        lastName = "Akano"
//        firstName = "Adekola"
//        phoneNumber = "08099411303"
//        password = "kkkkkk"
    }

    private val signUpLive= MutableLiveData<SignUpData>()

    fun signUp() {
        signUpLive.value = signUpData
    }

    val signUpRequest: LiveData<Resource<SignUpResData>> = signUpLive.switchMap { loginData->
        if (loginData != null) {
            viewModelScope.launch{
            }
            repository.signUp(signUpData)
        }else{
            AbsentLiveData.create()
        }
    }

    private val forgotPasswordLive= MutableLiveData<HashMap<String, String>>()

    var forgotPasswordEmail:String?=null

    fun forgotPassword() {
        val request = HashMap<String, String>()
        request["email"] = forgotPasswordEmail!!
        forgotPasswordLive.value = request
    }

    val forgotPasswordRequest: LiveData<Resource<Any>> = forgotPasswordLive.switchMap { value->
        if (value != null) {
            repository.forgotPassword(value)
        }else{
            AbsentLiveData.create()
        }
    }


    private val resetPasswordLive= MutableLiveData<ResetPasswordData>()

    var resetPasswordData = ResetPasswordData()

    fun resetPassword() {
        resetPasswordLive.value = resetPasswordData
    }

    val resetPasswordRequest: LiveData<Resource<Any>> = resetPasswordLive.switchMap { value->
        if (value != null) {
            repository.resetPassword(value)
        }else{
            AbsentLiveData.create()
        }
    }

    private val verifyUserLive= MutableLiveData<HashMap<String, String>>()

    fun verifyUser(request: HashMap<String, String>) {
        verifyUserLive.value = request
    }

    val verifyUserRequest: LiveData<Resource<Any>> = verifyUserLive.switchMap { value->
        if (value != null) {
            repository.verifyUser(value)
        }else{
            AbsentLiveData.create()
        }
    }

    private val resendActivationLinkLive= MutableLiveData<String>()

    fun resendActivationLink() {
        resendActivationLinkLive.value = loginData.email
    }

    val resendActivationLinkRequest: LiveData<Resource<Any>> = resendActivationLinkLive.switchMap { value->
        if (value != null) {
            repository.resendActivationLink(value)
        }else{
            AbsentLiveData.create()
        }
    }

    private val appVersionLive= MutableLiveData<Boolean>()

    fun appVersion() {
        appVersionLive.value = true
    }

    val appVersionRequest: LiveData<Resource<AppVersion>> = appVersionLive.switchMap { value->
        if (value != null) {
            repository.appVersion()
        }else{
            AbsentLiveData.create()
        }
    }
}
