package co.letsfarm.letsfarm.auth.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import co.letsfarm.letsfarm.dashBoard.model.GeneralRes
import co.letsfarm.letsfarm.utils.*
import co.letsfarm.letsfarm.utils.Constants.Companion.EMPTY_RESPONSE
import co.letsfarm.letsfarm.utils.Constants.Companion.NO_INTERNET_RESPONSE
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

abstract class NetworkResource<ResultType,RequestType> (isNetworkAvaliable:Boolean){
    private val result= MediatorLiveData<Resource<ResultType>>()

    init {
        result.postValue(Resource.Loading())
        if (isNetworkAvaliable){ GlobalScope.launch(Main) {
            withContext(Main){
                fetchFromApi()
            }
        }
        }else{
            result.postValue(Resource.Error("check your internet connection", "", NO_INTERNET_RESPONSE))
        }
    }

    private fun fetchFromApi(){
        val apiResponse=fetchService()
        result.addSource(apiResponse){response->
            result.removeSource(apiResponse)
            response?.let { res->
                when(res){
                    is ApiEmptyResponse -> {
                        setValue(Resource.Error("auth-token expired", "", EMPTY_RESPONSE))
                    }
                    is ApiSuccessResponse -> {
                        setValue(Resource.Success(res.body.msg, res.body.data, res.responseCode))
                    }
                    is ApiErrorResponse -> {
                        setValue(Resource.Error(res.errorMessage, res.status, res.responseCode))
                    }
                }
            }
        }
    }

    // Called to create the API call.
    protected abstract  fun fetchService(): LiveData<ApiResponse<GeneralRes<ResultType>>>

    fun asLiveData(): LiveData<Resource<ResultType>> {
        return result
    }
    private fun setValue(newValue: Resource<ResultType>) {
        result.value = newValue
    }
}