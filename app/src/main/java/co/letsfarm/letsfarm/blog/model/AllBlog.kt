package co.letsfarm.letsfarm.blog.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AllBlog(

	@field:SerializedName("total")
	val total: Int? = null,

	@field:SerializedName("data")
	val data: List<BlogData?>? = null,

	@field:SerializedName("limit")
	val limit: Int? = null,

	@field:SerializedName("page")
	val page: Int? = null
) : Parcelable

@Parcelize
data class BlogData(

	@field:SerializedName("top_post")
	val topPost: Boolean? = null,

	@field:SerializedName("blog_category")
	val blogCategory: BlogCategory? = null,

	@field:SerializedName("img_link")
	val imgLink: String? = null,

	@field:SerializedName("author")
	val author: String? = null,

	@field:SerializedName("total_likes")
	val totalLikes: Int? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("total_dislikes")
	val totalDislikes: Int? = null,

	@field:SerializedName("large_img_link")
	val largeImgLink: String? = null,

	@field:SerializedName("deleted")
	val deleted: Boolean? = null,

	@field:SerializedName("message_body")
	val messageBody: String? = null,

	@field:SerializedName("comments_count")
	val commentsCount: Int? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
) : Parcelable

@Parcelize
data class BlogCategory(

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("deleted")
	val deleted: Boolean? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
) : Parcelable
