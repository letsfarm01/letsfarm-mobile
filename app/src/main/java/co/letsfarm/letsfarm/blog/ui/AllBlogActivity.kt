package co.letsfarm.letsfarm.blog.ui

import android.app.Application
import android.os.Build
import android.os.Bundle
import android.text.Html
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.blog.model.AllBlog
import co.letsfarm.letsfarm.blog.model.BlogData
import co.letsfarm.letsfarm.blog.viewModel.AllBlogRespository
import co.letsfarm.letsfarm.blog.viewModel.AllBlogViewModel
import co.letsfarm.letsfarm.utils.*
import com.squareup.picasso.Picasso
import dagger.Component
import dagger.Module
import dagger.Provides
import kotlinx.android.synthetic.main.activity_all_blog.*
import kotlinx.android.synthetic.main.activity_all_blog.progress
import kotlinx.android.synthetic.main.fragment_running_farms.*
import kotlinx.android.synthetic.main.holder_blog.view.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import javax.inject.Inject
import javax.inject.Singleton

enum class BlogOrEducation{
    BLOG,EDUCATION
}

class AllBlogActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModel:AllBlogViewModel

    lateinit var blogOrEducation: BlogOrEducation

    var allBlogs: MutableList<BlogData?> = mutableListOf()

    lateinit var adapter:GenericListAdapter<BlogData?>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_all_blog)

        DaggerAllBlogComponent
            .builder()
            .allBlogModule(AllBlogModule(application))
            .build()
            .inject(this)

        intent.extras?.getString("blogOrEducation")?.let {
            blogOrEducation = BlogOrEducation.valueOf(it)
        }?:run{
            onBackPressed()
        }

        setViews()
        setObservers()
    }

    private fun setObservers() {
        viewModel.fetchAllBlog(blogOrEducation)

        if (blogOrEducation == BlogOrEducation.BLOG)
            viewModel.fetchTopBlog()

        viewModel.allBlogRequest.observe(this, Observer {resource->
            progress.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success->{
                    resource.data?.data?.let{
                        allBlogs.addAll(it)
                        adapter.notifyDataSetChanged()
                    }
                }
                is Resource.Error->{
                    processError(resource.message, resource.responseCode)
                }
            }
        })

        viewModel.topBlogRequest.observe(this, Observer {resource->
            progress.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success->{
                    resource.data?.let{
                        allBlogs.add(0, it)
                        adapter.notifyDataSetChanged()
                    }
                }
                is Resource.Error->{
                    processError(resource.message, resource.responseCode)
                }
            }
        })
    }


    private fun setAdapter() {
        adapter = GenericListAdapter(allBlogs, R.layout.holder_blog) { view, item, position ->
            view.title_tv.text = item?.title
            view.like_tv.text = "${item?.totalLikes ?: 0}"
            view.comment_tv.text = "${item?.commentsCount ?: 0}"

            item?.imgLink?.let {imggUrl->
                Picasso.get().load(imggUrl).into(view.blog_img)
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                view.content_tv.text = Html.fromHtml(item?.messageBody, Html.FROM_HTML_MODE_COMPACT)
            } else {
                view.content_tv.text = Html.fromHtml(item?.messageBody)
            }

            view.setOnClickListener {
                navigateTo<BlogActivity>{
                    putExtra("blogOrEducation", blogOrEducation.toString())
                    putExtra("blog", item)
                }
            }
        }

        blog_rv.adapter = adapter
    }

    private fun setViews() {
        toolbar_title_tv.text = if (blogOrEducation == BlogOrEducation.BLOG) "Blog" else "Education"
        back_img.setOnClickListener { onBackPressed() }
        setAdapter()
    }

}

@Singleton
@Component(modules = [AllBlogModule::class])
interface AllBlogComponent {
    fun inject(activity: AllBlogActivity)
}

@Module
class AllBlogModule(private val application: Application) : NetModule() {

    @Provides
    fun provideAllBlogViewModel(repo: AllBlogRespository) : AllBlogViewModel {
        return AllBlogViewModel(repo, application)
    }

    @Provides
    fun provideRepository(service: ApiServices) : AllBlogRespository {
        return AllBlogRespository(service, application)
    }
}