package co.letsfarm.letsfarm.blog.ui

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import androidx.core.text.HtmlCompat
import androidx.lifecycle.lifecycleScope
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.blog.model.BlogData
import co.letsfarm.letsfarm.utils.HtmlImageGetter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_blog.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class BlogActivity : AppCompatActivity() {

    lateinit var blogData: BlogData

    lateinit var blogOrEducation: BlogOrEducation

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_blog)

        intent.extras?.getString("blogOrEducation")?.let {
            blogOrEducation = BlogOrEducation.valueOf(it)
        }?:run{
            onBackPressed()
        }

        intent.extras?.getParcelable<BlogData>("blog")?.let{ data->
            this.blogData = data
            setViews()
        }?:run{
            onBackPressed()
        }
    }

    private fun setViews() {
        toolbar_title_tv.text = if (blogOrEducation == BlogOrEducation.BLOG) "Blog" else "Education"
        back_img.setOnClickListener { onBackPressed() }

        author_tv.text = "by ${blogData.author}"
        like_tv.text = "${blogData.totalLikes}"
        comment_tv.text = "${blogData.commentsCount}"
        title_tv.text = blogData.title

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            val imageGetter = HtmlImageGetter(lifecycleScope, resources, Picasso.get(), content_tv)
            val styledText = HtmlCompat.fromHtml(blogData.messageBody!!,
                HtmlCompat.FROM_HTML_SEPARATOR_LINE_BREAK_HEADING, imageGetter, null)
            content_tv.text = styledText
        } else {
            content_tv.text = Html.fromHtml(blogData.messageBody)
        }

        Picasso.get().load(blogData.largeImgLink).into(blog_img)

    }

}