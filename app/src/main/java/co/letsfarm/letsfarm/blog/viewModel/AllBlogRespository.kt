package co.letsfarm.letsfarm.blog.viewModel

import android.app.Application
import androidx.lifecycle.LiveData
import co.letsfarm.letsfarm.auth.viewModel.NetworkResource
import co.letsfarm.letsfarm.blog.model.AllBlog
import co.letsfarm.letsfarm.blog.model.BlogData
import co.letsfarm.letsfarm.blog.ui.BlogOrEducation
import co.letsfarm.letsfarm.dashBoard.model.GeneralRes
import co.letsfarm.letsfarm.utils.*

class AllBlogRespository(val apiServices: ApiServices, val application: Application) : BaseRepositiory(apiServices, application) {

    fun fetchAllBlog(blogOrEducation: BlogOrEducation, page:Int = 1): LiveData<Resource<AllBlog>> {
        return object : NetworkResource<AllBlog, AllBlog>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<AllBlog>>> {
                return if (blogOrEducation == BlogOrEducation.BLOG) {
                    val url = "https://api.letsfarm.com.ng/api/v1/blog/normal-posts?limit=100&&page=$page"
                    apiServices.fetchAllBlog(url)
                }
                else
                    apiServices.fetchAllBlog("https://api.letsfarm.com.ng/api/v1/education/publish?limit=100&&page=$page")
            }

        }.asLiveData()
    }

    fun fetchTopBlog(): LiveData<Resource<BlogData>> {
        return object : NetworkResource<BlogData, BlogData>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<BlogData>>> {
                return apiServices.fetchTopBlog()
            }
        }.asLiveData()
    }
}