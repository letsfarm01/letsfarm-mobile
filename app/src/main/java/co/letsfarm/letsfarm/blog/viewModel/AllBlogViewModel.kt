package co.letsfarm.letsfarm.blog.viewModel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.switchMap
import co.letsfarm.letsfarm.blog.model.AllBlog
import co.letsfarm.letsfarm.blog.model.BlogData
import co.letsfarm.letsfarm.blog.ui.BlogOrEducation
import co.letsfarm.letsfarm.common.utils.AbsentLiveData
import co.letsfarm.letsfarm.utils.BaseViewModel
import co.letsfarm.letsfarm.utils.Resource

class AllBlogViewModel(val repository: AllBlogRespository, val app: Application): BaseViewModel(repository, app){

    private val allBlogDataLive= MutableLiveData<BlogOrEducation>()

    fun fetchAllBlog(blogOrEducation: BlogOrEducation) {
        allBlogDataLive.value=blogOrEducation
    }

    val allBlogRequest: LiveData<Resource<AllBlog>> = allBlogDataLive.switchMap { data->
        if (data != null) {
            repository.fetchAllBlog(data)
        }else{
            AbsentLiveData.create()
        }
    }
    private val topBlogDataLive= MutableLiveData<Boolean>()

    fun fetchTopBlog() {
        topBlogDataLive.value=true
    }

    val topBlogRequest: LiveData<Resource<BlogData>> = topBlogDataLive.switchMap { data->
        if (data != null) {
            repository.fetchTopBlog()
        }else{
            AbsentLiveData.create()
        }
    }
}