package co.letsfarm.letsfarm.dashBoard.model

import com.google.gson.annotations.SerializedName

data class AllFarmsResData(

	@field:SerializedName("total")
	val total: Int? = null,

	@field:SerializedName("data")
	val data: List<AllFarmsDataItem?>? = null,

	@field:SerializedName("limit")
	val limit: Int? = null,

	@field:SerializedName("page")
	val page: Int? = null
)

data class AllFarmsDataItem(

	@field:SerializedName("percentage_to_gain")
	val percentageToGain: Int? = null,

	@field:SerializedName("closing_date")
	val closingDate: String? = null,

	@field:SerializedName("duration_type")
	val durationType: String? = null,

	@field:SerializedName("remaining_in_stock")
	val remainingInStock: Int? = null,

	@field:SerializedName("amount_to_invest")
	val amountToInvest: Int? = null,

//	@field:SerializedName("description")
//	val description: String? = null,

	@field:SerializedName("farm_location")
	val farmLocation: String? = null,

	@field:SerializedName("farm_type")
	val farmType: Any? = null,

	@field:SerializedName("duration")
	val duration: Int? = null,

	@field:SerializedName("total_in_stock")
	val totalInStock: Int? = null,

	@field:SerializedName("farm_status")
	val farmStatus: FarmStatus? = null,

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("deleted")
	val deleted: Boolean? = null,

	@field:SerializedName("display_img")
	val displayImg: String? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("currency")
	val currency: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("opening_date")
	val openingDate: String? = null,

	@field:SerializedName("farm_name")
	val farmName: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)

data class FarmType(

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("deleted")
	val deleted: Boolean? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("status")
	val status: Boolean? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)

data class FarmStatus(

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("deleted")
	val deleted: Boolean? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("can_sponsor")
	val canSponsor: Boolean? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("status")
	val status: Boolean? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)
