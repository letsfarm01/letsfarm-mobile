package co.letsfarm.letsfarm.dashBoard.model

import com.google.gson.annotations.SerializedName

data class CardPaymentCharges(

	@field:SerializedName("total")
	val total: Double? = null,

	@field:SerializedName("charge")
	val charge: Double? = null,

	@field:SerializedName("subTotal")
	val subTotal: Int? = null
)
