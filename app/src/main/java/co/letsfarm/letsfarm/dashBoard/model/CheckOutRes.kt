package co.letsfarm.letsfarm.dashBoard.model

import com.google.gson.annotations.SerializedName

data class CheckOutRes(

	@field:SerializedName("authorization_url")
	val authorizationUrl: String? = null,

	@field:SerializedName("transactionRef")
	val transactionRef: String? = null,

	@field:SerializedName("transactionId")
	val transactionId: String? = null,

	@field:SerializedName("amount")
	val amount: Int? = null,

	@field:SerializedName("details")
	val details: Details? = null
)

data class Details(

	@field:SerializedName("ref")
	val ref: String? = null,

	@field:SerializedName("full_name")
	val fullName: String? = null,

	@field:SerializedName("access_code")
	val accessCode: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("transactionId")
	val transactionId: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("total")
	val total: Double? = null,

	@field:SerializedName("charge")
	val charge: Double? = null,

	@field:SerializedName("subTotal")
	val subTotal: Int? = null
)
