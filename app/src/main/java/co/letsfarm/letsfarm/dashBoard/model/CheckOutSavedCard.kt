package co.letsfarm.letsfarm.dashBoard.model

import com.google.gson.annotations.SerializedName

data class CheckOutSavedCard(

	@field:SerializedName("payment_gateway")
	var paymentGateway: String? = null,

	@field:SerializedName("userId")
	var userId: String? = null,

	@field:SerializedName("card_id")
	var cardId: String? = null,

	@field:SerializedName("shipping_address")
	var shippingAddress: String? = null
)
