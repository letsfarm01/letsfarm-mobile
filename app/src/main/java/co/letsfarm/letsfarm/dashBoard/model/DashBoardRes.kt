package co.letsfarm.letsfarm.dashBoard.model

import com.google.gson.annotations.SerializedName
import java.util.*

data class DashBoardRes(

//	@field:SerializedName("wallet")
//	val wallet: Wallet? = null,
//
//	@field:SerializedName("total_investment")
//	val totalInvestment: TotalInvestment? = null,

	@field:SerializedName("active_sponsorship")
	val activeSponsorship: DashBoardResActiveSponsorship? = null,

//	@field:SerializedName("total_returns")
//	val totalReturns: TotalReturns? = null,

	@field:SerializedName("expected_returns")
	val expectedReturns: ExpectedReturns? = null

//	@field:SerializedName("withdraw")
//	val withdraw: Withdraw? = null
)

data class ExpectedReturns(

	@field:SerializedName("date")
	val date: Long? = null,

	@field:SerializedName("total")
	val total: Int? = null,

	@field:SerializedName("number_of_farms")
	val numberOfFarms: Int? = null,

	@field:SerializedName("names")
	val names: List<String?>? = null,

	@field:SerializedName("description")
	val description: List<String?>? = null,

	@field:SerializedName("returns")
	val returns: List<Int?>? = null,

	@field:SerializedName("currency")
	val currency: String? = null,

	@field:SerializedName("interests")
	val interests: List<Int?>? = null,

	@field:SerializedName("invested")
	val invested: List<Int?>? = null,

	@field:SerializedName("spike")
	val spike: List<Int?>? = null
)

data class DashBoardResActiveSponsorship(

	@field:SerializedName("duration")
	val duration: Int? = null,

	@field:SerializedName("date")
	val date: Long? = null,

	@field:SerializedName("end_date")
	val endDate: Date? = null,

	@field:SerializedName("total")
	val total: Int? = null,

	@field:SerializedName("interest")
	val interest: Int? = null,

	@field:SerializedName("duration_type")
	val durationType: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("currency")
	val currency: String? = null,

	@field:SerializedName("roi")
	val roi: Int? = null,

	@field:SerializedName("start_date")
	val startDate: String? = null,

	@field:SerializedName("spike")
	val spike: List<Int?>? = null
)

data class Withdraw(

	@field:SerializedName("date")
	val date: Long? = null,

	@field:SerializedName("total")
	val total: Int? = null,

	@field:SerializedName("currency")
	val currency: String? = null,

	@field:SerializedName("spike")
	val spike: List<Int?>? = null
)

data class TotalReturns(

	@field:SerializedName("date")
	val date: Long? = null,

	@field:SerializedName("total")
	val total: Int? = null,

	@field:SerializedName("currency")
	val currency: String? = null,

	@field:SerializedName("spike")
	val spike: List<Any?>? = null
)

data class Wallet(

	@field:SerializedName("date")
	val date: Long? = null,

	@field:SerializedName("wallet_balance")
	val walletBalance: Int? = null,

	@field:SerializedName("currency")
	val currency: String? = null,

	@field:SerializedName("spike")
	val spike: List<Int?>? = null
)

data class TotalInvestment(

	@field:SerializedName("date")
	val date: Long? = null,

	@field:SerializedName("total")
	val total: Int? = null,

	@field:SerializedName("currency")
	val currency: String? = null,

	@field:SerializedName("spike")
	val spike: List<Int?>? = null
)
