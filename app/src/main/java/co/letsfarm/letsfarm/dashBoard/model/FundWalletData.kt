package co.letsfarm.letsfarm.dashBoard.model

import com.google.gson.annotations.SerializedName

data class FundWalletData(

	@field:SerializedName("amount")
	var amount: String? = null,

	@field:SerializedName("payment_gateway")
	var paymentGateway: String? = null,

	@field:SerializedName("card_id")
	var cardId: String? = null,

	@field:SerializedName("userId")
	var userId: String? = null
)
