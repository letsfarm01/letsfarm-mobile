package co.letsfarm.letsfarm.dashBoard.model

import com.google.gson.annotations.SerializedName

data class GeneralRes<T>(

	@field:SerializedName("data")
	val data: T? = null,

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("status")
	val status: String? = null
)