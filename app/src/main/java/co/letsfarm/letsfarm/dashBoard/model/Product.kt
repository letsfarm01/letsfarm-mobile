package co.letsfarm.letsfarm.dashBoard.model

import com.google.gson.annotations.SerializedName

data class Product(

	@field:SerializedName("amount")
	val amount: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("imagename")
	val imagename: String? = null,

	@field:SerializedName("location")
	val location: String? = null
)
