package co.letsfarm.letsfarm.dashBoard.model

import com.google.gson.annotations.SerializedName

data class Referral(

	@field:SerializedName("has_joined")
	val hasJoined: Boolean? = null,

	@field:SerializedName("amount_sponsor")
	val amountSponsor: Int? = null,

	@field:SerializedName("is_sponsor_valid")
	val isSponsorValid: Boolean? = null,

	@field:SerializedName("referral_email")
	val referralEmail: String? = null,

	@field:SerializedName("has_sponsor")
	val hasSponsor: Boolean? = null,

	@field:SerializedName("is_cycle_completed")
	val isCycleCompleted: Boolean? = null,

	@field:SerializedName("is_bonus_earned")
	val isBonusEarned: Boolean? = null,

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("deleted")
	val deleted: Boolean? = null,

	@field:SerializedName("referral")
	val referral: Referral? = null,

	@field:SerializedName("percentage")
	val percentage: Int? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("currency")
	val currency: String? = null,

	@field:SerializedName("expected_bonus")
	val expectedBonus: Int? = null,

	@field:SerializedName("_id")
	val _id: String? = null,

	@field:SerializedName("referree_email")
	val referreeEmail: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null,

	@field:SerializedName("country")
	val country: String? = null,

	@field:SerializedName("auth_type")
	val authType: String? = null,

	@field:SerializedName("role")
	val role: String? = null,

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("gender")
	val gender: String? = null,

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("last_name")
	val lastName: String? = null,

	@field:SerializedName("is_auth_sign_up")
	val isAuthSignUp: Boolean? = null,

	@field:SerializedName("state_province")
	val stateProvince: String? = null,

	@field:SerializedName("is_verified")
	val isVerified: Boolean? = null,

	@field:SerializedName("full_name")
	val fullName: String? = null,

	@field:SerializedName("dob")
	val dob: String? = null,

	@field:SerializedName("referral_code")
	val referralCode: String? = null,

	@field:SerializedName("phone_number")
	val phoneNumber: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("migrated")
	val migrated: Boolean? = null,

	@field:SerializedName("referral_done")
	val referralDone: Boolean? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null
)
