package co.letsfarm.letsfarm.dashBoard.model

import com.google.gson.annotations.SerializedName

data class SavedCard(

	@field:SerializedName("last4")
	val last4: String? = null,

	@field:SerializedName("signature")
	val signature: String? = null,

	@field:SerializedName("bin")
	val bin: String? = null,

	@field:SerializedName("channel")
	val channel: String? = null,

	@field:SerializedName("exp_month")
	val expMonth: Int? = null,

	@field:SerializedName("exp_year")
	val expYear: Int? = null,

	@field:SerializedName("card_type")
	val cardType: String? = null,

	@field:SerializedName("userId")
	val userId: String? = null,

	@field:SerializedName("reusable")
	val reusable: Boolean? = null,

	@field:SerializedName("country_code")
	val countryCode: String? = null,

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("bank")
	val bank: String? = null,

	@field:SerializedName("deleted")
	val deleted: Boolean? = null,

	@field:SerializedName("authorization_code")
	val authorizationCode: String? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)
