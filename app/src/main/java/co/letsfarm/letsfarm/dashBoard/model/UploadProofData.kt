package co.letsfarm.letsfarm.dashBoard.model

import com.google.gson.annotations.SerializedName

data class UploadProofData(

	@field:SerializedName("amount")
	var amount: Int? = null,

	@field:SerializedName("documents")
	var documents: MutableList<Document>? = mutableListOf(),

	@field:SerializedName("bank_name")
	var bankName: String? = null,

	@field:SerializedName("transactionRef")
	var transactionRef: String? = null,

	@field:SerializedName("message")
	var message: String? = null,

	@field:SerializedName("userId")
	var userId: String? = null,

	@field:SerializedName("transactionId")
	var transactionId: String? = null
)

data class Document(
	@field:SerializedName("url")
	var url: String
)
