package co.letsfarm.letsfarm.dashBoard.model

import com.google.gson.annotations.SerializedName

class WithdrawData(
    @field:SerializedName("amount")
    var amount: String? = null,

    @field:SerializedName("userId")
    var userId: String? = null,

    @field:SerializedName("password")
    var password: String? = null
)