package co.letsfarm.letsfarm.dashBoard.ui

import android.os.Bundle
import co.letsfarm.letsfarm.BaseActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import br.com.ilhasoft.support.validation.Validator
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.databinding.ActivityAccountManagerBinding
import co.letsfarm.letsfarm.ui.DashBoardModule
import co.letsfarm.letsfarm.ui.ui.dashboard.DashboardViewModel
import co.letsfarm.letsfarm.utils.Resource
import co.letsfarm.letsfarm.utils.processError

import co.letsfarm.letsfarm.utils.trimmedText
import dagger.Component
import kotlinx.android.synthetic.main.activity_account_manager.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.collections.HashMap
import kotlin.collections.set

class AccountManagerActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: DashboardViewModel
    lateinit var binding: ActivityAccountManagerBinding
    lateinit var validator: Validator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,
            R.layout.activity_account_manager
        )
        validator = Validator(binding)

        DaggerAccountManagerComponent.builder()
            .dashBoardModule(DashBoardModule(application))
            .build()
            .inject(this)

        setViews()
        setObservers()
    }

    private fun setObservers() {
        viewModel.submitMessageRequest.observe(this, Observer {resource->
            submit_btn.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success->{
                    processError(resource.message, resource.responseCode)
                    onBackPressed()
                }
                is Resource.Error->{
                    processError(resource.message, resource.responseCode)
                }
            }
        })
    }

    private fun setViews() {
        toolbar_title_tv.text = "Account Manager"
        back_img.setOnClickListener { onBackPressed() }

        submit_btn.setOnClickListener {
            if (validator.validate()){

                val request = HashMap<String, String>()
                request["topic"] = topic_et.trimmedText
                request["message"] = message_et.trimmedText

                viewModel.submitMessageInfo(request)
            }
        }

    }

}

@Singleton
@Component(modules = [DashBoardModule::class])
interface AccountManagerComponent {
    fun inject(activity: AccountManagerActivity)
}
