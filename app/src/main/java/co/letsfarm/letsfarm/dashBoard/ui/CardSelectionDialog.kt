package co.letsfarm.letsfarm.dashBoard.ui

import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.allFarms.ui.CartActivity
import co.letsfarm.letsfarm.dashBoard.model.SavedCard
import co.letsfarm.letsfarm.product.ui.ProductCartActivity
import co.letsfarm.letsfarm.ui.BottomNavActivity
import co.letsfarm.letsfarm.ui.ui.dashboard.DashboardViewModel
import co.letsfarm.letsfarm.utils.BaseViewModel
import co.letsfarm.letsfarm.utils.Resource
import co.letsfarm.letsfarm.utils.load
import co.letsfarm.letsfarm.utils.processError

import kotlinx.android.synthetic.main.activity_dash_board2.*
import kotlinx.android.synthetic.main.fragment_card_selection_dialog.*
import kotlinx.android.synthetic.main.fragment_card_selection_dialog.progress
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

enum class CardSelectionOrigin{
    CART,DASHBOARD, PRODUCT
}

class CardSelectionDialog(val origin:CardSelectionOrigin, val paymentCardListener:(String?)->Unit) : DialogFragment() {

    private lateinit var viewModel: BaseViewModel

    var cards:List<SavedCard>? = null

    var adapter = CardsAdapter{
        dismiss()
        paymentCardListener(it)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        viewModel = when(origin){
            CardSelectionOrigin.CART-> (activity as CartActivity).viewModel
            CardSelectionOrigin.DASHBOARD-> (activity as BottomNavActivity).viewModel
            CardSelectionOrigin.PRODUCT-> (activity as ProductCartActivity).viewModel
        }


        return inflater.inflate(R.layout.fragment_card_selection_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.fetchCards()

        setViews()

        setObservers()

    }

    private fun setViews() {
        back_img.setOnClickListener { dismiss() }

        new_card_tv.setOnClickListener {
            paymentCardListener(null)
            dismiss()
        }

        cards_rv.adapter = adapter
    }

    private fun setObservers() {
        viewModel.fetchCardsRequest.observe(viewLifecycleOwner, Observer {resource->
            progress.load(resource is Resource.Loading)

            when(resource){
                is Resource.Success->{
                    resource.data?.let {
                        if (it.isEmpty()){
                            no_card_tv.visibility = View.VISIBLE
                        }else {
                            adapter.setData(it)
                        }
                    }?:run{
                        no_card_tv.visibility = View.VISIBLE
                    }

                }
                is Resource.Error->{
                    activity?.processError(resource.message, resource.responseCode)
                }
            }

        })
    }


    override fun onStart() {
        super.onStart()
        val dialog: Dialog? = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            dialog.window?.setLayout(width, height)
        }
    }
}