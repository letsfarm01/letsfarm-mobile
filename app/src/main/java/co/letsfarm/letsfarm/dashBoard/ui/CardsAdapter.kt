package co.letsfarm.letsfarm.dashBoard.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.dashBoard.model.SavedCard
import kotlinx.android.synthetic.main.holder_cards.view.*


class CardsAdapter(val cardSelectedListener:(String)->Unit) : RecyclerView.Adapter<CardsVH>() {

    var cards:List<SavedCard>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardsVH {
        LayoutInflater.from(parent.context).inflate(R.layout.holder_cards, parent, false).run {
            return CardsVH(this)
        }
    }

    override fun getItemCount()=cards?.size ?: 0

    override fun onBindViewHolder(holder: CardsVH, position: Int) {

        holder.itemView.run {
            cards?.get(position)?.let {card->
                card_no_tv.text = "${card.bin} **** **** **** ${card.last4}"
                card_type_tv.text = "${card.cardType}"
                setOnClickListener { cardSelectedListener("${card.id}") }
            }
        }
    }

    fun setData(cards:List<SavedCard>) {
        this.cards = cards
        notifyDataSetChanged()
    }

}

class CardsVH(itemView: View): RecyclerView.ViewHolder(itemView)
