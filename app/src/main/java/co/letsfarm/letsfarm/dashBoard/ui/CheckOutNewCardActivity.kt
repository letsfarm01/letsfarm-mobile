package co.letsfarm.letsfarm.dashBoard.ui

import android.app.Application
import co.letsfarm.letsfarm.BaseActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import br.com.ilhasoft.support.validation.Validator
import co.letsfarm.letsfarm.BuildConfig
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.allFarms.viewModel.AllFarmsRespository
import co.letsfarm.letsfarm.allFarms.viewModel.AllFarmsViewModel
import co.letsfarm.letsfarm.allFarms.viewModel.CardTransactions
import co.letsfarm.letsfarm.databinding.ActivityCheckOutNewCardBinding
import co.letsfarm.letsfarm.ui.BottomNavActivity
import co.letsfarm.letsfarm.utils.*
import co.paystack.android.Paystack
import co.paystack.android.PaystackSdk
import co.paystack.android.Transaction
import co.paystack.android.exceptions.InvalidEmailException
import co.paystack.android.model.Card
import co.paystack.android.model.Charge
import dagger.Component
import dagger.Module
import dagger.Provides
import kotlinx.android.synthetic.main.activity_check_out_new_card.*
import kotlinx.android.synthetic.main.activity_check_out_new_card.total_price_tv
import kotlinx.android.synthetic.main.layout_toolbar.*
import javax.inject.Inject
import javax.inject.Singleton

class CheckOutNewCardActivity : BaseActivity(), TextWatcher, Paystack.TransactionCallback  {

    lateinit var ref:String
    var cardTransactions: CardTransactions? = null
    lateinit var accessCode:String
    lateinit var transactionId:String
    var totalPrice:Double = 0.0

    @Inject
    lateinit var viewModel: AllFarmsViewModel
    lateinit var binding: ActivityCheckOutNewCardBinding
    lateinit var validator: Validator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_check_out_new_card)

        PaystackSdk.initialize(applicationContext);
        PaystackSdk.setPublicKey(BuildConfig.PAYSTACK_KEY);

        DaggerCheckOutNewCardComponent
            .builder()
            .checkOutNewCardModule(CheckOutNewCardModule(application))
            .build()
            .inject(this)

        validator = Validator(binding)

        intent.extras?.getString("cardTransactions")?.let {
            cardTransactions = CardTransactions.valueOf(it)
        }

        totalPrice = intent.extras?.getDouble("totalPrice")!!
        total_price_tv.text = totalPrice.getMoney
        ref = intent.extras?.getString("ref")!!
        accessCode = intent.extras?.getString("accessCode")!!
        transactionId = intent.extras?.getString("transactionId")!!
        setViews()
        setObservers()
    }

    private fun setObservers() {
        viewModel.verifyTransactionRequest.observe(this, Observer { resource->
            save_card_btn.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success->{
                    navigateToNew<BottomNavActivity>()
                    processError(resource.message, resource.responseCode)
                }
                is Resource.Error->{
                    navigateToNew<BottomNavActivity>()
                    processError("Unable to verify transaction, contact admin", resource.responseCode)
                }
            }
        })
    }

    private fun setViews() {
        toolbar_title_tv.text = "Card details"
        back_img.setOnClickListener { onBackPressed() }

        save_card_btn.setOnClickListener {
            if (validator.validate()){
                addCard()
            }
        }

        if (isDebug) {
//            card_number_et.setText("4084084084084081")
//            exp_month_et.setText("11")
//            exp_year_et.setText("202")
//            cvv_et.setText("408")
            card_number_et.setText("5060666666666666666")
            exp_month_et.setText("11")
            exp_year_et.setText("202")
            cvv_et.setText("123")
        }

        card_number_et.addTextChangedListener(this)
        exp_month_et.addTextChangedListener(this)
        exp_year_et.addTextChangedListener(this)
        cvv_et.addTextChangedListener(this)
    }
    private fun addCard() {
        val cardNumber = card_number_et.trimmedText
        val expYear = exp_year_et.trimmedText.toInt()
        val expMonth = exp_month_et.trimmedText.toInt()
        val cvv = cvv_et.trimmedText

        val card = Card(cardNumber, expMonth, expYear, cvv)

        val charge = Charge()

        val email = savepref().get("userEmail", "")

        charge.card = card
        try{
            charge.email = email
        }
        catch(e: InvalidEmailException){
            showErrorDialog("Invalid email")
            return
        }

        log("ref $ref")
        charge.accessCode = accessCode
        charge.reference = ref
//        showToast("----${totalPrice.toInt()}")
        charge.amount = totalPrice.toInt()

        if (card.isValid) {
            save_card_btn.load(true)
            PaystackSdk.chargeCard(this, charge, this)
        } else {
            log("4")
            showErrorDialog("Your card details are incorrect. Kindly check again")
        }
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        if (this.card_number_et?.hasFocus() == true) {
            val value = this.card_number_et?.trimmedText
            if (count > before) {
                this.card_number_et?.removeTextChangedListener(this)
                val actualCount = value!!.replace(" ", "").length
                this.card_number_et?.setText(if (actualCount % 4 == 0) "$value " else value)
                this.card_number_et?.setSelection(this.card_number_et!!.text!!.length)
                this.card_number_et?.addTextChangedListener(this)
                if (actualCount == 16) {
                    this.exp_month_et?.requestFocus()
                }
            }
        } else if (this.exp_month_et?.hasFocus() == true) {
            val value = this.exp_month_et?.trimmedText
            if (count > before && value!!.trim { it <= ' ' }.length == 2) {
                this.exp_year_et?.requestFocus()
            }
        } else if (this.exp_year_et?.hasFocus() == true) {
            val year = exp_year_et?.trimmedText
            if (count > before && year!!.length == 4) {
                this.cvv_et?.requestFocus()
            }
        } else if (this.cvv_et?.hasFocus() == true) {
            val value = this.cvv_et?.text.toString()
            if (value.length == 3) {
                HideSoftKeyboard()
            }
        }
    }

    override fun afterTextChanged(s: Editable) {

    }

    override fun onSuccess(transaction: Transaction?) {
        val transactionRef = transaction?.reference

        viewModel.verifyTransaction(transactionId, cardTransactions!!)

    }

    override fun beforeValidate(transaction: Transaction?) {
    }

    override fun onError(error: Throwable?, transaction: Transaction?) {
        save_card_btn.load(false)
        showErrorDialog(error?.message ?: "An error occurred" )
    }
}

@Singleton
@Component(modules = [CheckOutNewCardModule::class])
interface CheckOutNewCardComponent {
    fun inject(activity: CheckOutNewCardActivity)
}

@Module
class CheckOutNewCardModule(private val application: Application) : NetModule() {

    @Provides
    fun provideAllFarmsViewModel(repo: AllFarmsRespository) : AllFarmsViewModel {
        return AllFarmsViewModel(repo, application)
    }

    @Provides
    fun provideRepository(service: ApiServices) : AllFarmsRespository {
        return AllFarmsRespository(service, application)
    }
}