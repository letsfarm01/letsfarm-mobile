package co.letsfarm.letsfarm.dashBoard.ui

data class DashboardMenuItem (
    var title:String?=null,
    var image:Int?=null
)