package co.letsfarm.letsfarm.dashBoard.ui

import co.letsfarm.letsfarm.BaseActivity
import android.os.Bundle
import co.letsfarm.letsfarm.R
import kotlinx.android.synthetic.main.activity_faq.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class FaqActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_faq)

        setViews()
    }

    private fun setViews() {
        toolbar_title_tv.text = "Frequent questions and answers"
        back_img.setOnClickListener { onBackPressed() }
        faq_rv.adapter =
            FaqAdapter(getFaqItems())
    }

    private fun getFaqItems():MutableList<FaqItem> {
        return mutableListOf<FaqItem>().apply {
            add(
                FaqItem(
                    "What is Letsfarm",
                    "Letsfarm.com.ng is a an all round Agricultural solutions provider with services expertise in Agricultural investments, Education, Recycling, Logistics, Manufacturing & Exports"
                )
            )
            add(
                FaqItem(
                    "How do I sponsor a farm",
                    "Create an account an visit the farmshop to sponsor as many units of your chosen farm as available for sponsorship\n"
                )
            )
            add(
                FaqItem(
                    "What is ROI",
                    "ROI differs according to the type of farms. Sponsors can earn up to 35%. Visit the farmshop to see all farms\n"
                )
            )
            add(
                FaqItem(
                    "Are your farm insured?",
                    "Your sponsorship caters farm insurance with Leadway Assurance PLC against losses resulting from theft, death, floods etc\n"
                )
            )
            add(
                FaqItem(
                    "Can I cancel my sponsorship before before the due data?",
                    "No, you cant! Our packages are carefully crafted to ensure that your sponsorship cater for farms operations within a specific period. A contract is signed when you sponsor which you must see through.\n"
                )
            )
            add(
                FaqItem(
                    "Can I apply as a farmer",
                    "Yes, you can. Send us a message via info@letsfarm.co with the subject “FARMER APPLICATION”\n"
                )
            )
        }

    }
}

data class FaqItem(val question:String, val answer:String)