package co.letsfarm.letsfarm.dashBoard.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.utils.hide
import co.letsfarm.letsfarm.utils.show
import kotlinx.android.synthetic.main.holder_faq.view.*

class FaqAdapter(val faqItems: MutableList<FaqItem>) : RecyclerView.Adapter<FaqViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FaqViewHolder {
        LayoutInflater.from(parent.context).inflate(R.layout.holder_faq, parent, false).run{
            return FaqViewHolder(this)
        }
    }

    override fun getItemCount() = faqItems.size

    override fun onBindViewHolder(holder: FaqViewHolder, position: Int) {
        var isOpen = false
        holder.itemView.run {
            faqItems.get(position).let {faqItem->
                answer_tv.text = faqItem.answer
                question_tv.text = faqItem.question

                setOnClickListener {
                    isOpen = !isOpen

                    if (isOpen){
                        answer_tv.show()
                        open_img.setImageDrawable(resources.getDrawable(R.drawable.ic_faq_open))

                    }else{
                        answer_tv.hide()
                        open_img.setImageDrawable(resources.getDrawable(R.drawable.ic_faq_closed))
                    }
                }
            }
        }
    }

}

class FaqViewHolder(itemView:View):RecyclerView.ViewHolder(itemView)