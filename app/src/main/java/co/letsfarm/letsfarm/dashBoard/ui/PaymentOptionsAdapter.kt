package co.letsfarm.letsfarm.dashBoard.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.dashBoard.model.CardPaymentCharges
import co.letsfarm.letsfarm.dashBoard.model.PaymentOptionRes
import co.letsfarm.letsfarm.utils.getMoney
import co.letsfarm.letsfarm.utils.setVisibility
import kotlinx.android.synthetic.main.holder_payment_options.view.*

class PaymentOptionsAdapter(val walletBalance:Double, val paymentOptions: List<PaymentOptionRes?>,
                            val cardPaymentCharges: CardPaymentCharges, val listener: (PaymentOptionRes) -> Unit) : RecyclerView.Adapter<PaymentOptionsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentOptionsViewHolder {
        LayoutInflater.from(parent.context).inflate(R.layout.holder_payment_options, parent, false).run{
            return PaymentOptionsViewHolder(this)
        }
    }

    override fun getItemCount() = paymentOptions.size

    override fun onBindViewHolder(holder: PaymentOptionsViewHolder, position: Int) {
        holder.itemView.run {
            paymentOptions.get(position)?.let {option->
                option_tv.text = "${option.description}"
                setOnClickListener {
                    listener(option)
                }

                val showBalance = option.name?.toLowerCase()?.contains("wallet") ?: false
                val showCardCharges = option.name?.toLowerCase()?.contains("paystack") ?: false

                balance_title_tv.setVisibility(visible = showBalance)
                balance_tv.setVisibility(visible = showBalance)

                charges_cv.setVisibility(visible = showCardCharges)

                balance_tv.text = walletBalance.getMoney
                card_charges_tv.text = cardPaymentCharges.charge?.getMoney
                total_amount_tv.text = cardPaymentCharges.total?.getMoney
            }
        }
    }

}

class PaymentOptionsViewHolder(itemView:View):RecyclerView.ViewHolder(itemView)
