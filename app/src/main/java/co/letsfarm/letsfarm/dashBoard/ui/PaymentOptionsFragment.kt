package co.letsfarm.letsfarm.dashBoard.ui

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.dashBoard.model.CardPaymentCharges
import co.letsfarm.letsfarm.dashBoard.model.PaymentOptionRes
import kotlinx.android.synthetic.main.fragment_payment_options.*

class PaymentOptionsFragment(val walletBalance:Double, val paymentOptions: List<PaymentOptionRes?>,
                             val cardPaymentCharges: CardPaymentCharges, val listener: (PaymentOptionRes) -> Unit) : DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_payment_options, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        payment_option_rv.adapter = PaymentOptionsAdapter(walletBalance, paymentOptions, cardPaymentCharges){
            listener(it)
            dismiss()
        }
    }

    override fun onStart() {
        super.onStart()
        val dialog: Dialog? = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            dialog.window?.setLayout(width, height)
        }
    }
}