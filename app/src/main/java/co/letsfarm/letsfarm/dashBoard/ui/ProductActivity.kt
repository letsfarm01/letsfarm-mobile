package co.letsfarm.letsfarm.dashBoard.ui

import android.app.Application
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import co.letsfarm.letsfarm.BaseActivity
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.allFarms.ui.CartActivity
import co.letsfarm.letsfarm.product.model.ProductsData
import co.letsfarm.letsfarm.product.ui.AddProductCartBs
import co.letsfarm.letsfarm.product.ui.ProductCartActivity
import co.letsfarm.letsfarm.product.viewModel.ProductRepository
import co.letsfarm.letsfarm.product.viewModel.ProductViewModel
import co.letsfarm.letsfarm.utils.*
import com.squareup.picasso.Picasso
import dagger.Component
import dagger.Module
import dagger.Provides
import kotlinx.android.synthetic.main.activity_all_farms.*
import kotlinx.android.synthetic.main.activity_product.*
import kotlinx.android.synthetic.main.activity_product.back_img
import kotlinx.android.synthetic.main.activity_product.cart_count_tv
import kotlinx.android.synthetic.main.activity_product.cart_img
import kotlinx.android.synthetic.main.activity_product.progress
import kotlinx.android.synthetic.main.holder_product.view.*
import javax.inject.Inject
import javax.inject.Singleton

class ProductActivity : BaseActivity() {

    @Inject
    lateinit var viewModel:ProductViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product)

        DaggerAllProductComponent
            .builder()
            .allProductModule(AllProductModule(application))
            .build()
            .inject(this)

        setObservers()
        setViews()
        viewModel.getCartCount()

    }

    override fun onResume() {
        super.onResume()
        viewModel.getCartCount()
    }

    private fun setViews() {
        back_img.setOnClickListener { onBackPressed() }
        cart_img.setOnClickListener {
            if (cart_count_tv.text != "0")
                navigateTo<ProductCartActivity>()
        }
        cart_count_tv.setOnClickListener {
            if (cart_count_tv.text != "0")
                navigateTo<ProductCartActivity>()
        }
    }

    private fun setObservers() {
        viewModel.fetchAllProduct()
        viewModel.allProductRequest.observe(this, Observer {resource->
            progress.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success->{
                    resource.data?.data?.let{products->
                        setProductsAdapter(products)
                    }
                }
                is Resource.Error->{
                    processError(resource.message, resource.responseCode)
                }
            }
        })
        viewModel.getCartCountRequest.observe(this, Observer {
            when(it){
                is Resource.Success->{
                    cart_count_tv.text = it.data ?: "0"
                }
                is Resource.Error->{
                    processError(it.message, it.responseCode)
                }
            }
        })
    }

    private fun setProductsAdapter(products: List<ProductsData?>) {
        product_rv.layoutManager = GridLayoutManager(applicationContext, 2)

        product_rv.adapter =
            GenericListAdapter(products, R.layout.holder_product)
            { view, item, position ->

                Picasso.get().load(item?.displayImg).into(view.product_img)

                view.product_name_tv.text = "${item?.productName}"
                view.product_amount_tv.text = "${item?.price?.getMoney}"
                view.product_location_tv.text = "${item?.productLocation}"

                view.setOnClickListener {
                    item?.let { it1 -> AddProductCartBs.newInstance(it1).show(supportFragmentManager, null) }
                }
            }
    }

}

@Singleton
@Component(modules = [AllProductModule::class])
interface AllProductComponent {
    fun inject(activity: ProductActivity)
}

@Module
class AllProductModule(private val application: Application) : NetModule() {

    @Provides
    fun provideAllProductViewModel(repo: ProductRepository) : ProductViewModel {
        return ProductViewModel(repo, application)
    }

    @Provides
    fun provideRepository(service: ApiServices) : ProductRepository {
        return ProductRepository(service, application)
    }
}