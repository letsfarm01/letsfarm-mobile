package co.letsfarm.letsfarm.dashBoard.ui

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import co.letsfarm.letsfarm.BaseActivity
import androidx.lifecycle.Observer
import co.letsfarm.letsfarm.utils.GenericListAdapter
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.ui.DashBoardModule
import co.letsfarm.letsfarm.ui.ui.dashboard.DashboardViewModel
import co.letsfarm.letsfarm.utils.*
import dagger.Component
import kotlinx.android.synthetic.main.activity_referral.*
import kotlinx.android.synthetic.main.holder_referral.view.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import javax.inject.Inject
import javax.inject.Singleton

class ReferralActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: DashboardViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_referral)

        DaggerReferralComponent.builder()
            .dashBoardModule(DashBoardModule(application))
            .build()
            .inject(this)

        viewModel.getReferral()

        setViews()
        setObservers()
    }


    private fun setObservers() {
        viewModel.getReferralRequest.observe(this, Observer {resource->
            progress.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success->{
                    resource.data?.let {refs->
                        referral_rv.adapter =
                            GenericListAdapter(
                                refs,
                                R.layout.holder_referral
                            )
                            { view, item, position ->
                                view.ref_email_tv.text = "Email: ${item.referreeEmail}"
                                view.amount_earned_tv.text =
                                    "Earned bonus: ${item.expectedBonus?.getMoney}"
                            }
                    }
                }
                is Resource.Error->{
                    processError(resource.message, resource.responseCode)
                }
            }
        })
    }

    private fun setViews() {
        toolbar_title_tv.text = "Referrals"
        back_img.setOnClickListener { onBackPressed() }

        val refCode = savepref().get("refCode", "")

        copy_tv.setOnClickListener {
            val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText(refCode, refCode)
            clipboard.setPrimaryClip(clip)
            showToast("copied")
        }

        ref_code_tv.text = refCode
    }
}

@Singleton
@Component(modules = [DashBoardModule::class])
interface ReferralComponent {
    fun inject(activity: ReferralActivity)
}
