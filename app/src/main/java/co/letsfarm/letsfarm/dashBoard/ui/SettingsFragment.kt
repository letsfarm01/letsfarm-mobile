package co.letsfarm.letsfarm.dashBoard.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.allFarms.ui.AmountDialog
import co.letsfarm.letsfarm.auth.ui.LoginActivity
import co.letsfarm.letsfarm.dashBoard.model.WithdrawData
import co.letsfarm.letsfarm.profile.ui.*
import co.letsfarm.letsfarm.shippingAddress.ui.ShippingAddressActivity
import co.letsfarm.letsfarm.ui.BottomNavActivity
import co.letsfarm.letsfarm.ui.ui.dashboard.DashboardViewModel
import co.letsfarm.letsfarm.utils.*
import kotlinx.android.synthetic.main.fragment_settings.*
import kotlinx.android.synthetic.main.fragment_settings.progress

class SettingsFragment : Fragment() {

    private lateinit var viewModel: DashboardViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        viewModel = (activity as BottomNavActivity).viewModel
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setViews()
        setObservers()
    }

    private fun setViews() {
        log_out_img.setOnClickListener {
            activity?.savepref()?.edit()?.clear()?.apply()
            activity?.navigateToNew<LoginActivity>()
        }
        personal_info_cv.setOnClickListener {
            activity?.navigateTo<PersonalInfoActivity>()
        }
        next_of_kin_cv.setOnClickListener {
            activity?.navigateTo<NextKinInfoActivity>()
        }
        bank_cv.setOnClickListener {
            activity?.navigateTo<BankInfoActivity>()
        }
        business_cv.setOnClickListener {
            activity?.navigateTo<BusinessInfoActivity>()
        }
        shipping_address_cv.setOnClickListener {
            activity?.navigateTo<ShippingAddressActivity>()
        }
        change_password_cv.setOnClickListener {
            activity?.navigateTo<ChangePasswordActivity>()
        }
        withdraw_cv.setOnClickListener {
            viewModel.fetchBankInfo()
        }
        terms_of_use_cv.setOnClickListener {
            val url = "https://letsfarm.com.ng/terms-of-use"
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            startActivity(browserIntent)
        }
    }


    private fun setObservers() {
        viewModel.withdrawRequest.observe(viewLifecycleOwner, Observer {
            progress.load(it is Resource.Loading)
            when(it){
                is Resource.Success->{
                    viewModel.withdrawLive.value = null
                    activity?.showToast("${it.message}")
                }
                is Resource.Error->{
                    viewModel.withdrawLive.value = null
                    activity?.processError(it.message, it.responseCode)
                }
            }
        })
        viewModel.getWalletBalanceRequest.observe(viewLifecycleOwner, Observer {
            progress.load(it is Resource.Loading)
            when(it){
                is Resource.Success->{
                    viewModel.walletBalance = (it.data?.get("wallet_balance") ?: "0").toDouble()
                }
                is Resource.Error->{
                    activity?.processError(it.message, it.responseCode)
                }
            }
        })
        viewModel.fetchBankInfoRequest.observe(viewLifecycleOwner, Observer {resource->
            progress.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success->{
                    viewModel.fetchBankInfoLive.value = null
                    resource.data?.let {
                        if (areNotNull(it.bankCode, it.bankName)){
                            showAmountDialog()
                        }else{
                            showBankInfoUpdate()
                        }
                    }
                }
                is Resource.Error->{
                    viewModel.fetchBankInfoLive.value = null
                    activity?.processError(resource.message, resource.responseCode)
                }
            }
        })
    }

    private fun showAmountDialog() {
        AmountDialog(::showWithdrawalConfirmation).show(childFragmentManager, null)
    }

    private fun showBankInfoUpdate() {
        activity?.showDialog("Info", "Kindly update your bank info to proceed with the withdrawal"){
            activity?.navigateTo<BankInfoActivity>()
        }
    }

    private fun showWithdrawalConfirmation(amount:String) {

        if (viewModel.walletBalance.isNull()){
            viewModel.getWalletBalance()
        }else if(viewModel.walletBalance!! < amount.toDouble()){
            activity?.showDialog("Error",
                "Withdrawal amount ${amount.toDouble().getMoney} can not be more than your wallet balance of ${viewModel.walletBalance!!.getMoney}")
        }else{
            activity?.showConfirmationDialog("Confirm",
                "Are you sure you want to withdraw ${amount.toInt().getMoney} from your wallet",
                postiiveListener = {
                    showPasswordDialog(amount)
                })
        }
    }

    private fun showPasswordDialog(amount: String) {
        PasswordDialog{password->
            withdraw(amount, password)
        }.show(childFragmentManager, null)

    }

    private fun withdraw(amount: String, password: String) {
        val withdrawData = WithdrawData()
        withdrawData.amount = amount
        withdrawData.password = password
        withdrawData.userId = activity?.savepref()?.get("userId", "")

        viewModel.withdraw(withdrawData)
    }

}