package co.letsfarm.letsfarm.dashBoard.viewModel

import android.app.Application
import androidx.lifecycle.LiveData
import co.letsfarm.letsfarm.dashBoard.model.FundWalletData
import co.letsfarm.letsfarm.auth.viewModel.NetworkResource
import co.letsfarm.letsfarm.dashBoard.model.*
import co.letsfarm.letsfarm.history.model.ActiveSponsorship
import co.letsfarm.letsfarm.history.model.RunningFarm
import co.letsfarm.letsfarm.utils.*
import co.letsfarm.letsfarm.profile.model.UpdateProfileData
import co.letsfarm.letsfarm.history.model.Transaction
import co.letsfarm.letsfarm.history.model.WalletTransaction

class DashBoardRespository (val apiServices: ApiServices, val application: Application) : BaseRepositiory(apiServices, application){

    fun fetchDashboardInfo(): LiveData<Resource<DashBoardRes>> {
        return object : NetworkResource<DashBoardRes, DashBoardRes>(application.isConnectedToTheInternet()){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<DashBoardRes>>> {
                return apiServices.fetchDashboardInfo(token)
            }

        }.asLiveData()
    }
    fun getWalletBalance(): LiveData<Resource<Map<String, String>>> {
        return object : NetworkResource<Map<String, String>, Map<String, String>>(application.isConnectedToTheInternet()){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<Map<String, String>>>> {
                return apiServices.getWalletBalance(token, userId)
            }

        }.asLiveData()
    }

    fun getWalletPaymentOption(): LiveData<Resource<List<PaymentOptionRes?>>> {
        return object : NetworkResource<List<PaymentOptionRes?>, List<PaymentOptionRes?>>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<List<PaymentOptionRes?>>>> {
                return apiServices.getPaymentOption(token, true)
            }

        }.asLiveData()
    }

    fun checkout(data: FundWalletData): LiveData<Resource<CheckOutRes>> {
        return object : NetworkResource<CheckOutRes, CheckOutRes>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<CheckOutRes>>> {
                return apiServices.fundWallet(token, data)
            }

        }.asLiveData()
    }


    fun fundWalletCard(data: FundWalletData): LiveData<Resource<Any>> {
        return object : NetworkResource<Any, Any>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<Any>>> {
                return apiServices.fundWalletCard(token, data)
            }

        }.asLiveData()
    }

    fun fetchWalletTransactions(): LiveData<Resource<List<WalletTransaction>>> {
        return object : NetworkResource<List<WalletTransaction>, List<WalletTransaction>>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<List<WalletTransaction>>>> {
                return apiServices.getUserWalletTransaction(token, userId)
            }

        }.asLiveData()
    }

    fun getTransactions(): LiveData<Resource<List<Transaction>>> {
        return object : NetworkResource<List<Transaction>, List<Transaction>>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<List<Transaction>>>> {
                return apiServices.getTransactions(token, userId)
            }

        }.asLiveData()
    }
    fun getActiveSponsorship(): LiveData<Resource<List<ActiveSponsorship>>> {
        return object : NetworkResource<List<ActiveSponsorship>, List<ActiveSponsorship>>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<List<ActiveSponsorship>>>> {
                return apiServices.getActiveSponsorship(token, userId)
            }

        }.asLiveData()
    }
    fun getRunningFarms(): LiveData<Resource<List<RunningFarm>>> {
        val request = HashMap<String, String>()
        request["email"] = app.savepref().get("userEmail", "")

        return object : NetworkResource<List<RunningFarm>, List<RunningFarm>>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<List<RunningFarm>>>> {
                return apiServices.getRunningFarms(token, request)
            }

        }.asLiveData()
    }
    fun getFarmHistory(): LiveData<Resource<List<ActiveSponsorship>>> {
        return object : NetworkResource<List<ActiveSponsorship>, List<ActiveSponsorship>>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<List<ActiveSponsorship>>>> {
                return apiServices.getFarmHistory(token, userId)
            }

        }.asLiveData()
    }

    fun fetchPersonalInfo(): LiveData<Resource<UpdateProfileData>> {
        return object : NetworkResource<UpdateProfileData, UpdateProfileData>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<UpdateProfileData>>> {
                return apiServices.fetchPersonalInfo(token, userId)
            }

        }.asLiveData()
    }

    fun getCardPaymentCharges(totalPrice: Int): LiveData<Resource<CardPaymentCharges>> {
        return object : NetworkResource<CardPaymentCharges, CardPaymentCharges>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<CardPaymentCharges>>> {
                return apiServices.getCardPaymentCharges(token, totalPrice)
            }
        }.asLiveData()
    }

    fun submitMessage(request: HashMap<String, String>): LiveData<Resource<Any>> {
        request["userId"] = userId

        return object : NetworkResource<Any, Any>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<Any>>> {
                return apiServices.submitMessage(token, request)
            }

        }.asLiveData()
    }
    fun getReferral(): LiveData<Resource<List<Referral>>> {
        return object : NetworkResource<List<Referral>, List<Referral>>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<List<Referral>>>> {
                return apiServices.getReferral(token, userId)
            }

        }.asLiveData()
    }
    fun fetchBankInfo(): LiveData<Resource<UpdateProfileData>> {
        return object : NetworkResource<UpdateProfileData, UpdateProfileData>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<UpdateProfileData>>> {
                return apiServices.fetchBankInfo(token, userId)
            }

        }.asLiveData()
    }
    fun withdraw(data: WithdrawData): LiveData<Resource<Any>> {
        return object : NetworkResource<Any, Any>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<Any>>> {
                return apiServices.withdraw(token, data)
            }
        }.asLiveData()
    }


}
