package co.letsfarm.letsfarm.farmUpdates.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class FarmUpdates(

	@field:SerializedName("all")
	val all: List<UpdatesItem?>? = null,

	@field:SerializedName("updates")
	val updates: List<UpdatesItem?>? = null
)

data class UpdatesItem(

	@field:SerializedName("pushTo")
	val pushTo: String? = null,

	@field:SerializedName("updateId")
	val updateId: UpdateId? = null,

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("read")
	val read: Boolean? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)

data class UpdateId(

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("message_body")
	val messageBody: String? = null,

	@field:SerializedName("img_link")
	val imgLink: String? = null,

	@field:SerializedName("publish")
	val publish: Boolean? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
):Parcelable {
	constructor(parcel: Parcel) : this(
		parcel.readString(),
		parcel.readString(),
		parcel.readString(),
		parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
		parcel.readValue(Int::class.java.classLoader) as? Int,
		parcel.readString(),
		parcel.readString(),
		parcel.readString()
	) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(createdAt)
		parcel.writeString(messageBody)
		parcel.writeString(imgLink)
		parcel.writeValue(publish)
		parcel.writeValue(V)
		parcel.writeString(id)
		parcel.writeString(title)
		parcel.writeString(updatedAt)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<UpdateId> {
		override fun createFromParcel(parcel: Parcel): UpdateId {
			return UpdateId(parcel)
		}

		override fun newArray(size: Int): Array<UpdateId?> {
			return arrayOfNulls(size)
		}
	}
}
