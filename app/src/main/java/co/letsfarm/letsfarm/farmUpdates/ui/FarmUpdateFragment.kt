package co.letsfarm.letsfarm.farmUpdates.ui

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.blog.ui.BlogActivity
import co.letsfarm.letsfarm.farmUpdates.viewModel.FarmUpdatesViewModel
import co.letsfarm.letsfarm.utils.GenericListAdapter
import co.letsfarm.letsfarm.utils.navigateTo
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_farm_update.*
import kotlinx.android.synthetic.main.holder_farm_updates.view.*

class FarmUpdateFragment : Fragment() {

    lateinit var viewModel: FarmUpdatesViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_farm_update, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = (activity as FarmUpdatesActivity).viewModel

        setObservers()
    }

    private fun setObservers() {
        viewModel.farmUpdates.observe(viewLifecycleOwner, Observer {
            val adapter = GenericListAdapter(it.updates!!, R.layout.holder_farm_updates) { view, item, position ->
                val update = item?.updateId
                view.title_tv.text = update?.title

                update?.imgLink?.let {imggUrl->
                    Picasso.get().load(imggUrl).into(view.blog_img)
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    view.content_tv.text = Html.fromHtml(update?.messageBody, Html.FROM_HTML_MODE_COMPACT)
                } else {
                    view.content_tv.text = Html.fromHtml(update?.messageBody)
                }

                view.setOnClickListener {
                    activity?.navigateTo<ViewUpdatesActivity>{
                        putExtra("update", update)
                    }
                }
            }

            farm_updates_rv.adapter = adapter

        })
    }
}