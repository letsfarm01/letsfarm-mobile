package co.letsfarm.letsfarm.farmUpdates.ui

import android.app.Application
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.farmUpdates.viewModel.FarmUpdatesRespository
import co.letsfarm.letsfarm.farmUpdates.viewModel.FarmUpdatesViewModel
import co.letsfarm.letsfarm.utils.*
import com.google.android.material.tabs.TabLayout
import dagger.Component
import dagger.Module
import dagger.Provides
import kotlinx.android.synthetic.main.activity_farm_updates.*
import javax.inject.Inject
import javax.inject.Singleton

class FarmUpdatesActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModel:FarmUpdatesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_farm_updates)

        DaggerFarmUpdatesComponent
            .builder()
            .farmUpdatesModule(FarmUpdatesModule(application))
            .build()
            .inject(this)

        val sectionsPagerAdapter = FarmUpdateViewPager(this, supportFragmentManager)

        view_pager.adapter = sectionsPagerAdapter

        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(view_pager)
        back_img.setOnClickListener {onBackPressed()}

        setObservers()
    }

    private fun setObservers() {
        viewModel.getFarmUpdates()
        viewModel.farmUpdatesRequest.observe(this, Observer {
            progress.load(it is Resource.Loading)
            when(it){
                is Resource.Success->{
                    viewModel.farmUpdates.value = it.data
                }
                is Resource.Error->{
                    processError(it.message, it.responseCode)
                }
            }
        })
    }


}

@Singleton
@Component(modules = [FarmUpdatesModule::class])
interface FarmUpdatesComponent {
    fun inject(activity: FarmUpdatesActivity)
}

@Module
class FarmUpdatesModule(private val application: Application) : NetModule() {

    @Provides
    fun provideFarmUpdatesViewModel(repo: FarmUpdatesRespository) : FarmUpdatesViewModel {
        return FarmUpdatesViewModel(repo, application)
    }

    @Provides
    fun provideRepository(service: ApiServices) : FarmUpdatesRespository {
        return FarmUpdatesRespository(service, application)
    }
}