package co.letsfarm.letsfarm.farmUpdates.ui

import android.os.Build
import android.os.Bundle
import android.text.Html
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat
import androidx.lifecycle.lifecycleScope
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.farmUpdates.model.UpdateId
import co.letsfarm.letsfarm.utils.HtmlImageGetter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_blog.*
import kotlinx.android.synthetic.main.layout_toolbar.*


class ViewUpdatesActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_updates)

        intent.extras?.getParcelable<UpdateId>("update")?.let{ data->
            setViews(data)
        }?:run{
            onBackPressed()
        }
    }

    private fun setViews(data: UpdateId){
        toolbar_title_tv.text = "Update"
        back_img.setOnClickListener { onBackPressed() }

        title_tv.text = data.title

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            val imageGetter = HtmlImageGetter(lifecycleScope, resources, Picasso.get(), content_tv)
            val styledText = HtmlCompat.fromHtml(data.messageBody!!,
                HtmlCompat.FROM_HTML_SEPARATOR_LINE_BREAK_HEADING, imageGetter, null)
            content_tv.text = styledText

        } else {
            content_tv.text = Html.fromHtml(data.messageBody)
        }

        Picasso.get().load(data.imgLink).into(blog_img)
    }


}
