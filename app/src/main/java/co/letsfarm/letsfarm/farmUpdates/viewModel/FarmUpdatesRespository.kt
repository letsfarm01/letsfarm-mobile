package co.letsfarm.letsfarm.farmUpdates.viewModel

import android.app.Application
import androidx.lifecycle.LiveData
import co.letsfarm.letsfarm.auth.viewModel.NetworkResource
import co.letsfarm.letsfarm.dashBoard.model.GeneralRes
import co.letsfarm.letsfarm.farmUpdates.model.FarmUpdates
import co.letsfarm.letsfarm.utils.*

class FarmUpdatesRespository(val apiServices: ApiServices, val application: Application) : BaseRepositiory(apiServices, application) {

    fun getFarmUpdates(): LiveData<Resource<FarmUpdates>> {
        return object : NetworkResource<FarmUpdates, FarmUpdates>(
            application.isConnectedToTheInternet()){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<FarmUpdates>>> {
                return apiServices.getFarmUpdates(token)
            }

        }.asLiveData()
    }
}
