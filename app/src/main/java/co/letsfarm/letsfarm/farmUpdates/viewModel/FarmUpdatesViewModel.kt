package co.letsfarm.letsfarm.farmUpdates.viewModel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.switchMap
import co.letsfarm.letsfarm.blog.viewModel.AllBlogRespository
import co.letsfarm.letsfarm.common.utils.AbsentLiveData
import co.letsfarm.letsfarm.farmUpdates.model.FarmUpdates
import co.letsfarm.letsfarm.utils.BaseViewModel
import co.letsfarm.letsfarm.utils.Resource
import java.util.HashMap

class FarmUpdatesViewModel(val repository: FarmUpdatesRespository, val app: Application): BaseViewModel(repository, app){

    var farmUpdates: MutableLiveData<FarmUpdates> = MutableLiveData()

    private val farmUpdatesLive= MutableLiveData<Boolean>()

    fun getFarmUpdates(){
        farmUpdatesLive.value=true
    }

    val farmUpdatesRequest: LiveData<Resource<FarmUpdates>> = farmUpdatesLive.switchMap { data->
        if (data) {
            repository.getFarmUpdates()
        }else{
            AbsentLiveData.create()
        }
    }


}
