package co.letsfarm.letsfarm.history.model

import com.google.gson.annotations.SerializedName
import java.util.*

data class ActiveSponsorship(

	@field:SerializedName("itemRemaining")
	val itemRemaining: Int? = null,

	@field:SerializedName("orderId")
	val orderId: OrderId? = null,

	@field:SerializedName("expected_end_date")
	val expectedEndDate: Date? = null,

	@field:SerializedName("farmId")
	val farmId: String? = null,

	@field:SerializedName("expected_return")
	val expectedReturn: Int? = null,

	@field:SerializedName("userId")
	val userId: String? = null,

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("deleted")
	val deleted: Boolean? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("start_date")
	val startDate: Date? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)

data class OrderId(

	@field:SerializedName("quantity")
	val quantity: Int? = null,

	@field:SerializedName("total_price")
	val totalPrice: Int? = null,

	@field:SerializedName("cartId")
	val cartId: String? = null,

	@field:SerializedName("duration_type")
	val durationType: String? = null,

	@field:SerializedName("farmId")
	val farmId: String? = null,

	@field:SerializedName("userId")
	val userId: String? = null,

	@field:SerializedName("roi")
	val roi: Int? = null,

	@field:SerializedName("duration")
	val duration: Int? = null,

	@field:SerializedName("qty_remaining_before_order")
	val qtyRemainingBeforeOrder: Int? = null,

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("deleted")
	val deleted: Boolean? = null,

	@field:SerializedName("payment_channel")
	val paymentChannel: String? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("orderReference")
	val orderReference: String? = null,

	@field:SerializedName("currency")
	val currency: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("price_per_unit")
	val pricePerUnit: Int? = null,

	@field:SerializedName("farm_name")
	val farmName: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)
