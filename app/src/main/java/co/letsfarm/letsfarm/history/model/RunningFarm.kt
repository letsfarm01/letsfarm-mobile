package co.letsfarm.letsfarm.history.model

import com.google.gson.annotations.SerializedName

data class RunningFarm(

	@field:SerializedName("quantity")
	val quantity: Int? = null,

	@field:SerializedName("order_number")
	val orderNumber: String? = null,

	@field:SerializedName("roi_amount")
	val roiAmount: String? = null,

	@field:SerializedName("order_status")
	val orderStatus: String? = null,

	@field:SerializedName("roi_date")
	val roiDate: String? = null,

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("deleted")
	val deleted: Boolean? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("order_total")
	val orderTotal: String? = null,

	@field:SerializedName("date_of_sponsor")
	val dateOfSponsor: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("farm_name")
	val farmName: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)
