package co.letsfarm.letsfarm.history.model

import com.google.gson.annotations.SerializedName
import java.util.*

data class Transaction(

    @field:SerializedName("total_price")
	val totalPrice: Int? = null,

    @field:SerializedName("paymentReference")
	val paymentReference: String? = null,

    @field:SerializedName("userId")
	val userId: String? = null,

    @field:SerializedName("transactionType")
	val transactionType: String? = null,

    @field:SerializedName("createdAt")
	val createdAt: Date? = null,

    @field:SerializedName("deleted")
	val deleted: Boolean? = null,

    @field:SerializedName("payment_channel")
	val paymentChannel: TransactionPaymentChannel? = null,

    @field:SerializedName("narration")
	val narration: String? = null,

    @field:SerializedName("__v")
	val V: Int? = null,

    @field:SerializedName("orderReference")
	val orderReference: String? = null,

    @field:SerializedName("_id")
	val id: String? = null,

    @field:SerializedName("status")
	val status: String? = null,

    @field:SerializedName("updatedAt")
	val updatedAt: String? = null
)

data class TransactionPaymentChannel(

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("is_active")
	val isActive: Boolean? = null,

	@field:SerializedName("deleted")
	val deleted: Boolean? = null,

	@field:SerializedName("can_fund")
	val canFund: Boolean? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)
