package co.letsfarm.letsfarm.history.model

import com.google.gson.annotations.SerializedName
import java.util.*

data class WalletTransaction(

    @field:SerializedName("createdAt")
	val createdAt: Date? = null,

    @field:SerializedName("amount")
	val amount: Int? = null,

    @field:SerializedName("deleted")
	val deleted: Boolean? = null,

    @field:SerializedName("narration")
	val narration: String? = null,

    @field:SerializedName("__v")
	val V: Int? = null,

    @field:SerializedName("new_wallet_balance")
	val newWalletBalance: Int? = null,

    @field:SerializedName("_id")
	val id: String? = null,

    @field:SerializedName("type")
	val type: String? = null,

    @field:SerializedName("userId")
	val userId: String? = null,

    @field:SerializedName("transactionId")
	val transactionId: TransactionId? = null,

    @field:SerializedName("old_wallet_balance")
	val oldWalletBalance: Int? = null,

    @field:SerializedName("updatedAt")
	val updatedAt: String? = null
)

data class TransactionId(

	@field:SerializedName("total_price")
	val totalPrice: Int? = null,

	@field:SerializedName("paymentReference")
	val paymentReference: String? = null,

	@field:SerializedName("userId")
	val userId: String? = null,

	@field:SerializedName("transactionType")
	val transactionType: String? = null,

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("deleted")
	val deleted: Boolean? = null,

	@field:SerializedName("payment_channel")
	val paymentChannel: String? = null,

	@field:SerializedName("narration")
	val narration: String? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("orderReference")
	val orderReference: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)
