package co.letsfarm.letsfarm.history.ui

import co.letsfarm.letsfarm.BaseActivity
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.ui.DashBoardModule
import co.letsfarm.letsfarm.ui.ui.dashboard.DashboardViewModel
import com.google.android.material.tabs.TabLayout
import dagger.Component
import kotlinx.android.synthetic.main.activity_farm_history.*
import javax.inject.Inject
import javax.inject.Singleton

class FarmHistoryActivity : BaseActivity() {

    @Inject
    lateinit var viewModel:DashboardViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_farm_history)

        DaggerHistoryComponent.builder()
            .dashBoardModule(DashBoardModule(application))
            .build()
            .inject(this)

        setObservers()
        setViews()

        val sectionsPagerAdapter = FarmHistoryPagerAdapter(this, supportFragmentManager)

        val viewPager: ViewPager = findViewById(R.id.view_pager)

        viewPager.adapter = sectionsPagerAdapter

        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(viewPager)
        back_img.setOnClickListener {onBackPressed()}

    }

    private fun setViews() {
        back_img.setOnClickListener { onBackPressed() }
    }

    private fun setObservers() {
        viewModel.fetchWalletTransactions()

    }

    override fun onResume() {
        super.onResume()
    }
}
@Singleton
@Component(modules = [DashBoardModule::class])
interface HistoryComponent {
    fun inject(activity: FarmHistoryActivity)
}