package co.letsfarm.letsfarm.history.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.history.model.ActiveSponsorship
import co.letsfarm.letsfarm.utils.getMoney
import co.letsfarm.letsfarm.utils.parseDateGood
import kotlinx.android.synthetic.main.holder_farm_history.view.*

class FarmHistoryAdapter : RecyclerView.Adapter<HistoryHolder>() {

    var activeSponsorship: List<ActiveSponsorship>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryHolder {
        LayoutInflater.from(parent.context).inflate(R.layout.holder_farm_history, parent, false).run {
            return HistoryHolder(this)
        }
    }

    override fun getItemCount() = activeSponsorship?.size ?: 0

    override fun onBindViewHolder(holder: HistoryHolder, position: Int) {

        holder.itemView.run {
            activeSponsorship?.get(position)?.let {
                val farmName = it.orderId?.farmName
                val roi = it.orderId?.roi
                val duration = it.orderId?.duration
                val durationType = it.orderId?.durationType

                narration_tv.text = "$farmName at $roi% in $duration $durationType"

                investment_tv.text = "Amount invested: "+it.orderId?.totalPrice?.getMoney
                return_tv.text = "Expected Return: "+ it.expectedReturn?.getMoney

                start_date_tv.text = "Start of cycle: "+it.startDate?.parseDateGood
                end_date_tv.text = "End of cycle: "+it.expectedEndDate?.parseDateGood
            }
        }

    }

    fun setData(activeSponsorship: List<ActiveSponsorship>) {
        this.activeSponsorship = activeSponsorship
        notifyDataSetChanged()
    }

}