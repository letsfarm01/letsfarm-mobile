package co.letsfarm.letsfarm.history.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.ui.ui.dashboard.DashboardViewModel
import co.letsfarm.letsfarm.utils.Resource
import co.letsfarm.letsfarm.utils.load
import co.letsfarm.letsfarm.utils.processError

import kotlinx.android.synthetic.main.fragment_farm_history.*

class FarmHistoryFragment : Fragment() {

    private lateinit var viewModel: DashboardViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        viewModel = (activity as FarmHistoryActivity).viewModel

        return inflater.inflate(R.layout.fragment_farm_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getFarmHistory()
        setViews()
        setObservers()
    }

    private fun setObservers() {
        viewModel.getFarmHistoryRequest.observe(viewLifecycleOwner, Observer {
            progress.load(it is Resource.Loading)
            when(it){
                is Resource.Success->{
                    it.data?.let {
                        adapter.setData(it)
                    }
                }
                is Resource.Error->{
                    activity?.processError(it.message, it.responseCode)
                }
            }
        })

    }

    var adapter = FarmHistoryAdapter()

    private fun setViews() {
        history_rv.adapter = adapter

    }

}