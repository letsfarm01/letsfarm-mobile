package co.letsfarm.letsfarm.history.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import co.letsfarm.letsfarm.utils.GenericListAdapter
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.ui.ui.dashboard.DashboardViewModel
import co.letsfarm.letsfarm.utils.*
import kotlinx.android.synthetic.main.fragment_running_farms.*
import kotlinx.android.synthetic.main.holder_farm_history.view.*

class RunningFarmsFragment : Fragment() {

    private lateinit var viewModel: DashboardViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        viewModel = (activity as FarmHistoryActivity).viewModel

        return inflater.inflate(R.layout.fragment_running_farms, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getRunningFarms()
        setViews()
        setObservers()
    }

    private fun setObservers() {
        viewModel.getRunningFarmsRequest.observe(viewLifecycleOwner, Observer {
            progress.load(it is Resource.Loading)
            when(it){
                is Resource.Success->{
                    it.data?.let {runningFarms->
                        running_farms_rv.adapter =
                            GenericListAdapter(
                                runningFarms,
                                R.layout.holder_running_farms
                            )

                            { view, item, position ->

                                view.narration_tv.text = "${item.farmName}"
                                view.investment_tv.text = "Amount invested: ₦" + item.orderTotal
                                view.return_tv.text = "Expected Return: ₦" + item.roiAmount
                                view.start_date_tv.text = "Start of cycle: " + item.dateOfSponsor
                                view.end_date_tv.text = "End of cycle: " + item.roiDate
//                            view.amount_tv.text = "${item?.amount?.getMoney}"
                            }
                    }
                }
                is Resource.Error->{
                    activity?.processError(it.message, it.responseCode)
                }
            }
        })

    }


    private fun setViews() {


    }

}