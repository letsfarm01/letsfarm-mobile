package co.letsfarm.letsfarm.history.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.history.model.Transaction
import co.letsfarm.letsfarm.utils.getMoney
import co.letsfarm.letsfarm.utils.parseDateGood
import kotlinx.android.synthetic.main.holder_transaction_history.view.*

class TransactionsAdapter : RecyclerView.Adapter<HistoryHolder>() {

    var transaction: List<Transaction>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryHolder {
        LayoutInflater.from(parent.context).inflate(R.layout.holder_transaction_history, parent, false).run {
            return HistoryHolder(this)
        }
    }

    override fun getItemCount() = transaction?.size ?: 0

    override fun onBindViewHolder(holder: HistoryHolder, position: Int) {

        holder.itemView.run {
            transaction?.get(position)?.let {
                narration_tv.text = it.narration
                amount_tv.text = it.totalPrice?.getMoney
                date_tv.text = it.createdAt?.parseDateGood
                type_tv.text = it.status
                reference_tv.text = it.orderReference
                payment_channel_tv.text = it.paymentChannel?.name
            }
        }

    }

    fun setData(transaction: List<Transaction>) {
        this.transaction = transaction
        notifyDataSetChanged()
    }

}

class HistoryHolder(itemView: View): RecyclerView.ViewHolder(itemView)
