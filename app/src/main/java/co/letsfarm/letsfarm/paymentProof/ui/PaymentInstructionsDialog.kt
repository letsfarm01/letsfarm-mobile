package co.letsfarm.letsfarm.paymentProof.ui

import android.app.Dialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.text.bold
import androidx.core.text.scale
import androidx.fragment.app.DialogFragment
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.utils.getMoney
import kotlinx.android.synthetic.main.fragment_payment_instructions_dialog.*

class PaymentInstructionsDialog(val transactionRef: String, val amount: Int) : DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_payment_instructions_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        back_img.setOnClickListener { dismiss() }

        copy_ref_btn.setOnClickListener {
            val clipboard = context?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText(transactionRef, transactionRef)
            clipboard.setPrimaryClip(clip)
            Toast.makeText(context, "Reference copied to clipboard", Toast.LENGTH_SHORT).show()
        }

        copy_account_number_btn.setOnClickListener {
            val clipboard = context?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("0138013221", "0138013221")
            clipboard.setPrimaryClip(clip)
            Toast.makeText(context, "Account number copied to clipboard", Toast.LENGTH_SHORT).show()
        }

//        "1) Make payment using Online banking e.g Bank App on Phone or Web\n" +
//                "\nLogin to your online banking app and make a transfer into any of our list banks:\n" +
//                "\nBank Name: Union Bank of Nigeria\nAccount Number: 0138013221\nAccount Name: Letsfarm Integrated Services Ltd\n" +
//                "Amount: sdsd\nNarration/Comment: sdsds\n\n\n\nBank transfer procedure using bank teller\n" +
//                "\n\"Walk into any of the under listed bank and fill teller to make payment:\n" +
//                "\n" +
//                "Bank Name: Union Bank of Nigeria\n" +
//                "Account Number: 0138013221\n" +
//                "Account Name: Letsfarm Integrated Services Ltd\n" +
//                "Amount: sdsd\n" +
//                "Depositor's Name: Your registered name on Letsfarm\n"
//                "Narration/Comment: sdsds\n" +
//                        "N.B: On top of the deposit slip, kindly write this number transactionRef \n"

        instruction_tv.text = SpannableStringBuilder()
            .scale(1.2F) { bold { append("1) ")}}
            .scale(1F) { append("Make payment using Online banking e.g Bank App on Phone or Web\n\n")}
            .append("Login to your online banking app and make a transfer into any of our list banks:\n\n")
            .append("Bank Name: ")
            .scale(1.2F) { bold { append("Union Bank of Nigeria\n")}}
            .append("Account Number: ")
            .scale(1.2F) { bold { append("0138013221\n")}}
            .append("Account Name: ")
            .scale(1.2F) { bold { append("Letsfarm Integrated Services Ltd\n")}}
            .append("Amount: ")
            .scale(1.2F) { bold { append("${amount.getMoney} \n")}}
            .append("Narration/Comment: ")
            .scale(1.2F) { bold { append("$transactionRef \n\n\n\n")}}

            .scale(1.2F) { bold { append("2) ")}}
            .scale(1F) { append("Bank transfer procedure using bank teller\n\n")}
            .append("Walk into any of the under listed bank and fill teller to make payment:\n\n")
            .append("Bank Name: ")
            .scale(1.2F) { bold { append("Union Bank of Nigeria\n")}}
            .append("Account Number: ")
            .scale(1.2F) { bold { append("0138013221\n")}}
            .append("Account Name: ")
            .scale(1.2F) { bold { append("Letsfarm Integrated Services Ltd\n")}}
            .append("Amount: ")
            .scale(1.2F) { bold { append("${amount.getMoney} \n")}}
            .append("Depositor's Name: ")
            .scale(1.2F) { bold { append("Your registered name on Letsfarm\n")}}
            .append("Narration/Comment: ")
            .scale(1.2F) { bold { append("$transactionRef \n\n")}}

            .append("N.B: On top of the deposit slip, kindly write this number $transactionRef \n")

    }

    override fun onStart() {
        super.onStart()
        val dialog: Dialog? = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window?.setLayout(width, height)
        }
    }

}