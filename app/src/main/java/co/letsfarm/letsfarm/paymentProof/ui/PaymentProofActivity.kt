package co.letsfarm.letsfarm.paymentProof.ui

import android.Manifest
import android.annotation.TargetApi
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import co.letsfarm.letsfarm.BaseActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.allFarms.viewModel.AllFarmsViewModel
import co.letsfarm.letsfarm.dashBoard.model.Document
import co.letsfarm.letsfarm.dashBoard.model.UploadProofData
import co.letsfarm.letsfarm.ui.ui.dashboard.PendingTransactionsActivity
import co.letsfarm.letsfarm.ui.ui.dashboard.PendingTransactionsModule
import co.letsfarm.letsfarm.utils.*
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.ktx.storage
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import dagger.Component
import kotlinx.android.synthetic.main.activity_payment_proof.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import javax.inject.Inject
import javax.inject.Singleton

class PaymentProofActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: AllFarmsViewModel

    lateinit var storage: FirebaseStorage
    var mAmount = 0
    lateinit var mTransactionId: String
    lateinit var mTransactionRef: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_proof)

        DaggerPaymentProofComponent.builder()
            .pendingTransactionsModule(PendingTransactionsModule(application))
            .build()
            .inject(this)

        intent.extras?.let {bundle->
            mAmount = bundle.getInt("amount")
            mTransactionId = bundle.getString("transactionId") ?: ""
            mTransactionRef = bundle.getString("transactionRef") ?: ""
            PaymentInstructionsDialog(
                mTransactionRef,
                mAmount
            ).show(supportFragmentManager, null)
        }

        storage = Firebase.storage

//        progress.visibility = View.Visibi
        setViews()
        setObservers()
    }

    private fun setObservers() {
        viewModel.uploadProofRequest.observe(this, Observer {resource->
            submit_btn.load(resource is Resource.Loading)

            when(resource){
                is Resource.Success->{
                    processError(resource.message, resource.responseCode)
                    onBackPressed()
                }
                is Resource.Error->{
                    processError(resource.message, resource.responseCode)
                }
            }
        })
    }

    private fun setViews() {
        payment_instruction_tv.setOnClickListener {
            PaymentInstructionsDialog(
                mTransactionRef,
                mAmount
            ).show(supportFragmentManager, null)
        }
        submit_btn.setOnClickListener {
            submit()
        }
        upload_btn.setOnClickListener {
            if (hasPermission()) {
                UploadOptionsDialog(
                    ::uploadDoc,
                    ::takePicture
                ).show(supportFragmentManager, null)
            }
        }
        toolbar_title_tv.text = "Proof of payment"
        back_img.setOnClickListener { onBackPressed() }
    }

    private fun submit() {
        if (selectedFile.isNull() || uploadedFilePath.isNull()){
            showErrorDialog("Select a file")
        }else{
            val uploadProofData = UploadProofData().apply{
                transactionId = mTransactionId
                this.transactionRef = mTransactionRef
                userId = savepref().get("userId", "")
                bankName = ""
                message = comment_et.trimmedText
                documents!!.add(Document(uploadedFilePath!!))
                amount = mAmount
            }
            viewModel.uploadProof(uploadProofData)
        }
    }

    private fun takePicture() {
        CropImage.activity()
            .setGuidelines(CropImageView.Guidelines.ON)
            .start(this)
    }

    private fun uploadDoc() {
        val intent = Intent().setType("*/*").setAction(Intent.ACTION_GET_CONTENT)
        startActivityForResult(Intent.createChooser(intent, "Select a file"), 111)
    }

    private var selectedFile: Uri? = null
    private var uploadedFilePath: String? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        log("onActivityResult")
        if (resultCode == RESULT_OK) {
            if (requestCode == 111)
                data?.data?.let { uploadToFirebase(it) }
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)
                uploadToFirebase(CropImage.getActivityResult(data).uri)
        }
    }

    private fun uploadToFirebase(selectedFile: Uri) {
        progress_bar.show()
        val storageRef: StorageReference = storage.reference

        log("${selectedFile.getName(this)}")

        val imagesRef = storageRef.child("letsfarm_images/proof_payment/${selectedFile.getName(this)}")

        val uploadTask = imagesRef.putFile(selectedFile)

        uploadTask.addOnFailureListener {
            progress_bar.hide()
            showErrorDialog("${it.message ?: "An error occurred! Try again"}")
        }.addOnSuccessListener {
            val result = it.metadata!!.reference!!.downloadUrl
            result.addOnSuccessListener {
                progress_bar.hide()
                this.selectedFile = selectedFile
                file_uploaded_tv.show()
                uploadedFilePath = it.toString()
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    fun hasPermission(): Boolean {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
            || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){

            // Should we show an explanation?
            if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                // Explain to the user why we need to read the contacts
            }

            requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                202
            )

            // MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE is an
            // app-defined int constant

            return false
        }
        return true
    }

    override fun onBackPressed() {
        navigateTo<PendingTransactionsActivity>()
    }
}
@Singleton
@Component(modules = [PendingTransactionsModule::class])
interface PaymentProofComponent {
    fun inject(activity: PaymentProofActivity)
}