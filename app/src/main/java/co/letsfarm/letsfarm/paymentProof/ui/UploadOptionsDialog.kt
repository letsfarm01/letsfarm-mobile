package co.letsfarm.letsfarm.paymentProof.ui

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import co.letsfarm.letsfarm.R
import kotlinx.android.synthetic.main.fragment_upload_options_dialog.*
import kotlin.reflect.KFunction0

class UploadOptionsDialog(val uploadDoc: KFunction0<Unit>, val takePicture: KFunction0<Unit>) : DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_upload_options_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        storage_tv.setOnClickListener {
            uploadDoc()
            dismiss()
        }

        picture_tv.setOnClickListener {
            takePicture()
            dismiss()
        }
    }

    override fun onStart() {
        super.onStart()
        val dialog: Dialog? = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            dialog.window?.setLayout(width, height)
        }
    }
}