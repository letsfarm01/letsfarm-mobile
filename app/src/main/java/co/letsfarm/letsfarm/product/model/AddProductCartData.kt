package co.letsfarm.letsfarm.product.model

import com.google.gson.annotations.SerializedName

data class AddProductCartData(

    @field:SerializedName("quantity")
    var quantity: Int? = null,

    @field:SerializedName("productId")
    var productId: String? = null,

    @field:SerializedName("userId")
    var userId: String? = null
)
