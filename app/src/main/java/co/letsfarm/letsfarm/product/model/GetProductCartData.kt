package co.letsfarm.letsfarm.product.model

import com.google.gson.annotations.SerializedName

data class GetProductCartData(

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("quantity")
	val quantity: Int? = null,

	@field:SerializedName("deleted")
	val deleted: Boolean? = null,

	@field:SerializedName("total_price")
	val totalPrice: Int? = null,

	@field:SerializedName("productId")
	val productId: ProductId? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("cart_type")
	val cartType: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("userId")
	val userId: UserId? = null,

	@field:SerializedName("checkout_status")
	val checkoutStatus: Boolean? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)

data class ProductId(

	@field:SerializedName("remaining_in_stock")
	val remainingInStock: Int? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("product_status")
	val productStatus: ProductStatus? = null,

	@field:SerializedName("product_name")
	val productName: String? = null,

	@field:SerializedName("total_in_stock")
	val totalInStock: Int? = null,

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("deleted")
	val deleted: Boolean? = null,

	@field:SerializedName("product_type")
	val productType: ProductType? = null,

	@field:SerializedName("display_img")
	val displayImg: String? = null,

	@field:SerializedName("price")
	val price: Int? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("product_location")
	val productLocation: String? = null,

	@field:SerializedName("currency")
	val currency: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)

data class UserId(

	@field:SerializedName("full_name")
	val fullName: String? = null,

	@field:SerializedName("last_name")
	val lastName: String? = null,

	@field:SerializedName("phone_number")
	val phoneNumber: String? = null,

	@field:SerializedName("_id")
	val _id: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("email")
	val email: String? = null
)
//
//data class ProductType(
//
//	@field:SerializedName("createdAt")
//	val createdAt: String? = null,
//
//	@field:SerializedName("deleted")
//	val deleted: Boolean? = null,
//
//	@field:SerializedName("__v")
//	val V: Int? = null,
//
//	@field:SerializedName("name")
//	val name: String? = null,
//
//	@field:SerializedName("_id")
//	val id: String? = null,
//
//	@field:SerializedName("status")
//	val status: Boolean? = null,
//
//	@field:SerializedName("updatedAt")
//	val updatedAt: String? = null
//)
//
//data class ProductStatus(
//
//	@field:SerializedName("createdAt")
//	val createdAt: String? = null,
//
//	@field:SerializedName("deleted")
//	val deleted: Boolean? = null,
//
//	@field:SerializedName("__v")
//	val V: Int? = null,
//
//	@field:SerializedName("name")
//	val name: String? = null,
//
//	@field:SerializedName("can_order")
//	val canOrder: Boolean? = null,
//
//	@field:SerializedName("_id")
//	val id: String? = null,
//
//	@field:SerializedName("status")
//	val status: Boolean? = null,
//
//	@field:SerializedName("updatedAt")
//	val updatedAt: String? = null
//)
