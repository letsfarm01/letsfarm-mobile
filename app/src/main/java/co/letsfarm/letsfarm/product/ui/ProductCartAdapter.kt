package co.letsfarm.letsfarm.product.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.product.model.GetProductCartData
import co.letsfarm.letsfarm.utils.getMoney
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.holder_product_cart.view.*

class ProductCartAdapter(val deleteListener:(String)->Unit) : RecyclerView.Adapter<ProductCartViewHolder>() {

    var cartData: List<GetProductCartData?>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductCartViewHolder {
        LayoutInflater.from(parent.context).inflate(R.layout.holder_product_cart, parent, false).run {
            return ProductCartViewHolder(this)
        }
    }

    override fun getItemCount() = cartData?.size ?: 0

    override fun onBindViewHolder(holder: ProductCartViewHolder, position: Int) {
        holder.itemView.run {
            cartData?.get(position)?.let {cart->
                val product = cart.productId!!
                Picasso.get().load(product.displayImg).into(farm_img)

                delete_img.setOnClickListener {
                    deleteListener(cart.id!!)
                }

                farm_name_tv.text = product.productName
                location_tv.text = product.productLocation
                min_amount_tv.text = (product.price ?: 0).getMoney + " Per unit"
                number_tv.text = "${cart.quantity} units"
                total_amount_tv.text = "${cart.totalPrice?.getMoney} in total"
            }
        }
    }

    fun setdData(data: List<GetProductCartData?>?) {
        cartData = data
        notifyDataSetChanged()
    }

}

class ProductCartViewHolder(itemView:View):RecyclerView.ViewHolder(itemView)
