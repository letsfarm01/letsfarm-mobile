package co.letsfarm.letsfarm.product.viewModel

import android.app.Application
import androidx.lifecycle.LiveData
import co.letsfarm.letsfarm.allFarms.model.AddCartData
import co.letsfarm.letsfarm.allFarms.model.AddToCartRes
import co.letsfarm.letsfarm.allFarms.model.GetCartData
import co.letsfarm.letsfarm.auth.viewModel.NetworkResource
import co.letsfarm.letsfarm.blog.model.BlogData
import co.letsfarm.letsfarm.dashBoard.model.*
import co.letsfarm.letsfarm.product.model.AddProductCartData
import co.letsfarm.letsfarm.product.model.AllProducts
import co.letsfarm.letsfarm.product.model.GetProductCartData
import co.letsfarm.letsfarm.shippingAddress.model.ProductCheckoutData
import co.letsfarm.letsfarm.shippingAddress.model.ProductOrder
import co.letsfarm.letsfarm.shippingAddress.model.ShippingAddressData
import co.letsfarm.letsfarm.utils.*

class ProductRepository(val apiServices: ApiServices, val application: Application) : BaseRepositiory(apiServices, application) {

    fun fetchAllProduct(): LiveData<Resource<AllProducts>> {
        return object : NetworkResource<AllProducts, AllProducts>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<AllProducts>>> {
                return apiServices.getProducts(token, 1, "100")
            }
        }.asLiveData()
    }


    fun checkoutProduct(data: ProductCheckoutData): LiveData<Resource<CheckOutRes>> {
        return object : NetworkResource<CheckOutRes, CheckOutRes>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<CheckOutRes>>> {
                return apiServices.checkoutProduct(token, data)
            }

        }.asLiveData()
    }
    fun checkoutSavedCardProduct(data: CheckOutSavedCard): LiveData<Resource<CheckOutRes>> {
        return object : NetworkResource<CheckOutRes, CheckOutRes>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<CheckOutRes>>> {
                return apiServices.checkoutSavedCardProduct(token, data)
            }

        }.asLiveData()
    }

    fun addToCart(data: AddProductCartData): LiveData<Resource<AddToCartRes?>> {
        return object : NetworkResource<AddToCartRes?, AddToCartRes?>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<AddToCartRes?>>> {
                return apiServices.addProductToCart(token, data)
            }

        }.asLiveData()
    }
    fun getCart(): LiveData<Resource<List<GetProductCartData?>>> {
        return object : NetworkResource<List<GetProductCartData?>, List<GetProductCartData?>>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<List<GetProductCartData?>>>> {
                return apiServices.getProductCart(token, userId)
            }

        }.asLiveData()
    }
    fun deleteCartItem(id:String): LiveData<Resource<Any>> {
        return object : NetworkResource<Any, Any>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<Any>>> {
                return apiServices.deleteProductCartItem(token, id)
            }

        }.asLiveData()
    }
    fun getCartCount(): LiveData<Resource<String?>> {
        return object : NetworkResource<String?, String?>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<String?>>> {
                return apiServices.getProductCartCount(token, userId)
            }

        }.asLiveData()
    }

    fun getPaymentOption(): LiveData<Resource<List<PaymentOptionRes?>>> {
        return object : NetworkResource<List<PaymentOptionRes?>, List<PaymentOptionRes?>>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<List<PaymentOptionRes?>>>> {
                return apiServices.getPaymentOption(token)
            }

        }.asLiveData()
    }

    fun getCardPaymentCharges(totalPrice: Int): LiveData<Resource<CardPaymentCharges>> {
        return object : NetworkResource<CardPaymentCharges, CardPaymentCharges>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<CardPaymentCharges>>> {
                return apiServices.getCardPaymentCharges(token, totalPrice)
            }
        }.asLiveData()
    }

    fun getWalletBalance(): LiveData<Resource<Map<String, String>>> {
        return object : NetworkResource<Map<String, String>, Map<String, String>>(application.isConnectedToTheInternet()){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<Map<String, String>>>> {
                return apiServices.getWalletBalance(token, userId)
            }

        }.asLiveData()
    }

    fun getShippingAddress(): LiveData<Resource<List<ShippingAddressData?>>> {
        return object : NetworkResource<List<ShippingAddressData?>, List<ShippingAddressData?>>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<List<ShippingAddressData?>>>> {
                return apiServices.getShippingAddress(token, userId)
            }

        }.asLiveData()
    }

    fun getProductOrders(): LiveData<Resource<List<ProductOrder?>>> {
        return object : NetworkResource<List<ProductOrder?>, List<ProductOrder?>>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<List<ProductOrder?>>>> {
                return apiServices.getProductOrders(token, userId)
            }

        }.asLiveData()
    }
}
