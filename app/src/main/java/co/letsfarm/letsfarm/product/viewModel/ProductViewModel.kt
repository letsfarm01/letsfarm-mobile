package co.letsfarm.letsfarm.product.viewModel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.switchMap
import co.letsfarm.letsfarm.allFarms.model.AddCartData
import co.letsfarm.letsfarm.allFarms.model.AddToCartRes
import co.letsfarm.letsfarm.allFarms.model.GetCartData
import co.letsfarm.letsfarm.common.utils.AbsentLiveData
import co.letsfarm.letsfarm.dashBoard.model.CardPaymentCharges
import co.letsfarm.letsfarm.dashBoard.model.CheckOutRes
import co.letsfarm.letsfarm.dashBoard.model.CheckOutSavedCard
import co.letsfarm.letsfarm.dashBoard.model.PaymentOptionRes
import co.letsfarm.letsfarm.product.model.AddProductCartData
import co.letsfarm.letsfarm.product.model.AllProducts
import co.letsfarm.letsfarm.product.model.GetProductCartData
import co.letsfarm.letsfarm.product.model.ProductsData
import co.letsfarm.letsfarm.shippingAddress.model.ProductCheckoutData
import co.letsfarm.letsfarm.shippingAddress.model.ProductOrder
import co.letsfarm.letsfarm.shippingAddress.model.ShippingAddressData
import co.letsfarm.letsfarm.utils.BaseViewModel
import co.letsfarm.letsfarm.utils.Resource

class ProductViewModel(val repository: ProductRepository, val app: Application): BaseViewModel(repository, app) {


    private val allproductDataLive= MutableLiveData<Boolean>()

    fun fetchAllProduct() {
        allproductDataLive.value=true
    }

    val allProductRequest: LiveData<Resource<AllProducts>> = allproductDataLive.switchMap { data->
        if (data != null) {
            repository.fetchAllProduct()
        }else{
            AbsentLiveData.create()
        }
    }


    private val addToCartLive= MutableLiveData<AddProductCartData?>()

    fun addToCart(data: AddProductCartData?){
        addToCartLive.value=data
    }

    val addToCartRequest: LiveData<Resource<AddToCartRes?>> = addToCartLive.switchMap { data->
        if (data != null) {
            repository.addToCart(data)
        }else{
            AbsentLiveData.create()
        }
    }


    private val getCartCountLive= MutableLiveData<Boolean>()

    fun getCartCount(){
        getCartCountLive.value=true
    }

    val getCartCountRequest: LiveData<Resource<String?>> = getCartCountLive.switchMap { allFarmsData->
        if (allFarmsData != null) {
            repository.getCartCount()
        }else{
            AbsentLiveData.create()
        }
    }

    private val getCartLive= MutableLiveData<Boolean>()

    fun getCart(){
        getCartLive.value=true
    }

    val getCartRequest: LiveData<Resource<List<GetProductCartData?>>> = getCartLive.switchMap { allFarmsData->
        if (allFarmsData != null) {
            repository.getCart()
        }else{
            AbsentLiveData.create()
        }
    }

    private val deleteCartItemLive= MutableLiveData<String>()

    fun deleteCartItem(id:String){
        deleteCartItemLive.value=id
    }

    val deleteCartItemRequest: LiveData<Resource<Any>> = deleteCartItemLive.switchMap { data->
        if (data != null) {
            repository.deleteCartItem(data)
        }else{
            AbsentLiveData.create()
        }
    }


    private val getPaymentOptionLive= MutableLiveData<Boolean>()

    fun getPaymentOption(){
        getPaymentOptionLive.value=true
    }
    val getPaymentOptionRequest: LiveData<Resource<List<PaymentOptionRes?>>> = getPaymentOptionLive.switchMap { allFarmsData->
        if (allFarmsData != null) {
            repository.getPaymentOption()
        }else{
            AbsentLiveData.create()
        }
    }

    private val checkoutLive= MutableLiveData<ProductCheckoutData>()

    fun checkout(data: ProductCheckoutData?) {
        checkoutLive.value=data
    }

    val checkoutRequest: LiveData<Resource<CheckOutRes>> = checkoutLive.switchMap { data->
        if (data != null) {
            repository.checkoutProduct(data)
        }else{
            AbsentLiveData.create()
        }
    }

    private val checkoutSavedCardLive= MutableLiveData<CheckOutSavedCard>()

    fun checkoutSavedCard(id: CheckOutSavedCard?) {
        checkoutSavedCardLive.value=id
    }

    val checkoutSavedCardRequest: LiveData<Resource<CheckOutRes>> = checkoutSavedCardLive.switchMap { data->
        if (data != null) {
            repository.checkoutSavedCardProduct(data)
        }else{
            AbsentLiveData.create()
        }
    }

//    private val verifyTransactionLive= MutableLiveData<String>()
//    var isWalletFunding:Boolean? = null
//
//    fun verifyTransaction(transactionId: String, walletFunding: Boolean) {
//        this.isWalletFunding = walletFunding
//        verifyTransactionLive.value=transactionId
//    }
//
//    val verifyTransactionRequest: LiveData<Resource<Any>> = verifyTransactionLive.switchMap { data->
//        if (data != null) {
//            repository.verifyTransaction(data, isWalletFunding!!)
//        }else{
//            AbsentLiveData.create()
//        }
//    }


    private val getWalletBalanceLive= MutableLiveData<Boolean>()

    fun getWalletBalance(){
        getWalletBalanceLive.value=true
    }

    val getWalletBalanceRequest: LiveData<Resource<Map<String, String>>> = getWalletBalanceLive.switchMap { loginData->
        if (loginData != null) {
            repository.getWalletBalance()
        }else{
            AbsentLiveData.create()
        }
    }

    private val getCardPaymentChargesLive= MutableLiveData<Int>()

    fun getCardPaymentCharges(totalPrice: Int) {
        getCardPaymentChargesLive.value=totalPrice
    }

    val getCardPaymentChargesRequest: LiveData<Resource<CardPaymentCharges>> = getCardPaymentChargesLive.switchMap { data->
        if (data != null) {
            repository.getCardPaymentCharges(data)
        }else{
            AbsentLiveData.create()
        }
    }


    private val getShippingAddressLive= MutableLiveData<Boolean>()

    fun getShippingAddress() {
        getShippingAddressLive.value=true
    }

    val getShippingAddressRequest: LiveData<Resource<List<ShippingAddressData?>>> = getShippingAddressLive.switchMap { data->
        if (data != null) {
            repository.getShippingAddress()
        }else{
            AbsentLiveData.create()
        }
    }
    private val getProductOrdersLive= MutableLiveData<Boolean>()

    fun getProductOrders() {
        getProductOrdersLive.value=true
    }

    val getProductOrdersRequest: LiveData<Resource<List<ProductOrder?>>> = getProductOrdersLive.switchMap { data->
        if (data != null) {
            repository.getProductOrders()
        }else{
            AbsentLiveData.create()
        }
    }
}