package co.letsfarm.letsfarm.profile.model

import com.google.gson.annotations.SerializedName

data class Bank(

	@field:SerializedName("country")
	val country: String? = null,

	@field:SerializedName("code")
	val code: String? = null,

	@field:SerializedName("pay_with_bank")
	val payWithBank: Boolean? = null,

	@field:SerializedName("longcode")
	val longcode: String? = null,

	@field:SerializedName("active")
	val active: Boolean? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("is_deleted")
	val isDeleted: Any? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("currency")
	val currency: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("slug")
	val slug: String? = null,

	@field:SerializedName("gateway")
	val gateway: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)
