package co.letsfarm.letsfarm.profile.model

import com.google.gson.annotations.SerializedName

data class UpdateProfileData(
	@field:SerializedName("_id")
	var id: String? = null,

	@field:SerializedName("company_phone")
	var companyPhone: String? = null,

	@field:SerializedName("country")
	var country: String? = null,

	@field:SerializedName("account_number")
	var accountNumber: String? = null,

	@field:SerializedName("user_image_url")
	var userImageUrl: String? = null,

	@field:SerializedName("role")
	var role: String? = null,

	@field:SerializedName("gender")
	var gender: String? = null,

	@field:SerializedName("city")
	var city: String? = null,

	@field:SerializedName("company_email")
	var companyEmail: String? = null,

	@field:SerializedName("channel_notice")
	var channelNotice: String? = null,

	@field:SerializedName("company_address")
	var companyAddress: String? = null,

	@field:SerializedName("password")
	var password: String? = null,

	@field:SerializedName("account_name")
	var accountName: String? = null,

	@field:SerializedName("bank_name")
	var bankName: String? = null,

	@field:SerializedName("bank_code")
	var bankCode: String? = null,

//	@field:SerializedName("email")
//	var nextKinEmail: String? = null,

	@field:SerializedName("bvn")
	var bvn: String? = null,

	@field:SerializedName("relationship")
	var relationship: String? = null,

	@field:SerializedName("first_name")
	var firstName: String? = null,

	@field:SerializedName("email")
	var email: String? = null,

	@field:SerializedName("address")
	var address: String? = null,

//	@field:SerializedName("phone_number")
//	var nextKinPhoneNumber: String? = null,

	@field:SerializedName("last_name")
	var lastName: String? = null,

	@field:SerializedName("state_province")
	var stateProvince: String? = null,

	@field:SerializedName("is_verified")
	var isVerified: Boolean? = null,

	@field:SerializedName("userId")
	var userId: String? = null,

	@field:SerializedName("dob")
	var dob: String? = null,

	@field:SerializedName("referral_code")
	var referralCode: String? = null,

	@field:SerializedName("company_name")
	var companyName: String? = null,

	@field:SerializedName("name")
	var nextKinName: String? = null,

	@field:SerializedName("phone_number")
	var phoneNumber: String? = null,

	@field:SerializedName("old_password")
	var oldPassword: String? = null,

	@field:SerializedName("new_password")
	var new_password: String? = null,

	@field:SerializedName("confirm_password")
	var confirmPassword: String? = null,

	@field:SerializedName("username")
	var username: String? = null
)
