package co.letsfarm.letsfarm.profile.ui

import android.os.Bundle
import co.letsfarm.letsfarm.BaseActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import br.com.ilhasoft.support.validation.Validator
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.profile.model.Bank
import co.letsfarm.letsfarm.profile.viewModel.ProfileViewModel
import co.letsfarm.letsfarm.databinding.ActivityBankInfoBinding
import co.letsfarm.letsfarm.utils.*
import dagger.Component
import kotlinx.android.synthetic.main.activity_bank_info.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import javax.inject.Inject
import javax.inject.Singleton

class BankInfoActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: ProfileViewModel
    lateinit var binding:ActivityBankInfoBinding
    lateinit var validator: Validator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_bank_info)

        DaggerBankInfoComponent.builder()
            .profileModule(
                ProfileModule(
                    application
                )
            )
            .build()
            .inject(this)

        binding.viewModel = viewModel
        validator = Validator(binding)

        setViews()
        viewModel.fetchBankInfo()
        viewModel.fetchBanks()
        setObservers()
    }

    var banks:List<Bank>?=null

    private fun setObservers() {
        viewModel.fetchBanksRequest.observe(this, Observer {resource->
            when(resource){
                is Resource.Success->{
                    banks = resource.data
                    val bankNames = getBankNames(banks!!)
                    viewModel.bankNames.value = bankNames
                    binding.banks = bankNames
                    setUserBank()
                }
                is Resource.Error->{
                    processError(resource.message, resource.responseCode)
                }
            }
        })
        viewModel.updateBankInfoRequest.observe(this, Observer {resource->
            update_btn.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success->{
                    showToast("Successful")
                    onBackPressed()
                }
                is Resource.Error->{
                    processError(resource.message, resource.responseCode)
                }
            }
        })
        viewModel.fetchBankInfoRequest.observe(this, Observer {resource->
            progress.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success->{
                    resource.data?.let {
                        viewModel.updateProfileData = it
                        binding.viewModel = viewModel
                        setUserBank()
                    }
                }
                is Resource.Error->{
                    processError(resource.message, resource.responseCode)
                }
            }
        })

        viewModel.bankNames.observe(this, Observer {
            binding.banks = it
        })

        viewModel.selectedBankPosition.observe(this, Observer {
            if(it == 0){
                viewModel.updateProfileData.bankName = null
            }
            else {
                banks?.get(it-1)?.let {bank->
                    viewModel.updateProfileData.bankName = bank.name
                    viewModel.updateProfileData.bankCode = bank.code
                }
            }
        })
    }

    private fun setUserBank() {
        if (viewModel.bankNames.value != null && viewModel.updateProfileData.bankName != null){
            viewModel.selectedBankPosition.value = viewModel.bankNames.value.getPosition(viewModel.updateProfileData.bankName)
            binding.viewModel = viewModel
        }
    }

    private fun setViews() {
        update_btn.setOnClickListener {
            if (validator.validate()){
                viewModel.updateBankInfo()
            }
        }
        toolbar_title_tv.text = "Bank Information"
        back_img.setOnClickListener { onBackPressed() }
    }

    private fun getBankNames(bank: List<Bank>): Array<String?> {
        val bankNames = arrayOfNulls<String>(bank.size+1)
        bankNames[0] = "Select bank"
        bank.forEachIndexed { index, bankDataItem ->
            bankDataItem.name?.let {
                bankNames[index+1] = it
            }
        }
        return bankNames
    }
}

@Singleton
@Component(modules = [ProfileModule::class])
interface BankInfoComponent {
    fun inject(activity: BankInfoActivity)
}