package co.letsfarm.letsfarm.profile.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import br.com.ilhasoft.support.validation.Validator
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.databinding.ActivityChangePasswordBinding
import co.letsfarm.letsfarm.profile.viewModel.ProfileViewModel
import co.letsfarm.letsfarm.utils.Resource
import co.letsfarm.letsfarm.utils.showErrorDialog
import co.letsfarm.letsfarm.utils.showToast
import co.letsfarm.letsfarm.utils.trimmedText
import dagger.Component
import kotlinx.android.synthetic.main.activity_change_password.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import javax.inject.Inject
import javax.inject.Singleton

class ChangePasswordActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModel: ProfileViewModel
    lateinit var binding:ActivityChangePasswordBinding
    lateinit var validator: Validator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_password)

        DaggerChangePasswordComponent.builder()
            .profileModule(
                ProfileModule(
                    application
                )
            )
            .build()
            .inject(this)

        binding.viewModel = viewModel
        validator = Validator(binding)

        setViews()
        setObservers()
    }

    private fun setObservers() {
        viewModel.changePasswordRequest.observe(this, Observer {resource->
            update_btn.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success->{
                    showToast("Successful")
                    onBackPressed()
                }
                is Resource.Error->{
                    showErrorDialog(message = resource.message)
                }
            }
        })
    }

    private fun setViews() {
        update_btn.setOnClickListener {
            if (validator.validate() && doPasswordsMatch()){
                viewModel.changePassword()
            }
        }

        toolbar_title_tv.text = "Change password"
        back_img.setOnClickListener { onBackPressed() }
    }

    private fun doPasswordsMatch(): Boolean {
        val valid = password_et.trimmedText == confirm_password_et.trimmedText
        if (!valid){
            password_et.error = "Passwords don't match"
            confirm_password_et.error = "Passwords don't match"
        }
        return valid
    }
}
@Singleton
@Component(modules = [ProfileModule::class])
interface ChangePasswordComponent {
    fun inject(activity: ChangePasswordActivity)
}