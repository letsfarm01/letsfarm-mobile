package co.letsfarm.letsfarm.profile.ui

import android.os.Bundle
import co.letsfarm.letsfarm.BaseActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import br.com.ilhasoft.support.validation.Validator
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.profile.viewModel.ProfileViewModel
import co.letsfarm.letsfarm.databinding.ActivityNextKinInfoBinding
import co.letsfarm.letsfarm.utils.Resource
import co.letsfarm.letsfarm.utils.load
import co.letsfarm.letsfarm.utils.processError

import co.letsfarm.letsfarm.utils.showToast
import dagger.Component
import kotlinx.android.synthetic.main.activity_next_kin_info.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import javax.inject.Inject
import javax.inject.Singleton

class NextKinInfoActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: ProfileViewModel
    lateinit var binding:ActivityNextKinInfoBinding
    lateinit var validator: Validator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_next_kin_info)

        DaggerNextKinInfoComponent.builder()
            .profileModule(
                ProfileModule(
                    application
                )
            )
            .build()
            .inject(this)

        binding.viewModel = viewModel
        validator = Validator(binding)

        setViews()
        viewModel.fetchNextKinInfo()
        setObservers()
    }

    private fun setObservers() {
        viewModel.updateNextKinInfoRequest.observe(this, Observer {resource->
            update_btn.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success->{
                    showToast("Successful")
                    onBackPressed()
                }
                is Resource.Error->{
                    processError(resource.message, resource.responseCode)
                }
            }
        })
        viewModel.fetchNextKinInfoRequest.observe(this, Observer {resource->
            progress.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success->{
                    resource.data?.let {
//                        id = it.id
                        viewModel.updateProfileData = it
                        binding.viewModel = viewModel
                    }
                }
                is Resource.Error->{
//                    processError(resource.message, resource.responseCode)
                }
            }
        })
    }

//    var id:String? = null

    private fun setViews() {
        update_btn.setOnClickListener {
            if (validator.validate()){
//                viewModel.updateProfileData.id = id
                viewModel.updateNextKinInfo()
            }
        }
        toolbar_title_tv.text = "Next of kin Information"
        back_img.setOnClickListener { onBackPressed() }
    }
}@Singleton
@Component(modules = [ProfileModule::class])
interface NextKinInfoComponent {
    fun inject(activity: NextKinInfoActivity)
}