package co.letsfarm.letsfarm.profile.ui

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.app.Application
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.widget.DatePicker
import co.letsfarm.letsfarm.BaseActivity
import androidx.core.app.ActivityCompat
import androidx.core.graphics.drawable.toDrawable
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat
import br.com.ilhasoft.support.validation.Validator
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.profile.viewModel.ProfileRespository
import co.letsfarm.letsfarm.profile.viewModel.ProfileViewModel
import co.letsfarm.letsfarm.databinding.ActivityPersonalInfoBinding
import co.letsfarm.letsfarm.utils.*
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.ktx.storage
import com.squareup.picasso.Picasso
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import dagger.Component
import dagger.Module
import dagger.Provides
import kotlinx.android.synthetic.main.activity_personal_info.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton


class PersonalInfoActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: ProfileViewModel
    lateinit var binding:ActivityPersonalInfoBinding
    lateinit var validator: Validator
    lateinit var storage: FirebaseStorage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_personal_info)

        storage = Firebase.storage

        DaggerPersonalInfoComponent.builder()
            .profileModule(ProfileModule(application))
            .build()
            .inject(this)

        binding.viewModel = viewModel
        validator = Validator(binding)

        viewModel.fetchPersonalInfo()
        setViews()
        setObservers()
    }

    private fun setObservers() {
        viewModel.states.value = resources.getStringArray(R.array.states)
        viewModel.country.value = resources.getStringArray(R.array.countries)
        viewModel.gender.value = resources.getStringArray(R.array.genders)

        viewModel.updatePersonalInfoRequest.observe(this, Observer {resource->
            update_btn.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success->{
                    showToast("Successful")
                    onBackPressed()
                }
                is Resource.Error->{
                    processError(resource.message, resource.responseCode)
                }
            }
        })
        viewModel.fetchPersonalInfoRequest.observe(this, Observer {resource->
            progress.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success->{
                    resource.data?.let {
                        viewModel.updateProfileData = it
                        binding.viewModel = viewModel
                        Picasso.get().load(it.userImageUrl).placeholder(drawable!!.toDrawable(resources)).into(profile_img)
                        setUserState()
                        setUserCountry()
                        setUserGender()

                    }
                }
                is Resource.Error->{
//                    processError(resource.message, resource.responseCode)
                }
            }
        })
        viewModel.selectedStatePosition.observe(this, Observer {
            if(it == 0){
                viewModel.updateProfileData.stateProvince = null
            }
            else
                viewModel.updateProfileData.stateProvince = viewModel.states.value?.get(it)
        })

        viewModel.selectedCountryPosition.observe(this, Observer {
            if(it == 0){
                viewModel.updateProfileData.country = null
            }
            else
                viewModel.updateProfileData.country = viewModel.country.value?.get(it)
        })

        viewModel.selectedGenderPosition.observe(this, Observer {
            if(it == 0){
                viewModel.updateProfileData.gender = null
            }
            else
                viewModel.updateProfileData.gender = viewModel.gender.value?.get(it)
        })
    }

    private fun setUserGender() {
        if (viewModel.gender.value != null && viewModel.updateProfileData.gender != null){
            viewModel.selectedGenderPosition.value = viewModel.gender.value.getPosition(viewModel.updateProfileData.gender)
            binding.viewModel = viewModel
        }
    }

    private fun setUserCountry() {
        if (viewModel.country.value != null && viewModel.updateProfileData.country != null){
            viewModel.selectedCountryPosition.value = viewModel.country.value.getPosition(viewModel.updateProfileData.country)
            binding.viewModel = viewModel
        }
    }

    private fun setUserState() {
        if (viewModel.states.value != null && viewModel.updateProfileData.stateProvince != null){
            viewModel.selectedStatePosition.value = viewModel.states.value.getPosition(viewModel.updateProfileData.stateProvince)
            binding.viewModel = viewModel
        }
    }

    var drawable:Bitmap? = null

    private fun setViews() {

        drawable = R.drawable.ic_profile2.getBitmapFromVectorDrawable(this)

        profile_img.setImageDrawable(VectorDrawableCompat.create(resources, R.drawable.ic_profile2, theme))

        update_btn.setOnClickListener {
            if (validator.validate()){
                viewModel.updateProfileData.stateProvince = state_sp.selectedItem.toString()
                viewModel.updateProfileData.country = country_sp.selectedItem.toString()
                viewModel.updatePersonalInfo()
            }
        }
        toolbar_title_tv.text = "Personal Information"
        back_img.setOnClickListener { onBackPressed() }
        date_of_birth_et.setOnClickListener { setDateOfBirth() }
        profile_img.setOnClickListener { takePicture() }
        profile_img_tv.setOnClickListener { takePicture() }
    }

    private fun takePicture() {
        if (hasPermission()) {
            CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this)
        }
    }

    private fun setDateOfBirth() {
        val currentCalendar = Calendar.getInstance()
        val currentYear = currentCalendar[Calendar.YEAR]-20
        val currentMonth = currentCalendar[Calendar.MONTH]
        val currentDay = currentCalendar[Calendar.DAY_OF_MONTH]

        DatePickerDialog(this,
            DatePickerDialog.OnDateSetListener { view: DatePicker?, year: Int, month: Int, dayOfMonth: Int ->
                val setCalendar = Calendar.getInstance()
                setCalendar[Calendar.YEAR] = year
                setCalendar[Calendar.MONTH] = month
                setCalendar[Calendar.DAY_OF_MONTH] = dayOfMonth

                if (setCalendar.time.after(Date())){
                    showErrorDialog("Your date of birth cannot be a future date")
                }
                else {
                    val date = setCalendar.time.toDateOfBirth()
                    binding.dateOfBirthEt.setText(date)
                }

            }, currentYear, currentMonth, currentDay).show()

    }

    private var profilePictureFile:File? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                val resultUri: Uri = result.uri
                    profile_img.setImageURI(resultUri)

                progress.load(true)
                val storageRef: StorageReference = storage.reference

                    val email = savepref().get("userEmail", "")
                    val imagesRef = storageRef.child("profile_image/$email.jpg")

                    val uploadTask = imagesRef.putFile(resultUri)

                    uploadTask.addOnFailureListener {

                        progress.load(false)
                        showErrorDialog("${it.message ?: "An error occurred! Try again"}")

                    }.addOnSuccessListener {

                        progress.load(false)
                        val imageUrl = "https://firebasestorage.googleapis.com/v0/b/letsfarm-33131.appspot.com/o/profile_image%2F${email}.jpg?alt=media"
                        viewModel.updateProfileData.userImageUrl = imageUrl
                    }

//                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    fun hasPermission(): Boolean {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
            || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
            || ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                // Explain to the user why we need to read the contacts
            }

            requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA),
                202
            )

            // MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE is an
            // app-defined int constant

            return false
        }
        return true
    }
}

fun Date.toDateOfBirth() : String {
    val dateFormatter = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault())
    return dateFormatter.format(this)
}

@Singleton
@Component(modules = [ProfileModule::class])
interface PersonalInfoComponent {
    fun inject(activity: PersonalInfoActivity)
}

@Module
class ProfileModule(private val application: Application) : NetModule() {

    @Provides
    fun provideProfileViewModel(repo: ProfileRespository) : ProfileViewModel {
        return ProfileViewModel(
            repo,
            application
        )
    }

    @Provides
    fun provideRepository(service: ApiServices) : ProfileRespository {
        return ProfileRespository(
            service,
            application
        )
    }
}