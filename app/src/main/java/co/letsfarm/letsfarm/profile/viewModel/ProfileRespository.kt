package co.letsfarm.letsfarm.profile.viewModel

import android.app.Application
import androidx.lifecycle.LiveData
import co.letsfarm.letsfarm.dashBoard.model.GeneralRes
import co.letsfarm.letsfarm.profile.model.UpdateProfileData
import co.letsfarm.letsfarm.auth.viewModel.NetworkResource
import co.letsfarm.letsfarm.utils.ApiResponse
import co.letsfarm.letsfarm.profile.model.Bank
import co.letsfarm.letsfarm.utils.ApiServices
import co.letsfarm.letsfarm.utils.BaseRepositiory
import co.letsfarm.letsfarm.utils.Resource
import co.letsfarm.letsfarm.utils.isConnectedToTheInternet

class ProfileRespository (val apiServices: ApiServices, val application: Application) : BaseRepositiory(apiServices, application){

    fun fetchBusinessInfo(): LiveData<Resource<UpdateProfileData>> {
        return object : NetworkResource<UpdateProfileData, UpdateProfileData>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<UpdateProfileData>>> {
                return apiServices.fetchBusinessInfo(token, userId)
            }

        }.asLiveData()
    }

    fun fetchBankInfo(): LiveData<Resource<UpdateProfileData>> {
        return object : NetworkResource<UpdateProfileData, UpdateProfileData>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<UpdateProfileData>>> {
                return apiServices.fetchBankInfo(token, userId)
            }

        }.asLiveData()
    }

    fun fetchNextKinInfo(): LiveData<Resource<UpdateProfileData>> {
        return object : NetworkResource<UpdateProfileData, UpdateProfileData>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<UpdateProfileData>>> {
                return apiServices.fetchNextKinInfo(token, userId)
            }

        }.asLiveData()
    }
    fun fetchPersonalInfo(): LiveData<Resource<UpdateProfileData>> {
        return object : NetworkResource<UpdateProfileData, UpdateProfileData>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<UpdateProfileData>>> {
                return apiServices.fetchPersonalInfo(token, userId)
            }

        }.asLiveData()
    }

    fun updatePersonalInfo(data: UpdateProfileData): LiveData<Resource<Any>> {
        return object : NetworkResource<Any, Any>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<Any>>> {
                return apiServices.updatePersonalInfo(token, userId, data)
            }

        }.asLiveData()
    }

    fun updateBusinessInfo(data: UpdateProfileData): LiveData<Resource<Any>> {
        return object : NetworkResource<Any, Any>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<Any>>> {
                return apiServices.updateBusinessInfo(token, userId, data)
            }

        }.asLiveData()
    }

    fun updateBankInfo(data: UpdateProfileData): LiveData<Resource<Any>> {
        return object : NetworkResource<Any, Any>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<Any>>> {
                return apiServices.updateBankInfo(token, userId, data)
            }

        }.asLiveData()
    }

    fun updateNextKinInfo(data: UpdateProfileData): LiveData<Resource<Any>> {
        return object : NetworkResource<Any, Any>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<Any>>> {
                return apiServices.updateNextKinInfo(token, userId, data)
            }

        }.asLiveData()
    }

    fun changePassword(data: UpdateProfileData): LiveData<Resource<Any>> {
        return object : NetworkResource<Any, Any>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<Any>>> {
                return apiServices.changePassword(token, data)
            }

        }.asLiveData()
    }


    fun fetchBanks(): LiveData<Resource<List<Bank>>> {
        return object : NetworkResource<List<Bank>, List<Bank>>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<List<Bank>>>> {
                return apiServices.fetchBanks()
            }

        }.asLiveData()
    }

}
