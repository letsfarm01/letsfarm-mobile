package co.letsfarm.letsfarm.profile.viewModel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import co.letsfarm.letsfarm.profile.model.UpdateProfileData
import co.letsfarm.letsfarm.common.utils.AbsentLiveData
import co.letsfarm.letsfarm.profile.model.Bank
import co.letsfarm.letsfarm.utils.BaseViewModel
import co.letsfarm.letsfarm.utils.Resource
import co.letsfarm.letsfarm.utils.get
import co.letsfarm.letsfarm.utils.savepref
import kotlinx.coroutines.launch

class ProfileViewModel(val repository: ProfileRespository, val app: Application): BaseViewModel(repository, app){

    var states:MutableLiveData<Array<String?>> = MutableLiveData()
    var selectedStatePosition:MutableLiveData<Int> = MutableLiveData()

    var country:MutableLiveData<Array<String?>> = MutableLiveData()
    var selectedCountryPosition:MutableLiveData<Int> = MutableLiveData()

    var gender:MutableLiveData<Array<String?>> = MutableLiveData()
    var selectedGenderPosition:MutableLiveData<Int> = MutableLiveData()

    var updateProfileData = UpdateProfileData().apply {
        userId = app.savepref().get("userId", "")
    }

    private val changePasswordLive= MutableLiveData<Boolean>()

    fun changePassword(){
        changePasswordLive.value=true
    }

    val changePasswordRequest: LiveData<Resource<Any>> = changePasswordLive.switchMap { loginData->
        if (loginData != null) {
            viewModelScope.launch{
            }
            repository.changePassword(updateProfileData)
        }else{
            AbsentLiveData.create()
        }
    }

    private val updatePersonalInfoLive= MutableLiveData<Boolean>()

    fun updatePersonalInfo(){
        updatePersonalInfoLive.value=true
    }

    val updatePersonalInfoRequest: LiveData<Resource<Any>> = updatePersonalInfoLive.switchMap { loginData->
        if (loginData != null) {
            viewModelScope.launch{
            }
            repository.updatePersonalInfo(updateProfileData)
        }else{
            AbsentLiveData.create()
        }
    }

    private val updateBusinessInfoLive= MutableLiveData<Boolean>()

    fun updateBusinessInfo(){
        updateBusinessInfoLive.value=true
    }

    val updateBusinessInfoRequest: LiveData<Resource<Any>> = updateBusinessInfoLive.switchMap { loginData->
        if (loginData != null) {
            viewModelScope.launch{
            }
            repository.updateBusinessInfo(updateProfileData)
        }else{
            AbsentLiveData.create()
        }
    }

    private val updateBankInfoLive= MutableLiveData<Boolean>()

    fun updateBankInfo(){
        updateBankInfoLive.value=true
    }

    val updateBankInfoRequest: LiveData<Resource<Any>> = updateBankInfoLive.switchMap { loginData->
        if (loginData != null) {
            viewModelScope.launch{
            }
            repository.updateBankInfo(updateProfileData)
        }else{
            AbsentLiveData.create()
        }
    }

    private val updateNextKinInfoLive= MutableLiveData<Boolean>()

    fun updateNextKinInfo(){
        updateNextKinInfoLive.value=true
    }

    val updateNextKinInfoRequest: LiveData<Resource<Any>> = updateNextKinInfoLive.switchMap { loginData->
        if (loginData != null) {
            viewModelScope.launch{
            }
            repository.updateNextKinInfo(updateProfileData)
        }else{
            AbsentLiveData.create()
        }
    }

    private val fetchBusinessInfoLive= MutableLiveData<Boolean>()

    fun fetchBusinessInfo(){
        fetchBusinessInfoLive.value=true
    }

    val fetchBusinessInfoInfoRequest: LiveData<Resource<UpdateProfileData>> = fetchBusinessInfoLive.switchMap { loginData->
        if (loginData != null) {
            viewModelScope.launch{
            }
            repository.fetchBusinessInfo()
        }else{
            AbsentLiveData.create()
        }
    }

    private val fetchBankInfoLive= MutableLiveData<Boolean>()

    fun fetchBankInfo(){
        fetchBankInfoLive.value=true
    }

    val fetchBankInfoRequest: LiveData<Resource<UpdateProfileData>> = fetchBankInfoLive.switchMap { loginData->
        if (loginData != null) {
            viewModelScope.launch{
            }
            repository.fetchBankInfo()
        }else{
            AbsentLiveData.create()
        }
    }

    private val fetchNextKinInfoLive= MutableLiveData<Boolean>()

    fun fetchNextKinInfo(){
        fetchNextKinInfoLive.value=true
    }

    val fetchNextKinInfoRequest: LiveData<Resource<UpdateProfileData>> = fetchNextKinInfoLive.switchMap { loginData->
        if (loginData != null) {
            viewModelScope.launch{
            }
            repository.fetchNextKinInfo()
        }else{
            AbsentLiveData.create()
        }
    }


    private val fetchPersonalInfoLive= MutableLiveData<Boolean>()

    fun fetchPersonalInfo(){
        fetchPersonalInfoLive.value=true
    }

    val fetchPersonalInfoRequest: LiveData<Resource<UpdateProfileData>> = fetchPersonalInfoLive.switchMap { loginData->
        if (loginData != null) {
            viewModelScope.launch{
            }
            repository.fetchPersonalInfo()
        }else{
            AbsentLiveData.create()
        }
    }

    var bankNames:MutableLiveData<Array<String?>> = MutableLiveData()
    var selectedBankPosition:MutableLiveData<Int> = MutableLiveData()

    private val fetchBanksLive= MutableLiveData<Boolean>()

    fun fetchBanks(){
        fetchBanksLive.value=true
    }

    val fetchBanksRequest: LiveData<Resource<List<Bank>>> = fetchBanksLive.switchMap { loginData->
        if (loginData != null) {
            viewModelScope.launch{
            }
            repository.fetchBanks()
        }else{
            AbsentLiveData.create()
        }
    }
}
