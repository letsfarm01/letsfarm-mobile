package co.letsfarm.letsfarm.shippingAddress.model

import com.google.gson.annotations.SerializedName

data class ProductCheckoutData(

    @field:SerializedName("payment_gateway")
    var paymentGateway: String? = null,

    @field:SerializedName("shipping_address")
    var shippingAddress: String? = null

)