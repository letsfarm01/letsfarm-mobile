package co.letsfarm.letsfarm.shippingAddress.model

import com.google.gson.annotations.SerializedName

data class ProductOrder(

	@field:SerializedName("quantity")
	val quantity: Int? = null,

	@field:SerializedName("productId")
	val productId: ProductId? = null,

	@field:SerializedName("total_price")
	val totalPrice: Int? = null,

	@field:SerializedName("cartId")
	val cartId: String? = null,

	@field:SerializedName("userId")
	val userId: String? = null,

	@field:SerializedName("product_name")
	val productName: String? = null,

	@field:SerializedName("qty_remaining_before_order")
	val qtyRemainingBeforeOrder: Int? = null,

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("deleted")
	val deleted: Boolean? = null,

	@field:SerializedName("payment_channel")
	val paymentChannel: PaymentChannel? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("cartType")
	val cartType: String? = null,

	@field:SerializedName("orderReference")
	val orderReference: String? = null,

	@field:SerializedName("currency")
	val currency: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("shipping_address")
	val shippingAddress: ShippingAddress? = null,

	@field:SerializedName("price_per_unit")
	val pricePerUnit: Int? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)

data class PaymentChannel(

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("is_active")
	val isActive: Boolean? = null,

	@field:SerializedName("deleted")
	val deleted: Boolean? = null,

	@field:SerializedName("can_fund")
	val canFund: Boolean? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)

data class ProductId(

	@field:SerializedName("remaining_in_stock")
	val remainingInStock: Int? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("product_status")
	val productStatus: String? = null,

	@field:SerializedName("product_name")
	val productName: String? = null,

	@field:SerializedName("total_in_stock")
	val totalInStock: Int? = null,

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("deleted")
	val deleted: Boolean? = null,

	@field:SerializedName("product_type")
	val productType: String? = null,

	@field:SerializedName("display_img")
	val displayImg: String? = null,

	@field:SerializedName("price")
	val price: Int? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("product_location")
	val productLocation: String? = null,

	@field:SerializedName("currency")
	val currency: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)

data class ShippingAddress(

	@field:SerializedName("country")
	val country: String? = null,

	@field:SerializedName("address_line")
	val addressLine: String? = null,

	@field:SerializedName("alt_phone_number")
	val altPhoneNumber: String? = null,

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("last_name")
	val lastName: String? = null,

	@field:SerializedName("userId")
	val userId: String? = null,

	@field:SerializedName("additional_information")
	val additionalInformation: String? = null,

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("default")
	val jsonMemberDefault: Boolean? = null,

	@field:SerializedName("deleted")
	val deleted: Boolean? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("phone_number")
	val phoneNumber: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("nearest_bus_stop")
	val nearestBusStop: String? = null,

	@field:SerializedName("state")
	val state: String? = null,

	@field:SerializedName("postal_code")
	val postalCode: String? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)
