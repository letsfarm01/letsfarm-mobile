package co.letsfarm.letsfarm.shippingAddress.model

import com.google.gson.annotations.SerializedName

data class ShippingAddressData(

	@field:SerializedName("country")
	var country: String? = null,

	@field:SerializedName("address_line")
	var addressLine: String? = null,

	@field:SerializedName("alt_phone_number")
	var altPhoneNumber: String? = null,

	@field:SerializedName("city")
	var city: String? = null,

	@field:SerializedName("last_name")
	var lastName: String? = null,

	@field:SerializedName("userId")
	var userId: UserId? = null,

	@field:SerializedName("additional_information")
	var additionalInformation: String? = null,

	@field:SerializedName("createdAt")
	var createdAt: String? = null,

	@field:SerializedName("default")
	var mDefault: Boolean? = null,

	@field:SerializedName("deleted")
	var deleted: Boolean? = null,

	@field:SerializedName("__v")
	var V: Int? = null,

	@field:SerializedName("phone_number")
	var phoneNumber: String? = null,

	@field:SerializedName("_id")
	var id: String? = null,

	@field:SerializedName("nearest_bus_stop")
	var nearestBusStop: String? = null,

	@field:SerializedName("state")
	var state: String? = null,

	@field:SerializedName("postal_code")
	var postalCode: String? = null,

	@field:SerializedName("first_name")
	var firstName: String? = null,

	@field:SerializedName("updatedAt")
	var updatedAt: String? = null
)

data class UserId(

	@field:SerializedName("full_name")
	var fullName: String? = null,

	@field:SerializedName("last_name")
	var lastName: String? = null,

	@field:SerializedName("_id")
	var _id: String? = null,

	@field:SerializedName("id")
	var id: String? = null,

	@field:SerializedName("first_name")
	var firstName: String? = null,

	@field:SerializedName("email")
	var email: String? = null
)
