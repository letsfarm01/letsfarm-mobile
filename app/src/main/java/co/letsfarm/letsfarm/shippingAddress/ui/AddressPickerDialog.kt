package co.letsfarm.letsfarm.shippingAddress.ui

import android.app.Dialog
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.bold
import androidx.core.text.scale
import androidx.fragment.app.DialogFragment
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.shippingAddress.model.ShippingAddressData
import co.letsfarm.letsfarm.utils.GenericListAdapter
import co.letsfarm.letsfarm.utils.navigateTo
import kotlinx.android.synthetic.main.fragment_address_picker_dialog.*
import kotlinx.android.synthetic.main.holder_shipping_address_picker.view.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class AddressPickerDialog(val addressList: List<ShippingAddressData?>, val listener:(String)->Unit) : DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_address_picker_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setViews()
        setAdapter()
    }

    private fun setAdapter() {
        shipping_address_rv.adapter =
            GenericListAdapter(addressList, R.layout.holder_shipping_address_picker)
            { view, item, position ->
                view.address_tv.text = SpannableStringBuilder()
                    .scale(1.1F) { bold { append("${item?.firstName} ${item?.lastName}\n\n")}}
                    .scale(1.1F) { bold {append("Address: ")}}.append("${item?.addressLine} \n")
                    .scale(1.1F) { bold {append("Details: ")}}
                    .append("${item?.nearestBusStop}, ${item?.city}, ${item?.state}, ${item?.country}\n")
                    .scale(1.1F) { bold {append("Mobile: ")}}.append("${item?.phoneNumber}/${item?.altPhoneNumber}\n")
                    .scale(1.1F) { bold {append("Postal code: ")}}.append("${item?.postalCode}\n")
                    .scale(1.1F) { bold {append("More Info: ")}}.append("${item?.additionalInformation}")

                view.setOnClickListener {
                    item?.id?.let { addressId -> listener(addressId) }
                    dismiss()
                }

            }
    }

    private fun setViews() {
        toolbar_title_tv.text = "Select shipping address"
        back_img.setOnClickListener {
            dismiss()
        }
        add_shipping_address_btn.setOnClickListener {
            activity?.navigateTo<CreateShippingAddressActivity>()
            dismiss()
        }
    }

    override fun onStart() {
        super.onStart()
        val dialog: Dialog? = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window?.setLayout(width, height)
        }
    }
}