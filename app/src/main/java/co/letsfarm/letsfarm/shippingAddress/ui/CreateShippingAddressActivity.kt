package co.letsfarm.letsfarm.shippingAddress.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import br.com.ilhasoft.support.validation.Validator
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.databinding.ActivityCreateShippingAddressBinding
import co.letsfarm.letsfarm.shippingAddress.viewmodel.ShippingAddressViewModel
import co.letsfarm.letsfarm.utils.Resource
import co.letsfarm.letsfarm.utils.processError
import co.letsfarm.letsfarm.utils.showToast
import dagger.Component
import kotlinx.android.synthetic.main.activity_create_shipping_address.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import javax.inject.Inject
import javax.inject.Singleton

class CreateShippingAddressActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModel: ShippingAddressViewModel

    lateinit var binding: ActivityCreateShippingAddressBinding
    lateinit var validator: Validator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_create_shipping_address)

        DaggerCreateShippingAddressComponent
            .builder()
            .shippingAddressModule(ShippingAddressModule(application))
            .build()
            .inject(this)

        binding.viewModel = viewModel

        validator = Validator(binding)

        setViews()
        setObservers()
    }

    private fun setViews() {
        toolbar_title_tv.text = "Create address"
        back_img.setOnClickListener { onBackPressed() }
        create_btn.setOnClickListener {
            if (validator.validate() && areSpinnersValid()){
//                viewModel.shippingAddress.state = state_sp.selectedItem.toString()
//                viewModel.shippingAddress.country = country_sp.selectedItem.toString()

                viewModel.createShippingAddress()
            }
        }
    }

    private fun areSpinnersValid(): Boolean {
        var valid = true

        if (viewModel.selectedCountryPosition.value == 0){
            valid = false
            showToast("Select a country")
        }
        if (viewModel.selectedStatePosition.value == 0){
            valid = false
            showToast("Select a state")
        }

        return valid
    }

    private fun setObservers() {
        viewModel.country.value = resources.getStringArray(R.array.countries)
        viewModel.states.value = resources.getStringArray(R.array.states)

        viewModel.selectedCountryPosition.observe(this, Observer {
            if(it == 0){
                viewModel.shippingAddress2.country = null
            }
            else
                viewModel.shippingAddress2.country = viewModel.country.value?.get(it)
        })


        viewModel.selectedStatePosition.observe(this, Observer {
            if(it == 0){
                viewModel.shippingAddress2.state = null
            }
            else
                viewModel.shippingAddress2.state = viewModel.states.value?.get(it)
        })

        viewModel.createShippingAddressRequest.observe(this, Observer {resource->
            create_btn.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success->{
                    showToast("Successful")
                    onBackPressed()
                }
                is Resource.Error->{
                    processError(resource.message, resource.responseCode)
                }
            }
        })
    }

}

@Singleton
@Component(modules = [ShippingAddressModule::class])
interface CreateShippingAddressComponent {
    fun inject(activity: CreateShippingAddressActivity)
}
