package co.letsfarm.letsfarm.shippingAddress.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.dashBoard.ui.AllProductModule
import co.letsfarm.letsfarm.product.ui.AddProductCartBs
import co.letsfarm.letsfarm.product.viewModel.ProductViewModel
import co.letsfarm.letsfarm.shippingAddress.model.ProductOrder
import co.letsfarm.letsfarm.utils.*
import com.squareup.picasso.Picasso
import dagger.Component
import kotlinx.android.synthetic.main.activity_product_orders.*
import kotlinx.android.synthetic.main.holder_product_order.view.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import javax.inject.Inject
import javax.inject.Singleton

class ProductOrdersActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModel: ProductViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_orders)

        DaggerProductOrdersComponent
            .builder()
            .allProductModule(AllProductModule(application))
            .build()
            .inject(this)

        viewModel.getProductOrders()
        setObservers()
        setViews()
    }

    private fun setViews() {
        toolbar_title_tv.text = "Product Orders"
        back_img.setOnClickListener { onBackPressed() }
    }

    private fun setObservers() {
        viewModel.getProductOrdersRequest.observe(this, Observer {resource->
            progress.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success->{
                    resource.data?.let{products->
                        setProductOrderAdapter(products)
                    }
                }
                is Resource.Error->{
                    processError(resource.message, resource.responseCode)
                }
            }
        })
    }


    private fun setProductOrderAdapter(products: List<ProductOrder?>) {
        product_orders_rv.adapter =
            GenericListAdapter(products, R.layout.holder_product_order)
            { view, item, position ->

                view.product_name_tv.text = "${item?.productName}"
                view.quantity_tv.text = "${item?.quantity} units"
                view.unit_price_tv.text = "${item?.pricePerUnit?.getMoney} per unit"
                view.total_price_tv.text = "${item?.totalPrice?.getMoney} in total"
                view.status_tv.text = "${item?.status}"
            }
    }

}

@Singleton
@Component(modules = [AllProductModule::class])
interface ProductOrdersComponent {
    fun inject(activity: ProductOrdersActivity)
}
