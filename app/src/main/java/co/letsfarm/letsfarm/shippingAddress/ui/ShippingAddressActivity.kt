package co.letsfarm.letsfarm.shippingAddress.ui

import android.app.Application
import android.os.Bundle
import android.text.SpannableStringBuilder
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.bold
import androidx.core.text.scale
import androidx.lifecycle.Observer
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.shippingAddress.model.ShippingAddressData
import co.letsfarm.letsfarm.shippingAddress.viewmodel.ShippingAddressRepository
import co.letsfarm.letsfarm.shippingAddress.viewmodel.ShippingAddressViewModel
import co.letsfarm.letsfarm.utils.*
import dagger.Component
import dagger.Module
import dagger.Provides
import kotlinx.android.synthetic.main.activity_shipping_address.*
import kotlinx.android.synthetic.main.holder_shipping_address.view.*

import kotlinx.android.synthetic.main.layout_toolbar.back_img
import kotlinx.android.synthetic.main.layout_toolbar.toolbar_title_tv
import javax.inject.Inject
import javax.inject.Singleton

class ShippingAddressActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModel:ShippingAddressViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shipping_address)

        DaggerShippingAddressComponent
            .builder()
            .shippingAddressModule(ShippingAddressModule(application))
            .build()
            .inject(this)

        setViews()
        setObservers()
    }

    private fun setViews() {
        toolbar_title_tv.text = "Shiping address"
        back_img.setOnClickListener { onBackPressed() }
        add_shipping_address_btn.setOnClickListener { navigateTo<CreateShippingAddressActivity> {  } }
    }

    override fun onResume() {
        super.onResume()
        viewModel.getShippingAddress()
    }

    private fun setObservers() {
        viewModel.getShippingAddress()
        viewModel.getShippingAddressRequest.observe(this, Observer {
            progress_bar.load(it is Resource.Loading)
            when(it){
                is Resource.Success->{

                    val addressList = it.data

                    if (addressList.isNullOrEmpty()){
                        showErrorDialog("No address found")
                        setAdapter(listOf())
                    }else{
                        setAdapter(addressList)
                    }

                }
                is Resource.Error->{
                    processError(it.message, it.responseCode)
                }
            }
        })
        viewModel.deleteShippingAddressRequest.observe(this, Observer {
            progress_bar.load(it is Resource.Loading)
            when(it){
                is Resource.Success->{
                    viewModel.getShippingAddress()
                    processError(it.message, it.responseCode)
                }
                is Resource.Error->{
                    viewModel.getShippingAddress()
                    processError(it.message, it.responseCode)
                }
            }
        })
    }

    private fun setAdapter(addressList: List<ShippingAddressData?>) {
        shipping_address_rv.adapter =
            GenericListAdapter(addressList, R.layout.holder_shipping_address)
            { view, item, position ->
                view.address_tv.text = SpannableStringBuilder()
                    .scale(1.1F) { bold { append("${item?.firstName} ${item?.lastName}\n\n")}}
                    .scale(1.1F) { bold {append("Address: ")}}.append("${item?.addressLine} \n")
                    .scale(1.1F) { bold {append("Details: ")}}
                    .append("${item?.nearestBusStop}, ${item?.city}, ${item?.state}, ${item?.country},\n")
                    .scale(1.1F) { bold {append("Mobile: ")}}.append("${item?.phoneNumber}/${item?.altPhoneNumber}\n")
                    .scale(1.1F) { bold {append("Postal code: ")}}.append("${item?.postalCode}\n")
                    .scale(1.1F) { bold {append("More Info: ")}}.append("${item?.additionalInformation}")


                view.delete_img.setOnClickListener {
                    item?.id?.let { addressId -> deleteShippingAddress(addressId) }
                }

                view.edit_img.setOnClickListener {

                }
            }
    }

    private fun deleteShippingAddress(addressId: String) {
        viewModel.deleteShippingAddress(addressId)
    }

}


@Singleton
@Component(modules = [ShippingAddressModule::class])
interface ShippingAddressComponent {
    fun inject(activity: ShippingAddressActivity)
}

@Module
class ShippingAddressModule(private val application: Application) : NetModule() {

    @Provides
    fun provideShippingAddressViewModel(repo: ShippingAddressRepository) : ShippingAddressViewModel {
        return ShippingAddressViewModel(repo, application)
    }

    @Provides
    fun provideRepository(service: ApiServices) : ShippingAddressRepository {
        return ShippingAddressRepository(service, application)
    }
}