package co.letsfarm.letsfarm.shippingAddress.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import co.letsfarm.letsfarm.auth.viewModel.NetworkResource
import co.letsfarm.letsfarm.dashBoard.model.GeneralRes
import co.letsfarm.letsfarm.shippingAddress.ShippingAddressData2
import co.letsfarm.letsfarm.shippingAddress.model.ShippingAddressData
import co.letsfarm.letsfarm.utils.*

class ShippingAddressRepository(val apiServices: ApiServices, val application: Application) : BaseRepositiory(apiServices, application) {

    fun getShippingAddress(): LiveData<Resource<List<ShippingAddressData?>>> {
        return object : NetworkResource<List<ShippingAddressData?>, List<ShippingAddressData?>>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<List<ShippingAddressData?>>>> {
                return apiServices.getShippingAddress(token, userId)
            }

        }.asLiveData()
    }

    fun getDefaultShippingAddress(): LiveData<Resource<ShippingAddressData?>> {
        return object : NetworkResource<ShippingAddressData?, ShippingAddressData?>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<ShippingAddressData?>>> {
                return apiServices.getDefaultShippingAddress(token, userId)
            }

        }.asLiveData()
    }

    fun createShippingAddress(shippingAddress: ShippingAddressData2): LiveData<Resource<Any?>> {
        return object : NetworkResource<Any?, Any?>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<Any?>>> {
                return apiServices.createShippingAddress(token, shippingAddress)
            }

        }.asLiveData()
    }

    fun deleteShippingAddress(id: String): LiveData<Resource<Any?>> {
        return object : NetworkResource<Any?, Any?>(
            application.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<Any?>>> {
                return apiServices.deleteShippingAddress(token, id)
            }

        }.asLiveData()
    }

}