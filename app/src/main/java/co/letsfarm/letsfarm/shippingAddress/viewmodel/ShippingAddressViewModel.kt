package co.letsfarm.letsfarm.shippingAddress.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.switchMap
import co.letsfarm.letsfarm.common.utils.AbsentLiveData
import co.letsfarm.letsfarm.profile.model.UpdateProfileData
import co.letsfarm.letsfarm.shippingAddress.ShippingAddressData2
import co.letsfarm.letsfarm.shippingAddress.model.ShippingAddressData
import co.letsfarm.letsfarm.utils.BaseViewModel
import co.letsfarm.letsfarm.utils.Resource
import co.letsfarm.letsfarm.utils.get
import co.letsfarm.letsfarm.utils.savepref

class ShippingAddressViewModel(val repository: ShippingAddressRepository, val app: Application): BaseViewModel(repository, app) {


    private val getShippingAddressLive= MutableLiveData<Boolean>()

    fun getShippingAddress() {
        getShippingAddressLive.value=true
    }

    val getShippingAddressRequest: LiveData<Resource<List<ShippingAddressData?>>> = getShippingAddressLive.switchMap { data->
        if (data != null) {
            repository.getShippingAddress()
        }else{
            AbsentLiveData.create()
        }
    }

    var states:MutableLiveData<Array<String?>> = MutableLiveData()
    var selectedStatePosition:MutableLiveData<Int> = MutableLiveData()

    var country:MutableLiveData<Array<String?>> = MutableLiveData()
    var selectedCountryPosition:MutableLiveData<Int> = MutableLiveData()

//    var shippingAddress = ShippingAddressData().apply {
//        userIdString = app.savepref().get("userId", "")
//        mDefault = false
//    }


    var shippingAddress2 = ShippingAddressData2().apply {
        userIdString = app.savepref().get("userId", "")
        mDefault = false
    }


    var updateProfileData = UpdateProfileData()
        .apply {
            userId = app.savepref().get("userId", "")
        }

    private val createShippingAddressLive= MutableLiveData<Boolean>()

    fun createShippingAddress() {
        createShippingAddressLive.value=true
    }

    val createShippingAddressRequest: LiveData<Resource<Any?>> = createShippingAddressLive.switchMap { data->
        if (data != null) {
            repository.createShippingAddress(shippingAddress2)
        }else{
            AbsentLiveData.create()
        }
    }

    private val deleteShippingAddressLive= MutableLiveData<String>()

    fun deleteShippingAddress(id:String) {
        deleteShippingAddressLive.value=id
    }

    val deleteShippingAddressRequest: LiveData<Resource<Any?>> = deleteShippingAddressLive.switchMap { data->
        if (data != null) {
            repository.deleteShippingAddress(data)
        }else{
            AbsentLiveData.create()
        }
    }


}