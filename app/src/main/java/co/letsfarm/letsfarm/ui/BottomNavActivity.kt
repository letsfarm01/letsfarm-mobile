package co.letsfarm.letsfarm.ui

import android.app.Application
import android.os.Bundle
import co.letsfarm.letsfarm.BaseActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import co.letsfarm.letsfarm.dashBoard.viewModel.DashBoardRespository
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.ui.ui.dashboard.DashboardViewModel
import co.letsfarm.letsfarm.utils.ApiServices
import co.letsfarm.letsfarm.utils.NetModule
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.Component
import dagger.Module
import dagger.Provides
import javax.inject.Inject
import javax.inject.Singleton

class BottomNavActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: DashboardViewModel
    lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_nav)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(R.id.navigation_dashboard, R.id.navigation_farm, R.id.navigation_history)
        )
//        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)


        DaggerDashBoardComponent
            .builder()
            .dashBoardModule(DashBoardModule(application))
            .build()
            .inject(this)

        viewModel.getWalletBalance()
        viewModel.fetchDashboardInfo()
        setObservers()
    }

    private fun setObservers() {

    }

    override fun onResume() {
        super.onResume()
    }

}

@Singleton
@Component(modules = [DashBoardModule::class])
interface DashBoardComponent {
    fun inject(activity: BottomNavActivity)
}

@Module
class DashBoardModule(private val application: Application) : NetModule() {

    @Provides
    fun provideLoginViewmodel(repo: DashBoardRespository) : DashboardViewModel {
        return DashboardViewModel(repo, application)
    }

    @Provides
    fun provideRepository(service: ApiServices) : DashBoardRespository {
        return DashBoardRespository(
            service,
            application
        )
    }
}
