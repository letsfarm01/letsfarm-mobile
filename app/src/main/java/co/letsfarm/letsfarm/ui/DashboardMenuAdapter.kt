package co.letsfarm.letsfarm.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.letsfarm.letsfarm.dashBoard.ui.DashboardMenuItem
import co.letsfarm.letsfarm.R
import kotlinx.android.synthetic.main.holder_dashboard_menu.view.*

class DashboardMenuAdapter(val dashboardMenuItems: MutableList<DashboardMenuItem>, val dashBoardItemListener: (Int)->Unit) : RecyclerView.Adapter<DashboardMenuViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DashboardMenuViewHolder {
        LayoutInflater.from(parent.context).inflate(R.layout.holder_dashboard_menu, parent, false).run {
            return DashboardMenuViewHolder(this)
        }
    }

    override fun getItemCount() = dashboardMenuItems.size

    override fun onBindViewHolder(holder: DashboardMenuViewHolder, position: Int) {
        holder.itemView.run {
            this.setOnClickListener{dashBoardItemListener(position)}
            dashboardMenuItems[position].let {item->
                title.text = item.title
                image.setImageResource(item.image!!)
            }
        }
    }

}

class DashboardMenuViewHolder(itemView:View):RecyclerView.ViewHolder(itemView)
