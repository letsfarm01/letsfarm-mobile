package co.letsfarm.letsfarm.ui.ui

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import co.letsfarm.letsfarm.history.ui.TransactionsFragment
import co.letsfarm.letsfarm.history.ui.WalletTransactionsFragment

private val TAB_TITLES = arrayOf("Transactions", "Wallet History")

class ReportPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when(position){
            0 -> TransactionsFragment()
            1 -> WalletTransactionsFragment()
            else -> TransactionsFragment()
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return TAB_TITLES[position]
    }

    override fun getCount(): Int {
        return 2
    }
}