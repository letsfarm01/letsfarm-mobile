package co.letsfarm.letsfarm.ui.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat
import co.letsfarm.letsfarm.*
import co.letsfarm.letsfarm.dashBoard.model.FundWalletData
import co.letsfarm.letsfarm.allFarms.ui.AllFarmsActivity
import co.letsfarm.letsfarm.allFarms.ui.AmountDialog
import co.letsfarm.letsfarm.allFarms.viewModel.CardTransactions
import co.letsfarm.letsfarm.blog.ui.AllBlogActivity
import co.letsfarm.letsfarm.blog.ui.BlogOrEducation
import co.letsfarm.letsfarm.dashBoard.model.CardPaymentCharges
import co.letsfarm.letsfarm.dashBoard.model.PaymentOptionRes
import co.letsfarm.letsfarm.dashBoard.ui.*
import co.letsfarm.letsfarm.farmUpdates.ui.FarmUpdatesActivity
import co.letsfarm.letsfarm.history.ui.FarmHistoryActivity
import co.letsfarm.letsfarm.paymentProof.ui.PaymentProofActivity
import co.letsfarm.letsfarm.shippingAddress.ui.ProductOrdersActivity
import co.letsfarm.letsfarm.ui.BottomNavActivity
import co.letsfarm.letsfarm.ui.DashboardMenuAdapter
import co.letsfarm.letsfarm.ui.ui.ReportActivity
import co.letsfarm.letsfarm.utils.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_dash_board2.*
import kotlinx.android.synthetic.main.activity_dash_board2.icon_image

class DashboardFragment : Fragment() {

    private lateinit var viewModel: DashboardViewModel
    lateinit var navController: NavController
    var cardPaymentCharges: CardPaymentCharges? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewModel = (activity as BottomNavActivity).viewModel
        return inflater.inflate(R.layout.activity_dash_board2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = (activity as BottomNavActivity).navController

        viewModel.fetchPersonalInfo()
        setObservers()
        setViews()
    }

    private fun setObservers() {
        viewModel.dashboardInfoRequest.observe(viewLifecycleOwner, Observer {
            progress.load(it is Resource.Loading)
            when(it){
                is Resource.Success->{
                    val data = it.data

                    expected_return_tv.text = (data?.expectedReturns?.total ?: 0).getMoney

                    payout_date_tv.text = data?.activeSponsorship?.endDate?.parseDateGood
                }
                is Resource.Error->{
                    activity?.processError(it.message, it.responseCode)
                }
            }
        })
        viewModel.getWalletBalanceRequest.observe(viewLifecycleOwner, Observer {
            when(it){
                is Resource.Success->{
                    val walletBalance = (it.data?.get("wallet_balance") ?: "0").toDouble()
                    wallet_balance_tv.text = walletBalance.getMoney
                    viewModel.walletBalance = walletBalance
                }
                is Resource.Error->{
                    activity?.processError(it.message, it.responseCode)
                }
            }
        })
        viewModel.getPaymentOptionRequest.observe(viewLifecycleOwner, Observer {resource->
            progress.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success->{
                    resource.data?.let {
                        paymentOptions = resource.data
                        inputAmount()
                    }
                    viewModel.getWalletPaymentOption(false)
                }
                is Resource.Error->{
                    activity?.processError(resource.message, resource.responseCode)
                    viewModel.getWalletPaymentOption(false)
                }
            }
        })
        viewModel.checkoutSaveCardRequest.observe(viewLifecycleOwner, Observer {resource->
            progress.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success->{
                    viewModel.getWalletBalance()
                    activity?.processError(resource.message, resource.responseCode)
                    viewModel.checkoutSaveCard(null)
                }
                is Resource.Error->{
                    viewModel.getWalletBalance()
                    activity?.processError(resource.message, resource.responseCode)
                    viewModel.checkoutSaveCard(null)
                }
            }
        })
        viewModel.checkoutRequest.observe(viewLifecycleOwner, Observer {resource->
            progress.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success->{
                    resource.data?.details?.let {details->
                        when(checkOutOption!!.name){
                            "Paystack"->{
                                activity?.navigateTo<CheckOutNewCardActivity> {
                                    details.run {
                                        putExtra("cardTransactions", CardTransactions.WALLET_FUNDING.toString())
                                        putExtra("ref", ref)
                                        putExtra("accessCode", accessCode)
                                        putExtra("transactionId", transactionId)
                                        putExtra("totalPrice", total)
                                    }
                                }
                            }
                            else -> {}
                        }
                    }?:run{
                        activity?.processError(resource.message, resource.responseCode)
                        resource.data?.let {
                            activity?.navigateTo<PaymentProofActivity> {
                                putExtra("amount", it.amount)
                                putExtra("transactionId", it.transactionId)
                                putExtra("transactionRef", it.transactionRef)
                            }
                        }
                    }
                }
                is Resource.Error->{
                    activity?.processError(resource.message, resource.responseCode)
                }
            }
        })
        viewModel.fetchPersonalInfoRequest.observe(viewLifecycleOwner, Observer {resource->
            progress.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success->{
                    resource.data?.let {
                        it.userImageUrl?.let { image ->
                            Picasso.get().load(image).into(icon_image)
                        }
                    }
                }
                is Resource.Error->{
//                    processError(resource.message, resource.responseCode)
                }
            }
        })

        viewModel.getCardPaymentChargesRequest.observe(viewLifecycleOwner, Observer {resource->
            progress.load(resource is Resource.Loading)
            when(resource){
                is Resource.Success->{
                    cardPaymentCharges = resource.data

                    viewModel.getCardPaymentCharges(null)

                    PaymentOptionsFragment(0.0, paymentOptions!!, cardPaymentCharges!!){paymentOption->
                        paymentOptionsselection(paymentOption, cardPaymentCharges!!.subTotal!!)
                    }.show(childFragmentManager, null)
                }
                is Resource.Error->{
                    viewModel.getCardPaymentCharges(null)
                    activity?.processError(resource.message, resource.responseCode)
                }
            }
        })
    }


    private fun setViews() {
        icon_image.setImageDrawable(VectorDrawableCompat.create(resources, R.drawable.ic_profile2, activity?.theme))

        welcome_tv.setOnClickListener {
            openSettings()
        }
        icon_image.setOnClickListener {
            openSettings()
        }

        welcome_tv.text = "Welcome ${activity?.savepref()?.get("userName", "")}"

//        drawable = R.drawable.ic_profile2.getBitmapFromVectorDrawable(requireActivity())

        fund_wallet_tv.setOnClickListener { inputAmount() }
        wallet_balance_title_tv.setOnClickListener { inputAmount() }
        wallet_balance_tv.setOnClickListener { inputAmount() }

        icon_image.setImageDrawable(VectorDrawableCompat.create(resources, R.drawable.ic_profile2, requireActivity().theme))

        val dashboardMenuItems:MutableList<DashboardMenuItem> = mutableListOf()

        dashboardMenuItems.add(
            DashboardMenuItem(
                "Farm History",
                R.drawable.ic_history
            )
        )
        dashboardMenuItems.add(
            DashboardMenuItem(
                "Sponsor farms",
                R.drawable.ic_followed_projects
            )
        )
        dashboardMenuItems.add(
            DashboardMenuItem(
                "Farm updates",
                R.drawable.ic_project_updates
            )
        )
        dashboardMenuItems.add(
            DashboardMenuItem(
                "Reports",
                R.drawable.reports
            )
        )
        dashboardMenuItems.add(
            DashboardMenuItem(
                "Bank transfers",
                R.drawable.bank_transfer
            )
        )
        dashboardMenuItems.add(
            DashboardMenuItem(
                "Farm products",
                R.drawable.farm_products
            )
        )
        dashboardMenuItems.add(
            DashboardMenuItem(
                "Products orders",
                R.drawable.ic_order
            )
        )
        dashboardMenuItems.add(
            DashboardMenuItem(
                "Account Manager",
                R.drawable.ic_account_manager
            )
        )
        dashboardMenuItems.add(
            DashboardMenuItem(
                "Referral",
                R.drawable.more
            )
        )
        dashboardMenuItems.add(
            DashboardMenuItem(
                "FAQ",
                R.drawable.ic_faq
            )
        )
        dashboardMenuItems.add(
            DashboardMenuItem(
                "Blog",
                R.drawable.ic_blog
            )
        )
        dashboardMenuItems.add(
            DashboardMenuItem(
                "Education",
                R.drawable.ic_education
            )
        )
//        dashboardMenuItems.add(
//            DashboardMenuItem(
//                "Farm updates",
//                R.drawable.ic_updates
//            )
//        )

        dashboard_menu_rv.layoutManager = GridLayoutManager(activity, 3)
        dashboard_menu_rv.adapter = DashboardMenuAdapter(dashboardMenuItems, ::dashBoardItemListener)
    }

    private fun openSettings() {
        navController.navigate(R.id.navigation_farm)
    }

    var paymentOptions:List<PaymentOptionRes?>?=null
    var checkOutOption:PaymentOptionRes?=null

    fun inputAmount(){
        paymentOptions?.let {
            AmountDialog(::selectPaymentOption).show(childFragmentManager, null)
        }?:run {
            viewModel.getWalletPaymentOption()
        }
    }

    private fun selectPaymentOption(mAmount: String) {
        viewModel.getCardPaymentCharges(mAmount.toInt())
    }

    fun paymentOptionsselection(option:PaymentOptionRes, mAmount: Int){
        checkOutOption = option
        if (option.name == "Paystack"){
            CardSelectionDialog(CardSelectionOrigin.DASHBOARD){
                    processPayment(mAmount, it)
            }.show(childFragmentManager, null)
        }else{
            processPayment(mAmount)
        }
    }

//    var amount:Int = 0

    private fun processPayment(mAmount: Int, mCardId:String? = null) {
//        amount = mAmount
//        activity?.showToast("$amount")
        val fundWalletData = FundWalletData()
            .apply{
            userId = activity?.savepref()?.get("userId", "")
            cardId = mCardId
            amount = "$mAmount"
            paymentGateway = checkOutOption?.id
        }
        if (mCardId == null)
            viewModel.checkout(fundWalletData)
        else
            viewModel.checkoutSaveCard(fundWalletData)

    }

    fun dashBoardItemListener(position:Int){
        when(position){
            0-> activity?.navigateTo<FarmHistoryActivity>()
            1-> activity?.navigateTo<AllFarmsActivity>()
            2-> activity?.navigateTo<FarmUpdatesActivity>()
            3-> activity?.navigateTo<ReportActivity>()
            4-> activity?.navigateTo<PendingTransactionsActivity>()
            5-> activity?.navigateTo<ProductActivity>()
            6-> activity?.navigateTo<ProductOrdersActivity>()
            7-> activity?.navigateTo<AccountManagerActivity>()
            8-> activity?.navigateTo<ReferralActivity>()
            9-> activity?.navigateTo<FaqActivity>()
            10-> activity?.navigateTo<AllBlogActivity>{
                log(BlogOrEducation.BLOG.toString())
                log(BlogOrEducation.BLOG)
                putExtra("blogOrEducation", BlogOrEducation.BLOG.toString())
            }
            11-> activity?.navigateTo<AllBlogActivity>{
                putExtra("blogOrEducation", BlogOrEducation.EDUCATION.toString())
            }
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.getWalletBalance()
    }
}
