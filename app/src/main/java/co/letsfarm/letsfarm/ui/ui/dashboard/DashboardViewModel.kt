package co.letsfarm.letsfarm.ui.ui.dashboard

import android.app.Application
import androidx.lifecycle.*
import co.letsfarm.letsfarm.dashBoard.viewModel.DashBoardRespository
import co.letsfarm.letsfarm.history.model.Transaction
import co.letsfarm.letsfarm.dashBoard.model.FundWalletData
import co.letsfarm.letsfarm.common.utils.AbsentLiveData
import co.letsfarm.letsfarm.dashBoard.model.*
import co.letsfarm.letsfarm.utils.BaseViewModel
import co.letsfarm.letsfarm.utils.Resource
import co.letsfarm.letsfarm.history.model.ActiveSponsorship
import co.letsfarm.letsfarm.history.model.RunningFarm
import co.letsfarm.letsfarm.history.model.WalletTransaction
import co.letsfarm.letsfarm.profile.model.UpdateProfileData
import kotlinx.coroutines.launch
import java.util.HashMap

class DashboardViewModel(val repository: DashBoardRespository, val app: Application): BaseViewModel(repository, app){

    var walletBalance: Double? = null

    private val dashboardInfoRequestLive= MutableLiveData<Boolean>()

    fun fetchDashboardInfo(){
        dashboardInfoRequestLive.value=true
    }

    val dashboardInfoRequest: LiveData<Resource<DashBoardRes>> = dashboardInfoRequestLive.switchMap { loginData->
        if (loginData != null) {
            repository.fetchDashboardInfo()
        }else{
            AbsentLiveData.create()
        }
    }


    private val getWalletBalanceLive= MutableLiveData<Boolean>()

    fun getWalletBalance(){
        getWalletBalanceLive.value=true
    }

    val getWalletBalanceRequest: LiveData<Resource<Map<String, String>>> = getWalletBalanceLive.switchMap { loginData->
        if (loginData != null) {
            repository.getWalletBalance()
        }else{
            AbsentLiveData.create()
        }
    }

    private val checkoutSaveCardLive= MutableLiveData<FundWalletData>()

    fun checkoutSaveCard(id: FundWalletData?) {
        checkoutSaveCardLive.value=id
    }

    val checkoutSaveCardRequest: LiveData<Resource<Any>> = checkoutSaveCardLive.switchMap { data->
        if (data != null) {
            repository.fundWalletCard(data)
        }else{
            AbsentLiveData.create()
        }
    }

    private val checkoutLive= MutableLiveData<FundWalletData>()

    fun checkout(id: FundWalletData) {
        checkoutLive.value=id
    }

    val checkoutRequest: LiveData<Resource<CheckOutRes>> = checkoutLive.switchMap { data->
        if (data != null) {
            repository.checkout(data)
        }else{
            AbsentLiveData.create()
        }
    }

    private val fetchWalletTransactionsLive= MutableLiveData<Boolean>()

    fun fetchWalletTransactions() {
        fetchWalletTransactionsLive.value=true
    }

    val fetchWalletTransactionsRequest: LiveData<Resource<List<WalletTransaction>>> = fetchWalletTransactionsLive.switchMap { data->
        if (data != null) {
            repository.fetchWalletTransactions()
        }else{
            AbsentLiveData.create()
        }
    }
    private val getTransactionsLive= MutableLiveData<Boolean>()

    fun getTransactions() {
        getTransactionsLive.value=true
    }

    val getTransactionsRequest: LiveData<Resource<List<Transaction>>> = getTransactionsLive.switchMap { data->
        if (data != null) {
            repository.getTransactions()
        }else{
            AbsentLiveData.create()
        }
    }

    private val getActiveSponsorshipLive= MutableLiveData<Boolean>()

    fun getActiveSponsorship() {
        getActiveSponsorshipLive.value=true
    }

    val getActiveSponsorshipRequest: LiveData<Resource<List<ActiveSponsorship>>> = getActiveSponsorshipLive.switchMap { data->
        if (data != null) {
            repository.getActiveSponsorship()
        }else{
            AbsentLiveData.create()
        }
    }
    private val getRunningFarmsLive= MutableLiveData<Boolean>()

    fun getRunningFarms() {
        getRunningFarmsLive.value=true
    }

    val getRunningFarmsRequest: LiveData<Resource<List<RunningFarm>>> = getRunningFarmsLive.switchMap { data->
        if (data != null) {
            repository.getRunningFarms()
        }else{
            AbsentLiveData.create()
        }
    }

    private val getFarmHistoryLive= MutableLiveData<Boolean>()

    fun getFarmHistory() {
        getFarmHistoryLive.value=true
    }

    val getFarmHistoryRequest: LiveData<Resource<List<ActiveSponsorship>>> = getActiveSponsorshipLive.switchMap { data->
        if (data != null) {
            repository.getFarmHistory()
        }else{
            AbsentLiveData.create()
        }
    }


    var getPaymentOptionTrigger = MutableLiveData<Boolean>()

    fun getWalletPaymentOption(value:Boolean = true){
        getPaymentOptionTrigger.value=value
    }
    var getPaymentOptionRequest: LiveData<Resource<List<PaymentOptionRes?>>> = getPaymentOptionTrigger.switchMap { allFarmsData->
        if (allFarmsData) {
            repository.getWalletPaymentOption()
        }else{
            AbsentLiveData.create()
        }
    }

    private val fetchPersonalInfoLive= MutableLiveData<Boolean>()

    fun fetchPersonalInfo(){
        fetchPersonalInfoLive.value=true
    }

    val fetchPersonalInfoRequest: LiveData<Resource<UpdateProfileData>> = fetchPersonalInfoLive.switchMap { loginData->
        if (loginData != null) {
            viewModelScope.launch{
            }
            repository.fetchPersonalInfo()
        }else{
            AbsentLiveData.create()
        }
    }

    private val getCardPaymentChargesLive= MutableLiveData<Int>()

    fun getCardPaymentCharges(totalPrice: Int?) {
        getCardPaymentChargesLive.value=totalPrice
    }

    val getCardPaymentChargesRequest: LiveData<Resource<CardPaymentCharges>> = getCardPaymentChargesLive.switchMap { data->
        if (data != null) {
            repository.getCardPaymentCharges(data)
        }else{
            AbsentLiveData.create()
        }
    }

    private val submitMessageLive= MutableLiveData<HashMap<String, String>>()

    fun submitMessageInfo(request: HashMap<String, String>){
        submitMessageLive.value=request
    }

    val submitMessageRequest: LiveData<Resource<Any>> = submitMessageLive.switchMap { data->
        if (data != null) {
            repository.submitMessage(data)
        }else{
            AbsentLiveData.create()
        }
    }

    private val getReferralLive= MutableLiveData<Boolean>()

    fun getReferral(){
        getReferralLive.value=true
    }

    val getReferralRequest: LiveData<Resource<List<Referral>>> = getReferralLive.switchMap { data->
        if (data != null) {
            repository.getReferral()
        }else{
            AbsentLiveData.create()
        }
    }

    var fetchBankInfoLive= MutableLiveData<Boolean>()

    fun fetchBankInfo(){
        fetchBankInfoLive.value=true
    }

    val fetchBankInfoRequest: LiveData<Resource<UpdateProfileData>> = fetchBankInfoLive.switchMap { data->
        if (data != null) {
            repository.fetchBankInfo()
        }else{
            AbsentLiveData.create()
        }
    }

    var withdrawLive= MutableLiveData<WithdrawData>()

    fun withdraw(withdrawData: WithdrawData){
        withdrawLive.value=withdrawData
    }

    var withdrawRequest: LiveData<Resource<Any>> = withdrawLive.switchMap { data->
        if (data != null) {
            repository.withdraw(data)
        }else{
            AbsentLiveData.create()
        }
    }

}
