package co.letsfarm.letsfarm.ui.ui.dashboard

import android.app.Application
import co.letsfarm.letsfarm.BaseActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.allFarms.viewModel.AllFarmsRespository
import co.letsfarm.letsfarm.allFarms.viewModel.AllFarmsViewModel
import co.letsfarm.letsfarm.paymentProof.ui.PaymentProofActivity
import co.letsfarm.letsfarm.ui.BottomNavActivity
import co.letsfarm.letsfarm.utils.*
import dagger.Component
import dagger.Module
import dagger.Provides
import kotlinx.android.synthetic.main.activity_pending_transactions.*
import javax.inject.Inject
import javax.inject.Singleton

class PendingTransactionsActivity : BaseActivity() {

    @Inject
    lateinit var viewModel:AllFarmsViewModel

    val adapter = PendingTransactionsAdapter{transferData->
        if (transferData.paymentProof.isNullOrBlank()) {
            navigateTo<PaymentProofActivity> {
                putExtra("amount", transferData.totalPrice)
                putExtra("transactionId", transferData.id)
                putExtra("transactionRef", transferData.orderReference)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pending_transactions)

        DaggerPendingTransactionsComponent
            .builder()
            .pendingTransactionsModule(PendingTransactionsModule(application))
            .build()
            .inject(this)

        viewModel.fetchPendingTransactions()
        setViews()
        setObservers()

    }

    override fun onResume() {
        super.onResume()
        viewModel.fetchPendingTransactions()
    }

    private fun setViews() {
        back_img.setOnClickListener { onBackPressed() }
        pending_transfers_rv.adapter = adapter
    }

    override fun onBackPressed() {
        navigateToNew<BottomNavActivity>()
    }

    private fun setObservers() {
        viewModel.fetchPendingTransactionsRequest.observe(this, Observer {resource->
            progress.load(resource is Resource.Loading)

            when(resource){
                is Resource.Success->{
                    adapter.setData(resource.data)
                }
                is Resource.Error->{
                    processError(resource.message, resource.responseCode)
                }
            }

        })
    }
}

@Singleton
@Component(modules = [PendingTransactionsModule::class])
interface PendingTransactionsComponent {
    fun inject(activity: PendingTransactionsActivity)
}

@Module
class PendingTransactionsModule(private val application: Application) : NetModule() {

    @Provides
    fun provideAllFarmsViewmodel(repo: AllFarmsRespository) : AllFarmsViewModel {
        return AllFarmsViewModel(repo, application)
    }

    @Provides
    fun provideRepository(service: ApiServices) : AllFarmsRespository {
        return AllFarmsRespository(service, application)
    }
}