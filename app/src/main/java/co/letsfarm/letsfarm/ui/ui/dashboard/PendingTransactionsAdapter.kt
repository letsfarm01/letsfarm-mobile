package co.letsfarm.letsfarm.ui.ui.dashboard

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.dashBoard.model.PendingTransfersRes
import co.letsfarm.letsfarm.utils.getMoney
import kotlinx.android.synthetic.main.holder_pending_transfers.view.*

class PendingTransactionsAdapter(val clickListener:(PendingTransfersRes)->Unit) : RecyclerView.Adapter<PendingTransactionsViewHolder>() {

    var pendingTransfers: List<PendingTransfersRes>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PendingTransactionsViewHolder {
        LayoutInflater.from(parent.context).inflate(R.layout.holder_pending_transfers, parent, false).run {
            return PendingTransactionsViewHolder(this)
        }
    }

    override fun getItemCount() = pendingTransfers?.size ?: 0

    override fun onBindViewHolder(holder: PendingTransactionsViewHolder, position: Int) {
        holder.itemView.run {
            pendingTransfers?.get(position)?.let {transfer->
                setOnClickListener {
                    clickListener(transfer)
                }
                amount_tv.text = transfer.totalPrice?.getMoney
                descr_tv.text = transfer.narration
                reference_tv.text = transfer.orderReference
                upload_tv.text = if (transfer.paymentProof.isNullOrBlank()) "Upload proof of payment" else "Awaiting confirmation"
            }
        }
    }

    fun setData(data: List<PendingTransfersRes>?) {
        pendingTransfers = data
        notifyDataSetChanged()
    }

}

class PendingTransactionsViewHolder(itemView:View):RecyclerView.ViewHolder(itemView)