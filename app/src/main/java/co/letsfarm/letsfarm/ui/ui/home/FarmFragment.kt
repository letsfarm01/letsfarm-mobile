package co.letsfarm.letsfarm.ui.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import co.letsfarm.letsfarm.history.ui.FarmHistoryAdapter
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.ui.BottomNavActivity
import co.letsfarm.letsfarm.ui.ui.dashboard.DashboardViewModel
import co.letsfarm.letsfarm.utils.Resource
import co.letsfarm.letsfarm.utils.load
import co.letsfarm.letsfarm.utils.processError

import kotlinx.android.synthetic.main.fragment_farm.*

class FarmFragment : Fragment() {

    private lateinit var viewModel: DashboardViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        viewModel = (activity as BottomNavActivity).viewModel

        val root = inflater.inflate(R.layout.fragment_farm, container, false)

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getActiveSponsorship()
        setViews()
        setObservers()
    }

    private fun setObservers() {
        viewModel.getActiveSponsorshipRequest.observe(viewLifecycleOwner, Observer {
            progress.load(it is Resource.Loading)
            when(it){
                is Resource.Success->{
                    it.data?.let {
                        adapter.setData(it)
                    }
                }
                is Resource.Error->{
                    activity?.processError(it.message, it.responseCode)
                }
            }
        })

    }

    var adapter = FarmHistoryAdapter()

    private fun setViews() {
        history_rv.adapter = adapter

    }

}
