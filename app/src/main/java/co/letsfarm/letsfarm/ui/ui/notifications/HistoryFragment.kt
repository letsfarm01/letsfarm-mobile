package co.letsfarm.letsfarm.ui.ui.notifications

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.ui.BottomNavActivity
import co.letsfarm.letsfarm.ui.ui.dashboard.DashboardViewModel
import co.letsfarm.letsfarm.utils.Resource
import co.letsfarm.letsfarm.utils.load
import co.letsfarm.letsfarm.utils.processError

import kotlinx.android.synthetic.main.fragment_history.*
import kotlinx.android.synthetic.main.fragment_history.progress

class HistoryFragment : Fragment() {

    private lateinit var viewModel: DashboardViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        viewModel = (activity as BottomNavActivity).viewModel

        val root = inflater.inflate(R.layout.fragment_history, container, false)

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.fetchWalletTransactions()
        setViews()
        setObservers()
    }

    private fun setObservers() {
        viewModel.fetchWalletTransactionsRequest.observe(viewLifecycleOwner, Observer {
            progress.load(it is Resource.Loading)
            when(it){
                is Resource.Success->{
                    it.data?.let {
                        adapter.setData(it)
                    }
                }
                is Resource.Error->{
                    activity?.processError(it.message, it.responseCode)
                }
            }
        })

    }

    var adapter = WalletTransactionsAdapter()

    private fun setViews() {
        history_rv.adapter = adapter

    }
}
