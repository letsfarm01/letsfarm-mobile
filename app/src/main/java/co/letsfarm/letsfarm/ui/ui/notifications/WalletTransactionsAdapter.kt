package co.letsfarm.letsfarm.ui.ui.notifications

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.history.model.WalletTransaction
import co.letsfarm.letsfarm.utils.getMoney
import co.letsfarm.letsfarm.utils.parseDateGood
import kotlinx.android.synthetic.main.holder_transaction_history.view.*

class WalletTransactionsAdapter : RecyclerView.Adapter<HistoryHolder>() {

    var walletTransactions: List<WalletTransaction>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryHolder {
        LayoutInflater.from(parent.context).inflate(R.layout.holder_wallet_history, parent, false).run {
            return HistoryHolder(this)
        }
    }

    override fun getItemCount() = walletTransactions?.size ?: 0

    override fun onBindViewHolder(holder: HistoryHolder, position: Int) {

        holder.itemView.run {
            walletTransactions?.get(position)?.let {
                narration_tv.text = it.transactionId?.narration
                amount_tv.text = it.amount?.getMoney
                date_tv.text = it.createdAt?.parseDateGood
                type_tv.text = it.type
            }
        }

    }

    fun setData(walletTransactions: List<WalletTransaction>) {
        this.walletTransactions = walletTransactions
        notifyDataSetChanged()
    }

}

class HistoryHolder(itemView:View):RecyclerView.ViewHolder(itemView)
