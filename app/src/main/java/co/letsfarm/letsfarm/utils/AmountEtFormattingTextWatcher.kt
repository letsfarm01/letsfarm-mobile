package co.letsfarm.letsfarm.utils

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText

class AmountEtFormattingTextWatcher(val amount_et: EditText) : TextWatcher {
    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
    }

    override fun afterTextChanged(s: Editable) {
        val mValue = this.amount_et.removeComma
        if (mValue.isNotEmpty()) {
            val amv = mValue.replace(",", "").replace(" ", "")
            if (amv.isNotEmpty()) {
                var value = 0.0
                try {
                    value = java.lang.Double.parseDouble(amv)
                } catch (ex: NumberFormatException) {
                    this.amount_et.error = "Invalid amount"
                }
                if (value > 0) {
                    amount_et.removeTextChangedListener(this)
                    this.amount_et.setText(value.getMoneyWithoutSign)

                    val length = this.amount_et.text?.length
                    if (length != null) {
                        this.amount_et.setSelection(length)
                    }
                    this.amount_et.addTextChangedListener(this)


                }
            }
        }
    }
}
