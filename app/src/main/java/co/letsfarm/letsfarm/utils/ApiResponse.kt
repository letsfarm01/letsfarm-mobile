package co.letsfarm.letsfarm.utils

import co.letsfarm.letsfarm.utils.Constants.Companion.JSON_EXCEPTION
import co.letsfarm.letsfarm.utils.Constants.Companion.UNKNOWM_ERROR_RESPONSE
import org.json.JSONObject
import retrofit2.Response


/** Generic Api Handler Class**/

@Suppress("unused") // T is used in extending classes
sealed class ApiResponse<T> {

    companion object {
        private const val TAG: String = "AppDebug"

        fun <T> create(error: Throwable): ApiErrorResponse<T> {
//            return ApiErrorResponse(error.message ?: "unknown error", responseCode = UNKNOWM_ERROR_RESPONSE)
            return ApiErrorResponse(errorMessage = "An error occurred! Please try again!", status = "", responseCode = UNKNOWM_ERROR_RESPONSE)
        }

        fun <T> create(response: Response<T>): ApiResponse<T> {

            if(response.isSuccessful){
                val body = response.body()
                return if (body == null || response.code() == 204) {
                    ApiEmptyResponse()
                } else {
                    ApiSuccessResponse(body = body, responseCode = response.code())
                }
            }
//            else if(response.code() == 401){
//                return ApiErrorResponse("Unauthorized", responseCode = response.code())
//            }
            else{
                try {
//                val msg = response.errorBody()?.string()
                    val jObjError = JSONObject(response.errorBody()!!.string())
//                    Log.d("debug", "====${response.errorBody().toString()}")
                    return ApiErrorResponse(errorMessage = jObjError.getString("msg") ?: "An error Occurred", status = jObjError.getString("status") ?: "",responseCode = response.code())
                }catch (ex: Exception){
                    return ApiErrorResponse(errorMessage = "An error Occurred", status = "",responseCode = JSON_EXCEPTION)
                }
            }
        }

    }
}

/**
 * separate class for HTTP 204 responses so that we can make ApiSuccessResponse's body non-null.
 */
class ApiEmptyResponse<T> : ApiResponse<T>()

data class ApiSuccessResponse<T>(val body: T, val responseCode: Int) : ApiResponse<T>()

data class ApiErrorResponse<T>(val errorMessage: String, val status: String, val responseCode: Int) : ApiResponse<T>()



















