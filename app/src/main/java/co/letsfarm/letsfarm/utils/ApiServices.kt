package co.letsfarm.letsfarm.utils

import androidx.lifecycle.LiveData
import co.letsfarm.letsfarm.auth.model.AppVersion
import co.letsfarm.letsfarm.dashBoard.model.GeneralRes
import co.letsfarm.letsfarm.history.model.Transaction
import co.letsfarm.letsfarm.allFarms.model.*
import co.letsfarm.letsfarm.auth.model.*
import co.letsfarm.letsfarm.blog.model.AllBlog
import co.letsfarm.letsfarm.blog.model.BlogData
import co.letsfarm.letsfarm.dashBoard.model.*
import co.letsfarm.letsfarm.farmUpdates.model.FarmUpdates
import retrofit2.http.*
import java.util.*
import co.letsfarm.letsfarm.history.model.ActiveSponsorship
import co.letsfarm.letsfarm.history.model.RunningFarm
import co.letsfarm.letsfarm.history.model.WalletTransaction
import co.letsfarm.letsfarm.product.model.AddProductCartData
import co.letsfarm.letsfarm.product.model.AllProducts
import co.letsfarm.letsfarm.product.model.GetProductCartData
import co.letsfarm.letsfarm.profile.model.Bank
import co.letsfarm.letsfarm.profile.model.UpdateProfileData
import co.letsfarm.letsfarm.shippingAddress.ShippingAddressData2
import co.letsfarm.letsfarm.shippingAddress.model.ProductCheckoutData
import co.letsfarm.letsfarm.shippingAddress.model.ProductOrder
import co.letsfarm.letsfarm.shippingAddress.model.ShippingAddressData

interface ApiServices {

    @POST("auth/login")
    fun login(@Body loginRequest: LoginData):LiveData<ApiResponse<GeneralRes<LoginResData>>>

    @POST("auth/register")
    fun signUp(@Body signUpData: SignUpData):LiveData<ApiResponse<GeneralRes<SignUpResData>>>

    @POST("auth/reset-password")
    fun forgotPassword(@Body data: HashMap<String, String>):LiveData<ApiResponse<GeneralRes<Any>>>

    @POST("auth/forgot-password")
    fun resetPassword(@Body resetPasswordData: ResetPasswordData):LiveData<ApiResponse<GeneralRes<Any>>>

    @POST("auth/verify-signup")
    fun verifyUser(@Body request: HashMap<String, String>):LiveData<ApiResponse<GeneralRes<Any>>>

    @POST("auth/resend-verify-link")
    fun resendActivationLink(@Body request: HashMap<String, String>):LiveData<ApiResponse<GeneralRes<Any>>>

    @GET("dashboard")
    fun fetchDashboardInfo(@Header("Authorization")token: String):LiveData<ApiResponse<GeneralRes<DashBoardRes>>>

    @GET("farm-shop")
    fun fetchAllFarms(@Header("Authorization")token: String):LiveData<ApiResponse<GeneralRes<AllFarmsResData>>>

    @GET("farm-status/active-farm-status")
    fun fetchFarmStatus(@Header("Authorization")token: String):LiveData<ApiResponse<GeneralRes<List<FarmStatusData?>?>>>

    @POST("cart")
    fun addToCart(@Header("Authorization")token: String, @Body data: AddCartData): LiveData<ApiResponse<GeneralRes<AddToCartRes?>>>

    @DELETE("cart/{id}")
    fun deleteCartItem(@Header("Authorization")token: String, @Path("id") id:String): LiveData<ApiResponse<GeneralRes<Any>>>

    @GET("cart/user-count/{userId}")
    fun getCartCount(@Header("Authorization")token: String, @Path("userId") userId:String): LiveData<ApiResponse<GeneralRes<String?>>>

    @GET("cart/user-cart/{userId}")
    fun getCart(@Header("Authorization")token: String, @Path("userId") userId:String): LiveData<ApiResponse<GeneralRes<List<GetCartData?>>>>

    @GET("payment-channel")
    fun getPaymentOption(@Header("Authorization")token: String, @Query("can_fund_wallet") canFundWallet:Boolean = false): LiveData<ApiResponse<GeneralRes<List<PaymentOptionRes?>>>>

    @FormUrlEncoded
    @POST("checkout-cart/farm-shop")
    fun checkout(@Header("Authorization")token: String, @Field("payment_gateway") data: String): LiveData<ApiResponse<GeneralRes<CheckOutRes>>>

    @POST("checkout-cart/charge-existing-card")
    fun checkoutSavedCard(@Header("Authorization")token: String, @Body data: CheckOutSavedCard): LiveData<ApiResponse<GeneralRes<CheckOutRes>>>

    @FormUrlEncoded
    @POST("checkout-cart/paystack-verify")
    fun verifyTransaction(@Header("Authorization")token: String, @Field("transactionId") data: String): LiveData<ApiResponse<GeneralRes<Any>>>

    @FormUrlEncoded
    @POST("fund-wallet/paystack-verify-payment")
    fun verifyWalletTransaction(@Header("Authorization")token: String, @Field("transactionId") data: String): LiveData<ApiResponse<GeneralRes<Any>>>

    @POST("auth/change-password")
    fun changePassword(@Header("Authorization")token: String, @Body data: UpdateProfileData): LiveData<ApiResponse<GeneralRes<Any>>>

    @PUT("users/{userId}")
    fun updatePersonalInfo(@Header("Authorization")token: String, @Path("userId") userId:String, @Body data: UpdateProfileData): LiveData<ApiResponse<GeneralRes<Any>>>

    @PUT("profile/business-info/{userId}")
    fun updateBusinessInfo(@Header("Authorization")token: String, @Path("userId") userId:String, @Body data: UpdateProfileData): LiveData<ApiResponse<GeneralRes<Any>>>

    @PUT("profile/bank-setting/{userId}")
    fun updateBankInfo(@Header("Authorization")token: String, @Path("userId") userId:String, @Body data: UpdateProfileData): LiveData<ApiResponse<GeneralRes<Any>>>

    @PUT("profile/next-of-kin/{userId}")
    fun updateNextKinInfo(@Header("Authorization")token: String, @Path("userId") userId:String, @Body data: UpdateProfileData): LiveData<ApiResponse<GeneralRes<Any>>>

    @GET("users/{userId}")
    fun fetchPersonalInfo(@Header("Authorization")token: String, @Path("userId") userId:String): LiveData<ApiResponse<GeneralRes<UpdateProfileData>>>

    @GET("profile/business-info/{userId}")
    fun fetchBusinessInfo(@Header("Authorization")token: String, @Path("userId") userId:String): LiveData<ApiResponse<GeneralRes<UpdateProfileData>>>

    @GET("profile/bank-setting/{userId}")
    fun fetchBankInfo(@Header("Authorization")token: String, @Path("userId") userId:String): LiveData<ApiResponse<GeneralRes<UpdateProfileData>>>

    @GET("profile/next-of-kin/{userId}")
    fun fetchNextKinInfo(@Header("Authorization")token: String, @Path("userId") userId:String): LiveData<ApiResponse<GeneralRes<UpdateProfileData>>>


    @GET("bank")
    fun fetchBanks(): LiveData<ApiResponse<GeneralRes<List<Bank>>>>


    @GET("transaction/awaiting-payment-proof/{id}")
    fun fetchPendingTransfers(@Header("Authorization")token: String, @Path("id") userId:String): LiveData<ApiResponse<GeneralRes<List<PendingTransfersRes>>>>

    @POST("bank/payment/bank-transfer")
    fun uploadProof(@Header("Authorization")token: String, @Body data:UploadProofData): LiveData<ApiResponse<GeneralRes<Any>>>

    @GET("wallet/user/{userId}")
    fun getWalletBalance(@Header("Authorization")token: String, @Path("userId") userId:String): LiveData<ApiResponse<GeneralRes<Map<String, String>>>>

    @POST("fund-wallet")
    fun fundWallet(@Header("Authorization")token: String, @Body data: FundWalletData): LiveData<ApiResponse<GeneralRes<CheckOutRes>>>

    @POST("fund-wallet/charge-existing-card")
    fun fundWalletCard(@Header("Authorization")token: String, @Body data: FundWalletData): LiveData<ApiResponse<GeneralRes<Any>>>

    @GET("wallet-transaction/user/{userId}")
    fun getUserWalletTransaction(@Header("Authorization")token: String, @Path("userId") userId:String): LiveData<ApiResponse<GeneralRes<List<WalletTransaction>>>>

    @GET("fund-wallet/saved-cards/{userId}")
    fun getSavedCard(@Header("Authorization")token: String, @Path("userId") userId:String): LiveData<ApiResponse<GeneralRes<List<SavedCard>>>>
    
    @DELETE("fund-wallet/saved-cards/{id}")
    fun deleteSavedCard(@Header("Authorization")token: String, @Path("id") cardId:String): LiveData<ApiResponse<GeneralRes<List<SavedCard>>>>

    @GET("transaction/user/{userId}")
    fun getTransactions(@Header("Authorization")token: String, @Path("userId") cardId:String): LiveData<ApiResponse<GeneralRes<List<Transaction>>>>

    @GET("sponsorship/active/{userId}")
    fun getActiveSponsorship(@Header("Authorization")token: String, @Path("userId") cardId:String): LiveData<ApiResponse<GeneralRes<List<ActiveSponsorship>>>>

    @POST("archive-sponsor/user/email")
    fun getRunningFarms(@Header("Authorization")token: String, @Body data: HashMap<String, String>): LiveData<ApiResponse<GeneralRes<List<RunningFarm>>>>

    @GET("sponsorship/history/{userId}")
    fun getFarmHistory(@Header("Authorization")token: String, @Path("userId") cardId:String): LiveData<ApiResponse<GeneralRes<List<ActiveSponsorship>>>>

    @POST("contact-account-manager")
    fun submitMessage(@Header("Authorization")token: String, @Body request: HashMap<String, String>): LiveData<ApiResponse<GeneralRes<Any>>>

    @GET("referral/user/{userId}")
    fun getReferral(@Header("Authorization")token: String, @Path("userId") userId:String): LiveData<ApiResponse<GeneralRes<List<Referral>>>>

    @FormUrlEncoded
    @POST("checkout-cart/calculate-charge")
    fun getCardPaymentCharges(@Header("Authorization")token: String, @Field("amount") data: Int): LiveData<ApiResponse<GeneralRes<CardPaymentCharges>>>

    @GET("dashboard/app-version")
    fun appVersion(): LiveData<ApiResponse<GeneralRes<AppVersion>>>

    @GET
    fun fetchAllBlog(@Url path:String): LiveData<ApiResponse<GeneralRes<AllBlog>>>

    @GET("blog/top-post")
    fun fetchTopBlog(): LiveData<ApiResponse<GeneralRes<BlogData>>>

    @GET("farm-product")
    fun getProducts(@Header("Authorization")token: String, @Query("page") page:Int, @Query("limit") limit:String = "100")
            : LiveData<ApiResponse<GeneralRes<AllProducts>>>


    @POST("product-checkout/farm-product")
    fun checkoutProduct(@Header("Authorization")token: String, @Body data: ProductCheckoutData): LiveData<ApiResponse<GeneralRes<CheckOutRes>>>

    @POST("product-checkout/charge-existing-card")
    fun checkoutSavedCardProduct(@Header("Authorization")token: String, @Body data: CheckOutSavedCard): LiveData<ApiResponse<GeneralRes<CheckOutRes>>>

    @FormUrlEncoded
    @POST("product-checkout/paystack-verify")
    fun verifyProductTransaction(@Header("Authorization")token: String, @Field("transactionId") data: String): LiveData<ApiResponse<GeneralRes<Any>>>

    @POST("cart/product-to-cart")
    fun addProductToCart(@Header("Authorization")token: String, @Body data: AddProductCartData)
            : LiveData<ApiResponse<GeneralRes<AddToCartRes?>>>

    @DELETE("cart/{id}")
    fun deleteProductCartItem(@Header("Authorization")token: String, @Path("id") id:String): LiveData<ApiResponse<GeneralRes<Any>>>

    @GET("cart/user-count/{userId}")
    fun getProductCartCount(@Header("Authorization")token: String, @Path("userId") userId:String): LiveData<ApiResponse<GeneralRes<String?>>>

    @GET("cart/user-product-cart/{userId}")
    fun getProductCart(@Header("Authorization")token: String, @Path("userId") userId:String):
            LiveData<ApiResponse<GeneralRes<List<GetProductCartData?>>>>

    @GET("product-order/user/{userId}")
    fun getProductOrders(@Header("Authorization")token: String, @Path("userId") userId:String):
            LiveData<ApiResponse<GeneralRes<List<ProductOrder?>>>>

    @GET("address-book/user/{userId}")
    fun getShippingAddress(@Header("Authorization")token: String, @Path("userId") userId:String):
            LiveData<ApiResponse<GeneralRes<List<ShippingAddressData?>>>>

    @GET("address-book/user/{userId}/default")
    fun getDefaultShippingAddress(@Header("Authorization")token: String, @Path("userId") userId:String):
            LiveData<ApiResponse<GeneralRes<ShippingAddressData?>>>

    @POST("address-book")
    fun createShippingAddress(@Header("Authorization")token: String, @Body data: ShippingAddressData2):
            LiveData<ApiResponse<GeneralRes<Any?>>>

    @DELETE("address-book/{addressId}")
    fun deleteShippingAddress(@Header("Authorization")token: String, @Path("addressId") addressId:String):
            LiveData<ApiResponse<GeneralRes<Any?>>>


    @POST("withdrawal-request")
    fun withdraw(@Header("Authorization")token: String, @Body data:WithdrawData):
            LiveData<ApiResponse<GeneralRes<Any>>>

    @GET("updates/get-farm-updates-and-notifications")
    fun getFarmUpdates(@Header("Authorization")token: String): LiveData<ApiResponse<GeneralRes<FarmUpdates>>>

}