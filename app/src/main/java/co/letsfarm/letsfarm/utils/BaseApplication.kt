package co.letsfarm.letsfarm.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.appcompat.app.AppCompatDelegate
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.utils.isLive
import com.google.firebase.FirebaseApp
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.crashlytics.FirebaseCrashlytics

/** A home of app initalization**/

class BaseApplication : MultiDexApplication(){

    private var mFirebaseAnalytics: FirebaseAnalytics? = null

    override fun onCreate() {
        super.onCreate()

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        FirebaseApp.initializeApp(this)

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

//        if (isLive)
//            FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true)
//        else
//            FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(false)

        this.createNotificationChannel()

    }

    private fun createNotificationChannel() {
        val channelId = "LetsFarmChannelID"
        // Create the NotificationChannel, but only on API 26+ because
// the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name: CharSequence = getString(R.string.channel_name)
            val description = getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(channelId, name, importance)
            channel.description = description
            // Register the channel with the system; you can't change the importance
// or other notification behaviors after this
            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager?.createNotificationChannel(channel)
        }
    }
    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}

