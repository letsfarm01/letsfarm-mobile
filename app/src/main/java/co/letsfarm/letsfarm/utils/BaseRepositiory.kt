package co.letsfarm.letsfarm.utils

import android.app.Application
import androidx.lifecycle.LiveData
import co.letsfarm.letsfarm.dashBoard.model.GeneralRes
import co.letsfarm.letsfarm.auth.viewModel.NetworkResource
import co.letsfarm.letsfarm.dashBoard.model.SavedCard

open class BaseRepositiory (val services: ApiServices, val app: Application){

    var token = app.savepref().get("token", "")
    var userId = app.savepref().get("userId", "")


    fun fetchCards(): LiveData<Resource<List<SavedCard>>> {
        return object : NetworkResource<List<SavedCard>, List<SavedCard>>(
            app.isConnectedToTheInternet()
        ){
            override fun fetchService(): LiveData<ApiResponse<GeneralRes<List<SavedCard>>>> {
                return services.getSavedCard(token, userId)
            }

        }.asLiveData()
    }
}
