package co.letsfarm.letsfarm.utils

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.switchMap
import co.letsfarm.letsfarm.common.utils.AbsentLiveData
import co.letsfarm.letsfarm.dashBoard.model.SavedCard

open class BaseViewModel(val repo: BaseRepositiory, val appli: Application) : AndroidViewModel(appli) {


    private val fetchCardsLive= MutableLiveData<Boolean>()

    fun fetchCards() {
        fetchCardsLive.value=true
    }

    val fetchCardsRequest: LiveData<Resource<List<SavedCard>>> = fetchCardsLive.switchMap { data->
        if (data != null) {
            repo.fetchCards()
        }else{
            AbsentLiveData.create()
        }
    }
}