package co.letsfarm.letsfarm.utils

class Constants(){
    companion object{
        const val UNKNOWM_ERROR_RESPONSE=1
        const val EMPTY_RESPONSE=2
        const val NO_INTERNET_RESPONSE=3
        const val JSON_EXCEPTION=4
    }
}