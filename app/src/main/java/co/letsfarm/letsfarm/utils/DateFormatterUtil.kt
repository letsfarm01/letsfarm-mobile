package co.letsfarm.letsfarm.common.utils

import java.text.SimpleDateFormat
import java.util.*

class DateFormatterUtil {

    companion object {
        fun toSimpleFormat(date: Date) : String {
            val dateFormatter = SimpleDateFormat("yyyy-MM-d", Locale.getDefault())
            return dateFormatter.format(date)
        }

        fun fromSimpleToDate(simple: String?) : Date? {
            if (simple == null || simple.isEmpty())
                return null
            val dateFormatter = SimpleDateFormat("yyyy-MM-d", Locale.getDefault())
            return dateFormatter.parse(simple)
        }
    }

}