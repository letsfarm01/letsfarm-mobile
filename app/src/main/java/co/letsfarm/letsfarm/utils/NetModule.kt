package co.letsfarm.letsfarm.utils

import android.os.Build
import co.letsfarm.letsfarm.BuildConfig
import dagger.Module
import dagger.Provides
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import okhttp3.TlsVersion
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton
import javax.net.ssl.SSLContext


/**
 * Created by Akano Kola on 2020-02-06.
 */

@Module
open class NetModule {

    open fun enableTls12OnPreLollipop(client: OkHttpClient.Builder): OkHttpClient.Builder {
        if (Build.VERSION.SDK_INT in 16..21) {
            try {
                val sc: SSLContext = SSLContext.getInstance("TLSv1.2")
                sc.init(null, null, null)
                client.sslSocketFactory(
                    Tls12SocketFactory(
                        sc.socketFactory
                    )
                )
                val cs = ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                    .tlsVersions(TlsVersion.TLS_1_2)
                    .build()
                val specs: MutableList<ConnectionSpec> = ArrayList()
                specs.add(cs)
                specs.add(ConnectionSpec.COMPATIBLE_TLS)
                specs.add(ConnectionSpec.CLEARTEXT)
                client.connectionSpecs(specs)
            } catch (exc: Exception) {
//                Log.e("OkHttpTLSCompat", "Error while setting TLS 1.2", exc)
            }
        }
        return client
    }
    @Singleton
    @Provides
    fun provideClient(): OkHttpClient.Builder {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val httpClient = OkHttpClient.Builder()
//        httpClient.enableTls12()
        httpClient.connectTimeout(30, TimeUnit.SECONDS)
        httpClient.readTimeout(30, TimeUnit.SECONDS)
        httpClient.writeTimeout(30, TimeUnit.SECONDS)
        if (!isLive) {
            httpClient.addInterceptor(logging)
        }

        httpClient.addInterceptor { chain ->
            val builder = chain.request().newBuilder()
            builder.addHeader("OS", "android")
            builder.addHeader("version", "${BuildConfig.VERSION_CODE}")
            val original = builder.build()
            chain.proceed(original)
        }
//        httpClient.retryOnConnectionFailure(false)


        httpClient.followRedirects(true)
        httpClient.followSslRedirects(true)
        httpClient.cache(null)


        return enableTls12OnPreLollipop(httpClient)
    }

    @Provides
    fun provideApiService(builder: OkHttpClient.Builder) : ApiServices {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .client(builder.build())
            .build()
            .create(ApiServices::class.java)
    }


}

