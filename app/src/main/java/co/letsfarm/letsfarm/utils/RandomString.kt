package co.letsfarm.letsfarm.common.utils

import java.security.SecureRandom
import java.util.*

class RandomString @JvmOverloads constructor(length: Int = 21, private val random: Random = SecureRandom(), symbols: String = alphanum) {

    private val symbols: CharArray

    private val buf: CharArray

    fun nextString(): String {
        for (idx in buf.indices)
            buf[idx] = symbols[random.nextInt(symbols.size)]
        return String(buf)
    }

    init {
        if (length < 1) throw IllegalArgumentException()
        if (symbols.length < 2) throw IllegalArgumentException()
        this.symbols = symbols.toCharArray()
        this.buf = CharArray(length)
    }

    companion object {

        const val upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

        val lower = upper.toLowerCase(Locale.ROOT)

        const val digits = "0123456789"

        val alphanum = upper + lower + digits
    }

}