package co.letsfarm.letsfarm.utils

sealed class Resource<T>(val data: T? = null, var message: String? = null, var status: String? = null, var responseCode:Int = 0) {

    class Success<T>(message: String?, data: T?, responseCode:Int) : Resource<T>(message = message, data = data, responseCode = responseCode)
    class Loading<T> : Resource<T>()
    class Error<T>(message: String, status: String, responseCode:Int) : Resource<T>(message = message, status = status, responseCode = responseCode)
}