package co.letsfarm.letsfarm.common.utils

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by Akano Kola on 2020-01-10.
 */
object SharedPrefUtil {

    private const val TOKEN = "token"
    private fun getSharedPref(context: Context): SharedPreferences {
        return context.getSharedPreferences("", Context.MODE_PRIVATE)
    }

    private fun getSharedPrefEditor(context: Context): SharedPreferences.Editor {
        return getSharedPref(context).edit()
    }

    fun getToken(context: Context): String? {
        return getSharedPref(context).getString(TOKEN, "")
    }

    fun setToken(context: Context, token: String?) {
        getSharedPrefEditor(context).putString(TOKEN, token)
            .apply()
    }

}
