package co.letsfarm.letsfarm.common.utils

import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.widget.TextView
import androidx.annotation.ColorRes

/**
 * Created by Akano Kola on 2020-02-13.
 */


object TextViewHelper {

    fun setTextWithColor(text: String, textsToColor: Array<String>, mTextView: TextView?) {
        val span = SpannableString(text)
        for (textToColor in textsToColor) {
            val index = text.toLowerCase().indexOf(textToColor.toLowerCase())
            if (index > -1 && mTextView != null)
                span.setSpan(ForegroundColorSpan(mTextView.resources.getColor(android.R.color.holo_red_dark)), index, index + textToColor.length, 0)
        }
        mTextView?.text = span
    }

    fun setTextWithColor(text: String, textsToColor: Array<String>, textView: TextView?, @ColorRes color: Int, clickSpan : List<ClickableSpan>? = null) {
        val span = SpannableString(text)
        var position = 0;
        for (textToColor in textsToColor) {
            val index = text.toLowerCase().indexOf(textToColor.toLowerCase())
            if (index > -1 && textView != null) {
                span.setSpan(ForegroundColorSpan(textView.resources.getColor(color)), index, index + textToColor.length, 0)
                if (clickSpan != null && clickSpan.size > position) {
                    span.setSpan(clickSpan[position], index, index + textToColor.length, 0)
                }
            }
            position++
        }

        textView?.linksClickable = true
        textView?.isClickable = true
        textView?.movementMethod = LinkMovementMethod.getInstance()

        textView?.text = span
    }


}
