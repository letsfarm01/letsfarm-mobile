package co.letsfarm.letsfarm.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.Canvas
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import android.net.Uri
import android.os.Build
import android.provider.OpenableColumns
import android.util.Log
import android.util.Patterns
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.annotation.IntegerRes
import androidx.annotation.NonNull
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import co.letsfarm.letsfarm.BuildConfig
import co.letsfarm.letsfarm.R
import co.letsfarm.letsfarm.auth.ui.LoginActivity
import com.afollestad.materialdialogs.MaterialDialog
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * extension function for intent
 * **/
inline fun<reified T:Any> newIntent(activity: Activity): Intent = Intent(activity,T::class.java)

inline  fun <reified T:Any> Activity.navigateTo(noinline init: Intent.()->Unit={}){
    val intent= newIntent<T>(this)
    intent.init()
    startActivity(intent)
}

inline  fun <reified T:Any> Activity.navigateToNew(noinline init: Intent.()->Unit={}){
    val intent= newIntent<T>(this)
    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
    intent.init()
    startActivity(intent)
}

inline  fun <reified T:Any> Activity.navigateForResult(requestCode:Int=111, noinline init: Intent.()->Unit={}){
    val intent= newIntent<T>(this)
    intent.init()
    startActivityForResult(intent, requestCode)
}

/** email pattern matching extension**/
fun String.validateEmail():Boolean{
    return android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
}


/** extensions classes for snackbar **/
@SuppressLint("ResourceType")
inline fun View.snack(@IntegerRes messageRes: Int, length: Int = Snackbar.LENGTH_LONG, f: Snackbar.() -> Unit) {
    snack(resources.getString(messageRes), length, f)
}

inline fun View.snack(message: String, length: Int = Snackbar.LENGTH_LONG, f: Snackbar.() -> Unit) {
    val snack = Snackbar.make(this, message, length)
    snack.f()
//    snack.setBackgroundTint(resources.getColor())
    snack.show()
}

@SuppressLint("ResourceType")
fun Snackbar.action(@IntegerRes actionRes: Int, color: Int? = null, listener: (View) -> Unit) {
    action(view.resources.getString(actionRes), color, listener)
}

fun Snackbar.action(action: String, color: Int? = null, listener: (View) -> Unit) {
    setAction(action, listener)
    color?.let { setActionTextColor(color) }

}

fun Activity.showErrorDialog(message:String? = "An error occurred") {
    showToast(message!!)
}

//fun Activity.showErrorDialog(title:String, message:String, listener:(()->Unit)?=null) {
//    val view = LayoutInflater.from(window!!.context).inflate(R.layout.layout_toast_error, null, true)
//    val textView = view?.findViewById<TextView>(R.id.error_text)
//
//    textView?.text = message
//    val toast = Toast(window.context)
//    toast.view = view
//    toast.duration = Toast.LENGTH_LONG
//    toast.show()
//
////    val cTimer = object : CountDownTimer(30000, 1000) {
////        override fun onTick(millisUntilFinished: Long) {
////            toast.show()
////        }
////        override fun onFinish() {
////            toast.cancel()
////        }
////    }
////    (cTimer as CountDownTimer).start()
//
//    view?.findViewById<ImageView>(R.id.close_icon)?.setOnClickListener {
////        (cTimer as CountDownTimer).cancel()
//        toast.cancel()
//    }
//
//}


/** activity extension of material dialog **/
fun Activity.showDialog(title:String, message:String, listener:(()->Unit)?=null){
    MaterialDialog(this).show {

        listener?.let {
            cancelable(false)
        }

        cornerRadius(5F)
        title(text = title)
        message(text = message)

        positiveButton(R.string.okay) { dialog ->
            listener?.let { it() }
            hide()
        }
    }
}
fun Activity.showConfirmationDialog(title:String, message:String, negativeListener:(()->Unit)?=null, postiiveListener:(()->Unit)){
    MaterialDialog(this).show {

        cancelable(false)

        cornerRadius(5F)
        title(text = title)
        message(text = message)

        positiveButton(R.string.yes) { dialog ->
            postiiveListener()
            hide()
        }
        negativeButton(R.string.no) {
            negativeListener?.let { it() }
            hide()
        }
    }
}

/** custome view/anyview  extension of material dialog **/
fun Context.LoginDialog(title:String="", message:String){
    MaterialDialog(this).show {
        title(text = title)
        message(text = message)

        positiveButton(R.string.okay) { dialog ->
            hide()
        }

    }
}

/** toast extension for anyview **/
fun Context.showToast(message: String){
    Toast.makeText(this,message, Toast.LENGTH_LONG).show()
}
/** sharedpref extenstion for alltypes **/
inline fun <reified T:Any> SharedPreferences.putData(Key:String, value:T){
    val editor = this.edit()
    when (T::class) {
        Boolean::class -> editor.putBoolean(Key, value as Boolean)
        Float::class -> editor.putFloat(Key, value as Float)
        String::class -> editor.putString(Key, value as String)
        Int::class -> editor.putInt(Key, value as Int)
        Long::class -> editor.putLong(Key, value as Long)
        else -> {
            if (value is Set<*>) {
                editor.putStringSet(Key, value as Set<String>)
            }
        }
    }
    editor.commit()
}
inline fun <reified T> SharedPreferences.get(Key:String, defaultValue:T):T
{
    when(T::class){
        Boolean::class->return this.getBoolean(Key,defaultValue as Boolean) as T
        Float::class->return this.getFloat(Key,defaultValue as Float) as T
        String::class->return this.getString(Key,defaultValue as String) as T
        Int::class->return this.getInt(Key,defaultValue as Int) as T
        Long::class->return this.getLong(Key,defaultValue as Long) as T
        else->{
            if (defaultValue is Set<*>){
                return this.getStringSet(Key,defaultValue as Set<String>) as T
            }
        }
    }
    return defaultValue
}

/** savepref  **/
fun Activity.savepref(name:String = "mypref"): SharedPreferences {
    return  applicationContext.getSharedPreferences(name, Context.MODE_PRIVATE)
}

fun Context.savepref(name:String = "mypref"): SharedPreferences {
    return  applicationContext.getSharedPreferences(name, Context.MODE_PRIVATE)
}


@NonNull
fun Application.isConnectedToTheInternet(): Boolean{

    val connectivityManager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        val nw      = connectivityManager.activeNetwork ?: return false
        val actNw = connectivityManager.getNetworkCapabilities(nw) ?: return false
        return when {
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            //for other device how are able to connect with Ethernet
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
            //for check internet over Bluetooth
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_BLUETOOTH) -> true
            else -> false
        }
    } else {
        val nwInfo = connectivityManager.activeNetworkInfo ?: return false
        return nwInfo.isConnectedOrConnecting
    }

}


 fun Activity.HideSoftKeyboard(){
    val view=this.currentFocus
    view?.let {
        val imm=getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        imm?.hideSoftInputFromWindow(it.windowToken,0)
    }
}

/**
 * For Actvities, allows declarations like
 * ```
 * val myViewModel = viewModelProvider(myViewModelFactory)
 * ```
 */
inline fun <reified VM : ViewModel> FragmentActivity.viewModelProvider(provider: ViewModel) =
    ViewModelProvider(this).get(VM::class.java)

/**
 * For Fragments, allows declarations like
 * ```
 * val myViewModel = viewModelProvider(myViewModelFactory)
 * ```
 */
inline fun <reified VM : ViewModel> Fragment.viewModelProvider(
    provider: ViewModel
) =
    ViewModelProvider(this).get(VM::class.java)

/**
 * Like [Fragment.viewModelProvider] for Fragments that want a [ViewModel] scoped to the Activity.
 */
inline fun <reified VM : ViewModel> Fragment.activityViewModelProvider(
    provider: ViewModelProvider.Factory
) =
    ViewModelProvider(requireActivity(), provider).get(VM::class.java)

/**
 * Like [Fragment.viewModelProvider] for Fragments that want a [ViewModel] scoped to the parent
 * Fragment.
 */
inline fun <reified VM : ViewModel> Fragment.parentViewModelProvider(
    provider: ViewModelProvider.Factory
) =
    ViewModelProvider(parentFragment!!, provider).get(VM::class.java)



fun log(message: String){
    if (!isLive)
        Log.v("okhttp", "myTag $message")
}

fun log(obj: Any?){
    if (!isLive)
        Log.v("okhttp", "myTag ${Gson().toJson(obj)}")
}

val Double.getMoneyWithoutSign: String
    get() =  DecimalFormat("##,###,###.##").format(this)

val Double.getMoney: String
    get() =  "₦"+DecimalFormat("##,###,###.##").format(this)

val Int.getMoney: String
    get() =  "₦"+DecimalFormat("##,###,###").format(this)

val EditText.removeComma: String
    get() = this.trimmedText.replace(",", "")

val String.removeComma: String
    get() =  this.replace(",", "").replace(" ", "")

val EditText.trimmedText: String
    get() = this.text?.toString()?.trim() ?: ""

val Date.parseDateGood: String?
    get() = SimpleDateFormat("MMM dd, yyyy").format(this)

fun EditText.isEmailValid(showError:Boolean = true) : Boolean{
    val valid =  Patterns.EMAIL_ADDRESS.matcher(this.trimmedText).matches()

    if (!valid && showError) {
        this.error = "Invalid email address"
    }

    return valid
}
fun EditText.isValid(no : Int = 1, errorText : String = "Invalid input") : Boolean{
    val valid =  this.trimmedText.length >= no

    if (!valid) {
        this.error = errorText
    }

    return valid
}
fun areNotNull(vararg objArr: Any?) : Boolean {
    for(obj in objArr) {
        if(obj == null)
            return false
    }
    return  true;
}

fun ProgressBar.load(load:Boolean){
    this.visibility = if (load) View.VISIBLE else View.GONE
}

fun Any?.isNull():Boolean{
    return this == null
}
fun Any?.isNotNull():Boolean{
    return this != null
}

fun View.show(){
    this.visibility = View.VISIBLE
}
fun View.hide(){
    this.visibility = View.GONE
}
fun View.setVisibility(visible:Boolean){
    if (visible) this.show() else this.hide()
}

fun Uri.getName(context: Context): String? {
    val returnCursor = context.contentResolver.query(this, null, null, null, null)
    val nameIndex = returnCursor?.getColumnIndex(OpenableColumns.DISPLAY_NAME)
    returnCursor?.moveToFirst()
    val fileName = nameIndex?.let { returnCursor.getString(it) }
    returnCursor?.close()
    return fileName
}
fun Array<String?>?.getPosition(string:String?):Int{
    this?.forEachIndexed{index, it->
        if (it == string){
            return index
        }
    }
    return 0
}
fun Int.getBitmapFromVectorDrawable(context: Context): Bitmap? {
    ContextCompat.getDrawable(context, this)?.let { drawable->

        val bitmap = Bitmap.createBitmap(
            drawable.intrinsicWidth,
            drawable.intrinsicHeight, Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight())
        drawable.draw(canvas)
        return bitmap
    }
    return null
}

val isDebug:Boolean
    get() = BuildConfig.BUILD_TYPE == "debug"

val isLive:Boolean
    get() = BuildConfig.BUILD_TYPE == "release"

fun Activity.processError(message: String?, responseCode: Int) {

    showErrorDialog(message)

    if (responseCode == 401 && (message?.contains("Token timeout", ignoreCase = true) == true
                || message?.contains("Access denied", ignoreCase = true) == true
                || message?.contains("Authorization failed, token is not valid or has expired", ignoreCase = true) == true
                || message?.contains("Unauthenticated", ignoreCase = true) == true)){

        savepref().edit()?.clear()?.apply()
        navigateToNew<LoginActivity>()
    }

}
